﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace Biblioteca
{
    class LeitorDAO
    {
        public static String tabela_leitor = "leitor";
        public Leitor leitor { set; get; }

        public void cadastrar_leitor()
        {
            if (leitor == null) return;
            String SQL = "Insert into :tabela_leitor (nome,rg,cpf,email, contato, status, data_nasc) values (':nome', ':rg', ':cpf', ':email',':contato', ':status', ':data_nasc')";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public void alterar_leitor()
        {
            String SQL = "UPDATE :tabela_leitor SET nome =':nome', rg =':rg', cpf=':cpf', email=':email', contato = ':contato', status=':status', data_nasc= ':data_nasc' where  rg =':rg'";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);

        }

        private String fazerSubstituicao(String SQL)
        {
            SQL = SQL.Replace(":tabela_leitor", tabela_leitor);
            if (leitor != null)
            {
                SQL = SQL.Replace(":nome", leitor.nome);
                SQL = SQL.Replace(":rg", leitor.rg);
                SQL = SQL.Replace(":cpf", leitor.cpf);
                SQL = SQL.Replace(":email", leitor.email);
                SQL = SQL.Replace(":contato", leitor.contato);
                SQL = SQL.Replace(":status", leitor.status);
                SQL = SQL.Replace(":data_nasc", leitor.data_nasc.ToString());                

            }

            return SQL;

        }

        public Leitor consultar_leitor(string rg)
        {
            String SQL = "SELECT nome,rg,cpf,email, contato, status, data_nasc FROM :tabela_leitor where rg = ':rg'";
            SQL = SQL.Replace(":rg", rg);
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            Leitor le = new Leitor();
            while (dr.Read())
            {
                le.nome = dr[0].ToString();
                le.rg = dr[1].ToString();
                le.cpf = dr[2].ToString();
                le.email = dr[3].ToString();
                le.contato = dr[4].ToString();
                le.status = dr[5].ToString();
                le.data_nasc = DateTime.Parse(dr[6].ToString());

            }

            db.fechar();
            return le;
        }

        public void excluir_leitor(string rg)
        {
            String SQL = "DELETE FROM :tabela_leitor where rg = ':rg'";
            SQL = SQL.Replace(":tabela_leitor", tabela_leitor);
            SQL = SQL.Replace(":rg", rg);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public List<Leitor> listar_leitor(string rg)
        {
            String SQL = "SELECT nome,rg,cpf,email, contato, status, data_nasc from :tabela_leitor where rg=':rg'";
            SQL = SQL.Replace(":tabela_leitor", tabela_leitor);
            SQL = SQL.Replace(":rg", rg);
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            List<Leitor> lista_leitor = new List<Leitor>();
            while (dr.Read())
            {
                Leitor le = new Leitor();
                le.rg = dr[0].ToString();
                le.nome = dr[1].ToString();
                le.cpf = dr[2].ToString();
                le.email = dr[3].ToString();
                le.contato = dr[4].ToString();
                le.status = dr[5].ToString();
                le.data_nasc = DateTime.Parse(dr[6].ToString());                
                lista_leitor.Add(le);
            }

            db.fechar();
            return lista_leitor;
        }
    }
}
