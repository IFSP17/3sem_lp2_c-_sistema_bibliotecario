﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca
{
    class Funcionario
    {
        public string nome { set; get; }
        public string rg {set;get;}
        public string cpf { set; get; }
        public string contato {set;get;}
        public string status {set;get;}
        public DateTime data_nasc {set;get;}
        public string email {set;get;}
        public string senha { set; get; }

        public override string ToString()
        {
            return nome + ", RG:" + rg + ", CPF:" + cpf + ", Fone:" + contato+", Status:"+status+", Nasc.:"+data_nasc.ToString()+", Email:"+email+", Senha:"+senha;
        }
    }

}
