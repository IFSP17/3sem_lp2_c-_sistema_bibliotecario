﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows.Forms;

namespace Biblioteca
{
    class Database
    {
        public NpgsqlConnection conn;

        public void abrir()
        {
            conn = new NpgsqlConnection("Server=127.0.0.1;" +
                "User Id=postgres;Password=123456;Database=Biblioteca;");
            // Abre a conexão
            conn.Open();
        }

        public int executaSQL(String tSQL)
        {
            int n = 0;
            try
            {
                abrir();
                NpgsqlCommand SQL = new NpgsqlCommand(tSQL, conn);
                n = SQL.ExecuteNonQuery();
                fechar();
                return 0;
            }
            catch (Exception erro)
            {
                Console.WriteLine(erro.Message);
                return 1;
            }
            return n;
        }

        public void fechar()
        {
            conn.Close();
        }
    }
}
