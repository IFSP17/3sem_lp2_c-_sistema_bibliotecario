﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace Biblioteca
{
    class AutorDao
    {
        public static String tabela_autor = "autor";
        public Autor autor { set; get; }

        public void cadastrar_autor()
        {
            if (autor == null) return;
            String SQL = "Insert into :tabela_autor (id_autor, nome, data_nasc, data_falecimento, categoria_principal, contato) values (:id, ':nome', ':data_nasc', ':data_falecimento', ':categoria_principal', ':contato')";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public void alterar_autor()
        {
            String SQL = "UPDATE :tabela_autor SET nome =':nome', data_nasc=':data_nasc', data_falecimento=':data_falecimento', categoria_principal =':categoria_principal', contato=':contato' where  id_autor =:id;";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        private String fazerSubstituicao(String SQL)
        {
            SQL = SQL.Replace(":tabela_autor", tabela_autor);
            if (autor!= null)
            {
                SQL = SQL.Replace(":id", autor.id.ToString());
                SQL = SQL.Replace(":nome", autor.nome);
                SQL = SQL.Replace(":data_nasc", autor.data_nasc.ToString());
                SQL = SQL.Replace(":data_falecimento", autor.data_falecimento.ToString());
                SQL = SQL.Replace(":categoria_principal", autor.categoria_principal);
                SQL = SQL.Replace(":contato", autor.contato);       

            }

            return SQL;

        }

        public Autor consultar_autor (int id)
        {
            String SQL = "SELECT id_autor, nome, data_nasc, data_falecimento, categoria_principal, contato FROM :tabela_autor where id_autor = :id";
            SQL = SQL.Replace(":id", id.ToString());
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            Autor a = new Autor();
            while (dr.Read())
            {
                a.id = int.Parse(dr[0].ToString());
                a.nome = dr[1].ToString();
                a.data_nasc = DateTime.Parse(dr[2].ToString());
                a.data_falecimento = DateTime.Parse(dr[3].ToString());
                a.categoria_principal = dr[4].ToString();
                a.contato = dr[5].ToString();
            }

            db.fechar();
            return a;
        }

        public void excluir_autor (int id)
        {
            String SQL = "DELETE FROM :tabela_autor where id_autor = :id";
            SQL = SQL.Replace(":tabela_autor", tabela_autor);
            SQL = SQL.Replace(":id", id.ToString());
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public List<Autor> listar_autor(int id)
        {
            String SQL = "SELECT id_autor, nome, data_nasc, data_falecimento, categoria_principal, contato from :tabela_autor where id_autor=:id";
            SQL = SQL.Replace(":tabela_autor", tabela_autor);
            SQL = SQL.Replace(":id", id.ToString());
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            List<Autor> lista_autor = new List<Autor>();
            while (dr.Read())
            {
                Autor a = new Autor();
                a.id = int.Parse(dr[0].ToString());
                a.nome = dr[1].ToString();
                a.data_nasc = DateTime.Parse(dr[2].ToString());
                a.data_falecimento = DateTime.Parse(dr[3].ToString());
                a.categoria_principal = dr[4].ToString();
                a.contato = dr[5].ToString();               
                lista_autor.Add(a);
            }

            db.fechar();
            return lista_autor;
        }
    }
}
