﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca
{
    class Emprestimo
    {
        public int id { set; get; }
        public string rg_leitor { set; get; }
        public int id_livro { set; get; }
        public string rg_funcionario { set; get; }
        public DateTime data_retirada { set; get; }
        public DateTime data_devolucao { set; get; }

        public override string ToString()
        {
            return "ID:" + id.ToString() + ", Leitor:" + rg_leitor + ", Livro:" + id_livro.ToString() + ", Funcionário:" + rg_funcionario + ", Retirada:" + data_retirada.ToString() + ", Devolução:" + data_devolucao.ToString();
        }
    }
}
