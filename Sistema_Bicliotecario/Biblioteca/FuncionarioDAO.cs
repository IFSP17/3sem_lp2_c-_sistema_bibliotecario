﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace Biblioteca
{
    class FuncionarioDAO
    {
        public static String tabela_func = "funcionario";
        public Funcionario funcionario {set;get;}

        public void cadastrar_func()
        {
            if (funcionario == null)return;
            String SQL = "insert into :tabela_func (nome, rg, cpf, contato, status, data_nasc, email, senha) values (':nome', ':rg', ':cpf', ':contato', ':status', ':data_nasc', ':email', ':senha');";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public void alterar_func()
        {
            String SQL = "UPDATE :tabela_func SET nome =':nome',cpf=':cpf', contato=':contato', data_nasc=':data_nasc',status=':status', email=':email', senha=':senha' where  rg =':rg'";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);

        }

        private String fazerSubstituicao(String SQL)
        {
            SQL = SQL.Replace(":tabela_func", tabela_func);
            if (funcionario != null)
            {
                SQL = SQL.Replace(":nome", funcionario.nome);
                SQL = SQL.Replace(":rg", funcionario.rg);
                SQL = SQL.Replace(":cpf", funcionario.cpf);
                SQL = SQL.Replace(":contato", funcionario.contato);
                SQL = SQL.Replace(":status", funcionario.status);
                SQL = SQL.Replace(":data_nasc", funcionario.data_nasc.ToString());
                SQL = SQL.Replace(":email", funcionario.email);
                SQL = SQL.Replace(":senha", funcionario.senha);
            }

            return SQL;

        }

        public Funcionario consultar_func(string rg)
        {
            String SQL = "SELECT rg,nome, cpf, contato, status, data_nasc, email, senha FROM :tabela_func where rg = ':rg';";
            SQL = SQL.Replace(":rg", rg);
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            Funcionario f = new Funcionario();
            while (dr.Read())
            {

                f.rg = dr[0].ToString();
                f.nome = dr[1].ToString();
                f.cpf = dr[2].ToString();
                f.contato = dr[3].ToString();
                f.status = dr[4].ToString();
                f.data_nasc = DateTime.Parse(dr[5].ToString());
                f.email = dr[6].ToString();
                f.senha = dr[7].ToString();
            }
            db.fechar();
            return f;
           
        }

        public void excluir_func(string rg)
        {
            String SQL = "DELETE FROM :tabela_func where rg = ':rg'";
            SQL = SQL.Replace(":tabela_func", tabela_func);
            SQL = SQL.Replace(":rg", rg);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public List<Funcionario> listar_func(string rg)
        {
            String SQL = "SELECT nome, rg, cpf, contato, status, data_nasc, email, senha from :tabela_func where rg=':rg'";
            SQL = SQL.Replace(":tabela_func", tabela_func);
            SQL = SQL.Replace(":rg", rg);
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            List<Funcionario> lista_func = new List<Funcionario>();
            while (dr.Read())
            {
                Funcionario f = new Funcionario();
                

                //carrega atributos

                f.nome = dr[0].ToString();
                f.rg = dr[1].ToString();
                f.cpf = dr[2].ToString();
                f.contato = dr[3].ToString();
                f.status = dr[4].ToString();
                f.data_nasc = DateTime.Parse(dr[5].ToString());
                f.email = dr[6].ToString();
                f.senha = dr[7].ToString();
                lista_func.Add(f);                    
            }

            db.fechar();
            return lista_func;

        }

    }
}
