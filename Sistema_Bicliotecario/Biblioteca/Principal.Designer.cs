﻿namespace Biblioteca
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inícioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excluirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leitoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.autoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.livrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.empréstimosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.gbinicio = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gbcadfunc = new System.Windows.Forms.GroupBox();
            this.btnfunccadlimpar = new System.Windows.Forms.Button();
            this.txtfunccadrg = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.datefunccaddatanasc = new System.Windows.Forms.DateTimePicker();
            this.cbfunccadstatus = new System.Windows.Forms.ComboBox();
            this.txtfunccadsenha = new System.Windows.Forms.TextBox();
            this.txtfunccadcontato = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtfunccadcpf = new System.Windows.Forms.TextBox();
            this.txtfunccademail = new System.Windows.Forms.TextBox();
            this.txtfunccadnome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnfunccadastrar = new System.Windows.Forms.Button();
            this.gbfuncionario = new System.Windows.Forms.GroupBox();
            this.gbexcluirfunc = new System.Windows.Forms.GroupBox();
            this.datedelfuncdatanasc = new System.Windows.Forms.DateTimePicker();
            this.btnfuncdelbuscar = new System.Windows.Forms.Button();
            this.btnfuncexcluir = new System.Windows.Forms.Button();
            this.btnfuncdellimpar = new System.Windows.Forms.Button();
            this.cbdelfuncstatus = new System.Windows.Forms.ComboBox();
            this.txtdelfuncsenha = new System.Windows.Forms.TextBox();
            this.txtdelfunccpf = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.txtdelfunccontato = new System.Windows.Forms.TextBox();
            this.txtdelfuncemail = new System.Windows.Forms.TextBox();
            this.txtdelfuncrg = new System.Windows.Forms.TextBox();
            this.txtdelfuncnome = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.gbfuncalter = new System.Windows.Forms.GroupBox();
            this.datefuncalterdatanasc = new System.Windows.Forms.DateTimePicker();
            this.cbfuncalterstatus = new System.Windows.Forms.ComboBox();
            this.txtfuncaltersenha = new System.Windows.Forms.TextBox();
            this.txtfuncaltercpf = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtfuncaltercontato = new System.Windows.Forms.TextBox();
            this.txtfuncalteremail = new System.Windows.Forms.TextBox();
            this.txtfuncalternome = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnfuncalterbuscar = new System.Windows.Forms.Button();
            this.btnfuncalterar = new System.Windows.Forms.Button();
            this.btnfuncalterlimpar = new System.Windows.Forms.Button();
            this.txtfuncalterrg = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.gbbuscarfunc = new System.Windows.Forms.GroupBox();
            this.listBoxfuncionario = new System.Windows.Forms.ListBox();
            this.btnfuncbuscar = new System.Windows.Forms.Button();
            this.btnfuncbuscalimpar = new System.Windows.Forms.Button();
            this.txtfuncbuscarg = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.gbleitores = new System.Windows.Forms.GroupBox();
            this.gbbuscarleitor = new System.Windows.Forms.GroupBox();
            this.listBoxleitor = new System.Windows.Forms.ListBox();
            this.btnleitorbuscar = new System.Windows.Forms.Button();
            this.btnleitorbuscarlimpar = new System.Windows.Forms.Button();
            this.txtleitorbuscarrg = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.gbexcluirleitor = new System.Windows.Forms.GroupBox();
            this.dateleitordeldatanasc = new System.Windows.Forms.DateTimePicker();
            this.btnleitordelbuscar = new System.Windows.Forms.Button();
            this.btnleitorexcluir = new System.Windows.Forms.Button();
            this.btnleitordellimpar = new System.Windows.Forms.Button();
            this.cbleitordelstatus = new System.Windows.Forms.ComboBox();
            this.txtleitordelcpf = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.txtleitordelcontato = new System.Windows.Forms.TextBox();
            this.txtleitordelemail = new System.Windows.Forms.TextBox();
            this.txtleitordelrg = new System.Windows.Forms.TextBox();
            this.txtleitordelnome = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.gbalterleitor = new System.Windows.Forms.GroupBox();
            this.dateleitoralterdatanasc = new System.Windows.Forms.DateTimePicker();
            this.btnleitoralterbuscar = new System.Windows.Forms.Button();
            this.btnleitoralterar = new System.Windows.Forms.Button();
            this.btnleitoralterlimpar = new System.Windows.Forms.Button();
            this.cbleitoralterstatus = new System.Windows.Forms.ComboBox();
            this.txtleitoraltercpf = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtleitoraltercontato = new System.Windows.Forms.TextBox();
            this.txtleitoralteremail = new System.Windows.Forms.TextBox();
            this.txtleitoralterrg = new System.Windows.Forms.TextBox();
            this.txtleitoralternome = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.gbcadleitor = new System.Windows.Forms.GroupBox();
            this.dateleitorcaddatanasc = new System.Windows.Forms.DateTimePicker();
            this.btnleitorcadastrar = new System.Windows.Forms.Button();
            this.btnleitorcadlimpar = new System.Windows.Forms.Button();
            this.cbleitorcadstatus = new System.Windows.Forms.ComboBox();
            this.txtleitorcadcpf = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtleitorcadcontato = new System.Windows.Forms.TextBox();
            this.txtleitorcademail = new System.Windows.Forms.TextBox();
            this.txtleitorcadrg = new System.Windows.Forms.TextBox();
            this.txtleitorcadnome = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.gbautores = new System.Windows.Forms.GroupBox();
            this.gbcadautor = new System.Windows.Forms.GroupBox();
            this.dateautorcaddatanasc = new System.Windows.Forms.DateTimePicker();
            this.dateautorcaddatafalesc = new System.Windows.Forms.DateTimePicker();
            this.txtautorcadid = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.btnautorcadastrar = new System.Windows.Forms.Button();
            this.btnautorcadlimpar = new System.Windows.Forms.Button();
            this.cbautorcadcategoria = new System.Windows.Forms.ComboBox();
            this.txtautorcadcontato = new System.Windows.Forms.TextBox();
            this.txtautorcadnome = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.gbbuscarautor = new System.Windows.Forms.GroupBox();
            this.listBoxautor = new System.Windows.Forms.ListBox();
            this.btnautorbuscar = new System.Windows.Forms.Button();
            this.btnautorbuscarlimpar = new System.Windows.Forms.Button();
            this.txtautorbuscarid = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.gbexcluirautor = new System.Windows.Forms.GroupBox();
            this.dateautordeldatafalesc = new System.Windows.Forms.DateTimePicker();
            this.dateautordeldatanasc = new System.Windows.Forms.DateTimePicker();
            this.cbautordelcategoria = new System.Windows.Forms.ComboBox();
            this.txtautordelcontato = new System.Windows.Forms.TextBox();
            this.txtautordelnome = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.btnautordelbuscar = new System.Windows.Forms.Button();
            this.btnautorexcluir = new System.Windows.Forms.Button();
            this.btnautordellimpar = new System.Windows.Forms.Button();
            this.txtautordelid = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.gbalterautor = new System.Windows.Forms.GroupBox();
            this.dateautoralterdatanasc = new System.Windows.Forms.DateTimePicker();
            this.dateautoralterdatafales = new System.Windows.Forms.DateTimePicker();
            this.cbautoraltercategoria = new System.Windows.Forms.ComboBox();
            this.txtautoraltercontato = new System.Windows.Forms.TextBox();
            this.txtautoralternome = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.btnautoralterbuscar = new System.Windows.Forms.Button();
            this.btnautoralterar = new System.Windows.Forms.Button();
            this.btnautoralterlimpar = new System.Windows.Forms.Button();
            this.txtautoralterid = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.gblivros = new System.Windows.Forms.GroupBox();
            this.gbbuscarlivro = new System.Windows.Forms.GroupBox();
            this.listBoxlivro = new System.Windows.Forms.ListBox();
            this.btnlivrobuscar = new System.Windows.Forms.Button();
            this.btnlivrobuscarlimpar = new System.Windows.Forms.Button();
            this.txtlivrobuscarid = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.gbexcluirlivro = new System.Windows.Forms.GroupBox();
            this.cblivrodelcaegoria = new System.Windows.Forms.ComboBox();
            this.txtlivrodelquant = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.txtlivrodeleditora = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.txtlivrodeldescricao = new System.Windows.Forms.TextBox();
            this.txtlivrodelano = new System.Windows.Forms.TextBox();
            this.txtlivrodeledicao = new System.Windows.Forms.TextBox();
            this.txtlivrodelidautor = new System.Windows.Forms.TextBox();
            this.txtlivrodeltitulo = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.btndelbuscar = new System.Windows.Forms.Button();
            this.btnlivroexcluir = new System.Windows.Forms.Button();
            this.btndellivrolimpar = new System.Windows.Forms.Button();
            this.txtlivrodelid = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.gbalterlivro = new System.Windows.Forms.GroupBox();
            this.cblivroaltercategoria = new System.Windows.Forms.ComboBox();
            this.txtlivroalterquant = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.txtlivroaltereditora = new System.Windows.Forms.TextBox();
            this.label107 = new System.Windows.Forms.Label();
            this.txtlivroalterdescricao = new System.Windows.Forms.TextBox();
            this.txtlivroalterano = new System.Windows.Forms.TextBox();
            this.txtlivroalteredicao = new System.Windows.Forms.TextBox();
            this.txtlivroalteridautor = new System.Windows.Forms.TextBox();
            this.txtlivroaltertitulo = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.btnlivroalterbuscar = new System.Windows.Forms.Button();
            this.btnlivroalterar = new System.Windows.Forms.Button();
            this.btnlivroalterlimpar = new System.Windows.Forms.Button();
            this.txtlivroalterid = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.gbcadlivro = new System.Windows.Forms.GroupBox();
            this.cblivrocadcategoria = new System.Windows.Forms.ComboBox();
            this.txtlivrocadquant = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.txtlivrocadeditora = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.txtlivrocadid = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.btnlivrocadastrar = new System.Windows.Forms.Button();
            this.btnlivrocadlimpar = new System.Windows.Forms.Button();
            this.txtlivrocaddescricao = new System.Windows.Forms.TextBox();
            this.txtlivrocadano = new System.Windows.Forms.TextBox();
            this.txtlivrocadedicao = new System.Windows.Forms.TextBox();
            this.txtlivrocadidautor = new System.Windows.Forms.TextBox();
            this.txtlivrocadtitulo = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.gbemprestimo = new System.Windows.Forms.GroupBox();
            this.gbalterempres = new System.Windows.Forms.GroupBox();
            this.txtempresalterrgleitor = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.btnempresalterbuscar = new System.Windows.Forms.Button();
            this.txtempresalteridempres = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.dateempresalterdatdev = new System.Windows.Forms.DateTimePicker();
            this.dateempresalterdataret = new System.Windows.Forms.DateTimePicker();
            this.txtempresalteridlivro = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtempresalterrgfunc = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.btnempresalterar = new System.Windows.Forms.Button();
            this.btnempresalterlimpar = new System.Windows.Forms.Button();
            this.gbcadempres = new System.Windows.Forms.GroupBox();
            this.txtemprescadid = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.dateemprescaddatadev = new System.Windows.Forms.DateTimePicker();
            this.dateemprescaddataret = new System.Windows.Forms.DateTimePicker();
            this.btnemprescadastrar = new System.Windows.Forms.Button();
            this.btnemprescadlimpar = new System.Windows.Forms.Button();
            this.txtemprescadidlivro = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.txtemprescadrgfunc = new System.Windows.Forms.TextBox();
            this.txtemprescadrgleitor = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.gbbuscarempres = new System.Windows.Forms.GroupBox();
            this.listBoxemprestimo = new System.Windows.Forms.ListBox();
            this.btnempresbuscar = new System.Windows.Forms.Button();
            this.btnempresbuscarlimpar = new System.Windows.Forms.Button();
            this.txtempresbuscarg = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.gbexcluirempres = new System.Windows.Forms.GroupBox();
            this.txtempresdelrgleitor = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.dateempresdeldatadev = new System.Windows.Forms.DateTimePicker();
            this.dateempresdeldataret = new System.Windows.Forms.DateTimePicker();
            this.txtempresdelidlivro = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.txtempresdelrgfunc = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.btnempresdelbuscar = new System.Windows.Forms.Button();
            this.btnempresexcluir = new System.Windows.Forms.Button();
            this.btnempresdellimpar = new System.Windows.Forms.Button();
            this.txtempresdelidempres = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.gbinicio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbcadfunc.SuspendLayout();
            this.gbfuncionario.SuspendLayout();
            this.gbexcluirfunc.SuspendLayout();
            this.gbfuncalter.SuspendLayout();
            this.gbbuscarfunc.SuspendLayout();
            this.gbleitores.SuspendLayout();
            this.gbbuscarleitor.SuspendLayout();
            this.gbexcluirleitor.SuspendLayout();
            this.gbalterleitor.SuspendLayout();
            this.gbcadleitor.SuspendLayout();
            this.gbautores.SuspendLayout();
            this.gbcadautor.SuspendLayout();
            this.gbbuscarautor.SuspendLayout();
            this.gbexcluirautor.SuspendLayout();
            this.gbalterautor.SuspendLayout();
            this.gblivros.SuspendLayout();
            this.gbbuscarlivro.SuspendLayout();
            this.gbexcluirlivro.SuspendLayout();
            this.gbalterlivro.SuspendLayout();
            this.gbcadlivro.SuspendLayout();
            this.gbemprestimo.SuspendLayout();
            this.gbalterempres.SuspendLayout();
            this.gbcadempres.SuspendLayout();
            this.gbbuscarempres.SuspendLayout();
            this.gbexcluirempres.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inícioToolStripMenuItem,
            this.funcionariosToolStripMenuItem,
            this.leitoresToolStripMenuItem,
            this.autoresToolStripMenuItem,
            this.livrosToolStripMenuItem,
            this.empréstimosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(510, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // inícioToolStripMenuItem
            // 
            this.inícioToolStripMenuItem.Name = "inícioToolStripMenuItem";
            this.inícioToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.inícioToolStripMenuItem.Text = "Início";
            this.inícioToolStripMenuItem.Click += new System.EventHandler(this.inícioToolStripMenuItem_Click);
            // 
            // funcionariosToolStripMenuItem
            // 
            this.funcionariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrasToolStripMenuItem,
            this.alterarToolStripMenuItem,
            this.excluirToolStripMenuItem,
            this.buscarToolStripMenuItem});
            this.funcionariosToolStripMenuItem.Name = "funcionariosToolStripMenuItem";
            this.funcionariosToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.funcionariosToolStripMenuItem.Text = "Funcionários";
            // 
            // cadastrasToolStripMenuItem
            // 
            this.cadastrasToolStripMenuItem.Name = "cadastrasToolStripMenuItem";
            this.cadastrasToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.cadastrasToolStripMenuItem.Text = "Cadastrar";
            this.cadastrasToolStripMenuItem.Click += new System.EventHandler(this.cadastrasToolStripMenuItem_Click);
            // 
            // alterarToolStripMenuItem
            // 
            this.alterarToolStripMenuItem.Name = "alterarToolStripMenuItem";
            this.alterarToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.alterarToolStripMenuItem.Text = "Alterar";
            this.alterarToolStripMenuItem.Click += new System.EventHandler(this.alterarToolStripMenuItem_Click);
            // 
            // excluirToolStripMenuItem
            // 
            this.excluirToolStripMenuItem.Name = "excluirToolStripMenuItem";
            this.excluirToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.excluirToolStripMenuItem.Text = "Excluir";
            this.excluirToolStripMenuItem.Click += new System.EventHandler(this.excluirToolStripMenuItem_Click);
            // 
            // buscarToolStripMenuItem
            // 
            this.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem";
            this.buscarToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.buscarToolStripMenuItem.Text = "Buscar";
            this.buscarToolStripMenuItem.Click += new System.EventHandler(this.buscarToolStripMenuItem_Click);
            // 
            // leitoresToolStripMenuItem
            // 
            this.leitoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.leitoresToolStripMenuItem.Name = "leitoresToolStripMenuItem";
            this.leitoresToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.leitoresToolStripMenuItem.Text = "Leitores";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem1.Text = "Cadastrar";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem2.Text = "Alterar";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem3.Text = "Excluir";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem4.Text = "Buscar";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // autoresToolStripMenuItem
            // 
            this.autoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10});
            this.autoresToolStripMenuItem.Name = "autoresToolStripMenuItem";
            this.autoresToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.autoresToolStripMenuItem.Text = "Autores";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem5.Text = "Cadastrar";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem8.Text = "Alterar";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem9.Text = "Excluir";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem10.Text = "Buscar";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // livrosToolStripMenuItem
            // 
            this.livrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6,
            this.toolStripMenuItem11,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13});
            this.livrosToolStripMenuItem.Name = "livrosToolStripMenuItem";
            this.livrosToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.livrosToolStripMenuItem.Text = "Livros";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem6.Text = "Cadastrar";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem11.Text = "Alterar";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.toolStripMenuItem11_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem12.Text = "Excluir";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.toolStripMenuItem12_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem13.Text = "Buscar";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
            // 
            // empréstimosToolStripMenuItem
            // 
            this.empréstimosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem7,
            this.toolStripMenuItem14,
            this.toolStripMenuItem15,
            this.toolStripMenuItem16});
            this.empréstimosToolStripMenuItem.Name = "empréstimosToolStripMenuItem";
            this.empréstimosToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.empréstimosToolStripMenuItem.Text = "Empréstimos";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem7.Text = "Cadastrar";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem14.Text = "Alterar";
            this.toolStripMenuItem14.Click += new System.EventHandler(this.toolStripMenuItem14_Click);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem15.Text = "Excluir";
            this.toolStripMenuItem15.Click += new System.EventHandler(this.toolStripMenuItem15_Click);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem16.Text = "Buscar";
            this.toolStripMenuItem16.Click += new System.EventHandler(this.toolStripMenuItem16_Click);
            // 
            // gbinicio
            // 
            this.gbinicio.Controls.Add(this.pictureBox1);
            this.gbinicio.Location = new System.Drawing.Point(1, 27);
            this.gbinicio.Name = "gbinicio";
            this.gbinicio.Size = new System.Drawing.Size(508, 344);
            this.gbinicio.TabIndex = 1;
            this.gbinicio.TabStop = false;
            this.gbinicio.Text = "Inicio";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-81, 45);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(684, 253);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // gbcadfunc
            // 
            this.gbcadfunc.Controls.Add(this.btnfunccadlimpar);
            this.gbcadfunc.Controls.Add(this.txtfunccadrg);
            this.gbcadfunc.Controls.Add(this.label9);
            this.gbcadfunc.Controls.Add(this.datefunccaddatanasc);
            this.gbcadfunc.Controls.Add(this.cbfunccadstatus);
            this.gbcadfunc.Controls.Add(this.txtfunccadsenha);
            this.gbcadfunc.Controls.Add(this.txtfunccadcontato);
            this.gbcadfunc.Controls.Add(this.label1);
            this.gbcadfunc.Controls.Add(this.txtfunccadcpf);
            this.gbcadfunc.Controls.Add(this.txtfunccademail);
            this.gbcadfunc.Controls.Add(this.txtfunccadnome);
            this.gbcadfunc.Controls.Add(this.label2);
            this.gbcadfunc.Controls.Add(this.label3);
            this.gbcadfunc.Controls.Add(this.label4);
            this.gbcadfunc.Controls.Add(this.label5);
            this.gbcadfunc.Controls.Add(this.label6);
            this.gbcadfunc.Controls.Add(this.label8);
            this.gbcadfunc.Controls.Add(this.btnfunccadastrar);
            this.gbcadfunc.Location = new System.Drawing.Point(11, 19);
            this.gbcadfunc.Name = "gbcadfunc";
            this.gbcadfunc.Size = new System.Drawing.Size(485, 314);
            this.gbcadfunc.TabIndex = 2;
            this.gbcadfunc.TabStop = false;
            this.gbcadfunc.Text = "Cadastrar Funcionário";
            // 
            // btnfunccadlimpar
            // 
            this.btnfunccadlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfunccadlimpar.Location = new System.Drawing.Point(385, 256);
            this.btnfunccadlimpar.Name = "btnfunccadlimpar";
            this.btnfunccadlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnfunccadlimpar.TabIndex = 48;
            this.btnfunccadlimpar.Text = "Limpar";
            this.btnfunccadlimpar.UseVisualStyleBackColor = true;
            this.btnfunccadlimpar.Click += new System.EventHandler(this.btnfunccadlimpar_Click);
            // 
            // txtfunccadrg
            // 
            this.txtfunccadrg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfunccadrg.Location = new System.Drawing.Point(70, 67);
            this.txtfunccadrg.Name = "txtfunccadrg";
            this.txtfunccadrg.Size = new System.Drawing.Size(182, 25);
            this.txtfunccadrg.TabIndex = 47;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(26, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 17);
            this.label9.TabIndex = 46;
            this.label9.Text = "RG:";
            // 
            // datefunccaddatanasc
            // 
            this.datefunccaddatanasc.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.datefunccaddatanasc.Location = new System.Drawing.Point(118, 135);
            this.datefunccaddatanasc.Name = "datefunccaddatanasc";
            this.datefunccaddatanasc.Size = new System.Drawing.Size(275, 25);
            this.datefunccaddatanasc.TabIndex = 45;
            // 
            // cbfunccadstatus
            // 
            this.cbfunccadstatus.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbfunccadstatus.FormattingEnabled = true;
            this.cbfunccadstatus.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbfunccadstatus.Location = new System.Drawing.Point(313, 99);
            this.cbfunccadstatus.Name = "cbfunccadstatus";
            this.cbfunccadstatus.Size = new System.Drawing.Size(148, 25);
            this.cbfunccadstatus.TabIndex = 44;
            // 
            // txtfunccadsenha
            // 
            this.txtfunccadsenha.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfunccadsenha.Location = new System.Drawing.Point(77, 207);
            this.txtfunccadsenha.Name = "txtfunccadsenha";
            this.txtfunccadsenha.Size = new System.Drawing.Size(172, 25);
            this.txtfunccadsenha.TabIndex = 43;
            this.txtfunccadsenha.UseSystemPasswordChar = true;
            // 
            // txtfunccadcontato
            // 
            this.txtfunccadcontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfunccadcontato.Location = new System.Drawing.Point(80, 99);
            this.txtfunccadcontato.Name = "txtfunccadcontato";
            this.txtfunccadcontato.Size = new System.Drawing.Size(182, 25);
            this.txtfunccadcontato.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 41;
            this.label1.Text = "Contato:";
            // 
            // txtfunccadcpf
            // 
            this.txtfunccadcpf.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfunccadcpf.Location = new System.Drawing.Point(291, 67);
            this.txtfunccadcpf.Name = "txtfunccadcpf";
            this.txtfunccadcpf.Size = new System.Drawing.Size(171, 25);
            this.txtfunccadcpf.TabIndex = 40;
            // 
            // txtfunccademail
            // 
            this.txtfunccademail.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfunccademail.Location = new System.Drawing.Point(66, 170);
            this.txtfunccademail.Name = "txtfunccademail";
            this.txtfunccademail.Size = new System.Drawing.Size(392, 25);
            this.txtfunccademail.TabIndex = 39;
            // 
            // txtfunccadnome
            // 
            this.txtfunccadnome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfunccadnome.Location = new System.Drawing.Point(72, 37);
            this.txtfunccadnome.Name = "txtfunccadnome";
            this.txtfunccadnome.Size = new System.Drawing.Size(389, 25);
            this.txtfunccadnome.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 37;
            this.label2.Text = "Senha:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(255, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 17);
            this.label3.TabIndex = 36;
            this.label3.Text = "CPF:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 17);
            this.label4.TabIndex = 35;
            this.label4.Text = "Data de Nasc.:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 34;
            this.label5.Text = "Email:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(264, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 17);
            this.label6.TabIndex = 33;
            this.label6.Text = "Status:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 17);
            this.label8.TabIndex = 32;
            this.label8.Text = "Nome:";
            // 
            // btnfunccadastrar
            // 
            this.btnfunccadastrar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfunccadastrar.Location = new System.Drawing.Point(289, 257);
            this.btnfunccadastrar.Name = "btnfunccadastrar";
            this.btnfunccadastrar.Size = new System.Drawing.Size(75, 31);
            this.btnfunccadastrar.TabIndex = 19;
            this.btnfunccadastrar.Text = "Cadastrar";
            this.btnfunccadastrar.UseVisualStyleBackColor = true;
            this.btnfunccadastrar.Click += new System.EventHandler(this.btnfunccadastrar_Click);
            // 
            // gbfuncionario
            // 
            this.gbfuncionario.Controls.Add(this.gbcadfunc);
            this.gbfuncionario.Controls.Add(this.gbexcluirfunc);
            this.gbfuncionario.Controls.Add(this.gbfuncalter);
            this.gbfuncionario.Controls.Add(this.gbbuscarfunc);
            this.gbfuncionario.Location = new System.Drawing.Point(0, 27);
            this.gbfuncionario.Name = "gbfuncionario";
            this.gbfuncionario.Size = new System.Drawing.Size(508, 344);
            this.gbfuncionario.TabIndex = 20;
            this.gbfuncionario.TabStop = false;
            this.gbfuncionario.Text = "Funcionario";
            // 
            // gbexcluirfunc
            // 
            this.gbexcluirfunc.Controls.Add(this.datedelfuncdatanasc);
            this.gbexcluirfunc.Controls.Add(this.btnfuncdelbuscar);
            this.gbexcluirfunc.Controls.Add(this.btnfuncexcluir);
            this.gbexcluirfunc.Controls.Add(this.btnfuncdellimpar);
            this.gbexcluirfunc.Controls.Add(this.cbdelfuncstatus);
            this.gbexcluirfunc.Controls.Add(this.txtdelfuncsenha);
            this.gbexcluirfunc.Controls.Add(this.txtdelfunccpf);
            this.gbexcluirfunc.Controls.Add(this.label74);
            this.gbexcluirfunc.Controls.Add(this.txtdelfunccontato);
            this.gbexcluirfunc.Controls.Add(this.txtdelfuncemail);
            this.gbexcluirfunc.Controls.Add(this.txtdelfuncrg);
            this.gbexcluirfunc.Controls.Add(this.txtdelfuncnome);
            this.gbexcluirfunc.Controls.Add(this.label76);
            this.gbexcluirfunc.Controls.Add(this.label77);
            this.gbexcluirfunc.Controls.Add(this.label78);
            this.gbexcluirfunc.Controls.Add(this.label79);
            this.gbexcluirfunc.Controls.Add(this.label80);
            this.gbexcluirfunc.Controls.Add(this.label81);
            this.gbexcluirfunc.Controls.Add(this.label82);
            this.gbexcluirfunc.Location = new System.Drawing.Point(11, 18);
            this.gbexcluirfunc.Name = "gbexcluirfunc";
            this.gbexcluirfunc.Size = new System.Drawing.Size(485, 314);
            this.gbexcluirfunc.TabIndex = 21;
            this.gbexcluirfunc.TabStop = false;
            this.gbexcluirfunc.Text = "Excluir Funcionário";
            // 
            // datedelfuncdatanasc
            // 
            this.datedelfuncdatanasc.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.datedelfuncdatanasc.Location = new System.Drawing.Point(114, 144);
            this.datedelfuncdatanasc.Name = "datedelfuncdatanasc";
            this.datedelfuncdatanasc.Size = new System.Drawing.Size(275, 25);
            this.datedelfuncdatanasc.TabIndex = 31;
            // 
            // btnfuncdelbuscar
            // 
            this.btnfuncdelbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfuncdelbuscar.Location = new System.Drawing.Point(248, 15);
            this.btnfuncdelbuscar.Name = "btnfuncdelbuscar";
            this.btnfuncdelbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnfuncdelbuscar.TabIndex = 20;
            this.btnfuncdelbuscar.Text = "Buscar";
            this.btnfuncdelbuscar.UseVisualStyleBackColor = true;
            this.btnfuncdelbuscar.Click += new System.EventHandler(this.btnfuncdelbuscar_Click);
            // 
            // btnfuncexcluir
            // 
            this.btnfuncexcluir.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfuncexcluir.Location = new System.Drawing.Point(289, 257);
            this.btnfuncexcluir.Name = "btnfuncexcluir";
            this.btnfuncexcluir.Size = new System.Drawing.Size(75, 31);
            this.btnfuncexcluir.TabIndex = 19;
            this.btnfuncexcluir.Text = "Excluir";
            this.btnfuncexcluir.UseVisualStyleBackColor = true;
            this.btnfuncexcluir.Click += new System.EventHandler(this.btnfuncexcluir_Click);
            // 
            // btnfuncdellimpar
            // 
            this.btnfuncdellimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfuncdellimpar.Location = new System.Drawing.Point(382, 257);
            this.btnfuncdellimpar.Name = "btnfuncdellimpar";
            this.btnfuncdellimpar.Size = new System.Drawing.Size(75, 31);
            this.btnfuncdellimpar.TabIndex = 18;
            this.btnfuncdellimpar.Text = "Limpar";
            this.btnfuncdellimpar.UseVisualStyleBackColor = true;
            this.btnfuncdellimpar.Click += new System.EventHandler(this.btnfuncdellimpar_Click);
            // 
            // cbdelfuncstatus
            // 
            this.cbdelfuncstatus.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbdelfuncstatus.FormattingEnabled = true;
            this.cbdelfuncstatus.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbdelfuncstatus.Location = new System.Drawing.Point(309, 108);
            this.cbdelfuncstatus.Name = "cbdelfuncstatus";
            this.cbdelfuncstatus.Size = new System.Drawing.Size(148, 25);
            this.cbdelfuncstatus.TabIndex = 17;
            // 
            // txtdelfuncsenha
            // 
            this.txtdelfuncsenha.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtdelfuncsenha.Location = new System.Drawing.Point(284, 217);
            this.txtdelfuncsenha.Name = "txtdelfuncsenha";
            this.txtdelfuncsenha.Size = new System.Drawing.Size(172, 25);
            this.txtdelfuncsenha.TabIndex = 16;
            this.txtdelfuncsenha.UseSystemPasswordChar = true;
            // 
            // txtdelfunccpf
            // 
            this.txtdelfunccpf.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtdelfunccpf.Location = new System.Drawing.Point(65, 108);
            this.txtdelfunccpf.Name = "txtdelfunccpf";
            this.txtdelfunccpf.Size = new System.Drawing.Size(182, 25);
            this.txtdelfunccpf.TabIndex = 15;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(21, 113);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(38, 17);
            this.label74.TabIndex = 14;
            this.label74.Text = "CPF:";
            // 
            // txtdelfunccontato
            // 
            this.txtdelfunccontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtdelfunccontato.Location = new System.Drawing.Point(77, 214);
            this.txtdelfunccontato.Name = "txtdelfunccontato";
            this.txtdelfunccontato.Size = new System.Drawing.Size(149, 25);
            this.txtdelfunccontato.TabIndex = 12;
            // 
            // txtdelfuncemail
            // 
            this.txtdelfuncemail.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtdelfuncemail.Location = new System.Drawing.Point(62, 179);
            this.txtdelfuncemail.Name = "txtdelfuncemail";
            this.txtdelfuncemail.Size = new System.Drawing.Size(392, 25);
            this.txtdelfuncemail.TabIndex = 11;
            // 
            // txtdelfuncrg
            // 
            this.txtdelfuncrg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtdelfuncrg.Location = new System.Drawing.Point(59, 19);
            this.txtdelfuncrg.Name = "txtdelfuncrg";
            this.txtdelfuncrg.Size = new System.Drawing.Size(180, 25);
            this.txtdelfuncrg.TabIndex = 9;
            // 
            // txtdelfuncnome
            // 
            this.txtdelfuncnome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtdelfuncnome.Location = new System.Drawing.Point(68, 75);
            this.txtdelfuncnome.Name = "txtdelfuncnome";
            this.txtdelfuncnome.Size = new System.Drawing.Size(389, 25);
            this.txtdelfuncnome.TabIndex = 8;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(232, 219);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(47, 17);
            this.label76.TabIndex = 7;
            this.label76.Text = "Senha:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(19, 219);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(57, 17);
            this.label77.TabIndex = 5;
            this.label77.Text = "Contato:";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(21, 149);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(97, 17);
            this.label78.TabIndex = 4;
            this.label78.Text = "Data de Nasc.:";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(19, 183);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(44, 17);
            this.label79.TabIndex = 3;
            this.label79.Text = "Email:";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(260, 112);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(47, 17);
            this.label80.TabIndex = 2;
            this.label80.Text = "Status:";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(22, 22);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(31, 17);
            this.label81.TabIndex = 1;
            this.label81.Text = "RG:";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(22, 78);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(47, 17);
            this.label82.TabIndex = 0;
            this.label82.Text = "Nome:";
            // 
            // gbfuncalter
            // 
            this.gbfuncalter.Controls.Add(this.datefuncalterdatanasc);
            this.gbfuncalter.Controls.Add(this.cbfuncalterstatus);
            this.gbfuncalter.Controls.Add(this.txtfuncaltersenha);
            this.gbfuncalter.Controls.Add(this.txtfuncaltercpf);
            this.gbfuncalter.Controls.Add(this.label7);
            this.gbfuncalter.Controls.Add(this.txtfuncaltercontato);
            this.gbfuncalter.Controls.Add(this.txtfuncalteremail);
            this.gbfuncalter.Controls.Add(this.txtfuncalternome);
            this.gbfuncalter.Controls.Add(this.label10);
            this.gbfuncalter.Controls.Add(this.label11);
            this.gbfuncalter.Controls.Add(this.label12);
            this.gbfuncalter.Controls.Add(this.label13);
            this.gbfuncalter.Controls.Add(this.label14);
            this.gbfuncalter.Controls.Add(this.label16);
            this.gbfuncalter.Controls.Add(this.btnfuncalterbuscar);
            this.gbfuncalter.Controls.Add(this.btnfuncalterar);
            this.gbfuncalter.Controls.Add(this.btnfuncalterlimpar);
            this.gbfuncalter.Controls.Add(this.txtfuncalterrg);
            this.gbfuncalter.Controls.Add(this.label15);
            this.gbfuncalter.Location = new System.Drawing.Point(11, 19);
            this.gbfuncalter.Name = "gbfuncalter";
            this.gbfuncalter.Size = new System.Drawing.Size(485, 314);
            this.gbfuncalter.TabIndex = 20;
            this.gbfuncalter.TabStop = false;
            this.gbfuncalter.Text = "Alterar Funcionário";
            // 
            // datefuncalterdatanasc
            // 
            this.datefuncalterdatanasc.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.datefuncalterdatanasc.Location = new System.Drawing.Point(118, 143);
            this.datefuncalterdatanasc.Name = "datefuncalterdatanasc";
            this.datefuncalterdatanasc.Size = new System.Drawing.Size(275, 25);
            this.datefuncalterdatanasc.TabIndex = 45;
            // 
            // cbfuncalterstatus
            // 
            this.cbfuncalterstatus.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbfuncalterstatus.FormattingEnabled = true;
            this.cbfuncalterstatus.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbfuncalterstatus.Location = new System.Drawing.Point(313, 107);
            this.cbfuncalterstatus.Name = "cbfuncalterstatus";
            this.cbfuncalterstatus.Size = new System.Drawing.Size(148, 25);
            this.cbfuncalterstatus.TabIndex = 44;
            // 
            // txtfuncaltersenha
            // 
            this.txtfuncaltersenha.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfuncaltersenha.Location = new System.Drawing.Point(288, 216);
            this.txtfuncaltersenha.Name = "txtfuncaltersenha";
            this.txtfuncaltersenha.Size = new System.Drawing.Size(172, 25);
            this.txtfuncaltersenha.TabIndex = 43;
            this.txtfuncaltersenha.UseSystemPasswordChar = true;
            // 
            // txtfuncaltercpf
            // 
            this.txtfuncaltercpf.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfuncaltercpf.Location = new System.Drawing.Point(69, 107);
            this.txtfuncaltercpf.Name = "txtfuncaltercpf";
            this.txtfuncaltercpf.Size = new System.Drawing.Size(182, 25);
            this.txtfuncaltercpf.TabIndex = 42;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(25, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 17);
            this.label7.TabIndex = 41;
            this.label7.Text = "CPF:";
            // 
            // txtfuncaltercontato
            // 
            this.txtfuncaltercontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfuncaltercontato.Location = new System.Drawing.Point(81, 213);
            this.txtfuncaltercontato.Name = "txtfuncaltercontato";
            this.txtfuncaltercontato.Size = new System.Drawing.Size(149, 25);
            this.txtfuncaltercontato.TabIndex = 40;
            // 
            // txtfuncalteremail
            // 
            this.txtfuncalteremail.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfuncalteremail.Location = new System.Drawing.Point(66, 178);
            this.txtfuncalteremail.Name = "txtfuncalteremail";
            this.txtfuncalteremail.Size = new System.Drawing.Size(392, 25);
            this.txtfuncalteremail.TabIndex = 39;
            // 
            // txtfuncalternome
            // 
            this.txtfuncalternome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfuncalternome.Location = new System.Drawing.Point(72, 74);
            this.txtfuncalternome.Name = "txtfuncalternome";
            this.txtfuncalternome.Size = new System.Drawing.Size(389, 25);
            this.txtfuncalternome.TabIndex = 38;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(236, 218);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 17);
            this.label10.TabIndex = 37;
            this.label10.Text = "Senha:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(23, 218);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 17);
            this.label11.TabIndex = 36;
            this.label11.Text = "Contato:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(25, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 17);
            this.label12.TabIndex = 35;
            this.label12.Text = "Data de Nasc.:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(23, 182);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 17);
            this.label13.TabIndex = 34;
            this.label13.Text = "Email:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(264, 111);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 17);
            this.label14.TabIndex = 33;
            this.label14.Text = "Status:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(26, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 17);
            this.label16.TabIndex = 32;
            this.label16.Text = "Nome:";
            // 
            // btnfuncalterbuscar
            // 
            this.btnfuncalterbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfuncalterbuscar.Location = new System.Drawing.Point(248, 15);
            this.btnfuncalterbuscar.Name = "btnfuncalterbuscar";
            this.btnfuncalterbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnfuncalterbuscar.TabIndex = 20;
            this.btnfuncalterbuscar.Text = "Buscar";
            this.btnfuncalterbuscar.UseVisualStyleBackColor = true;
            this.btnfuncalterbuscar.Click += new System.EventHandler(this.btnfuncalterbuscar_Click);
            // 
            // btnfuncalterar
            // 
            this.btnfuncalterar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfuncalterar.Location = new System.Drawing.Point(289, 257);
            this.btnfuncalterar.Name = "btnfuncalterar";
            this.btnfuncalterar.Size = new System.Drawing.Size(75, 31);
            this.btnfuncalterar.TabIndex = 19;
            this.btnfuncalterar.Text = "Alterar";
            this.btnfuncalterar.UseVisualStyleBackColor = true;
            this.btnfuncalterar.Click += new System.EventHandler(this.btnfuncalterar_Click);
            // 
            // btnfuncalterlimpar
            // 
            this.btnfuncalterlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfuncalterlimpar.Location = new System.Drawing.Point(382, 257);
            this.btnfuncalterlimpar.Name = "btnfuncalterlimpar";
            this.btnfuncalterlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnfuncalterlimpar.TabIndex = 18;
            this.btnfuncalterlimpar.Text = "Limpar";
            this.btnfuncalterlimpar.UseVisualStyleBackColor = true;
            this.btnfuncalterlimpar.Click += new System.EventHandler(this.btnfuncalterlimpar_Click);
            // 
            // txtfuncalterrg
            // 
            this.txtfuncalterrg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfuncalterrg.Location = new System.Drawing.Point(59, 19);
            this.txtfuncalterrg.Name = "txtfuncalterrg";
            this.txtfuncalterrg.Size = new System.Drawing.Size(180, 25);
            this.txtfuncalterrg.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(22, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 17);
            this.label15.TabIndex = 1;
            this.label15.Text = "RG:";
            // 
            // gbbuscarfunc
            // 
            this.gbbuscarfunc.Controls.Add(this.listBoxfuncionario);
            this.gbbuscarfunc.Controls.Add(this.btnfuncbuscar);
            this.gbbuscarfunc.Controls.Add(this.btnfuncbuscalimpar);
            this.gbbuscarfunc.Controls.Add(this.txtfuncbuscarg);
            this.gbbuscarfunc.Controls.Add(this.label89);
            this.gbbuscarfunc.Location = new System.Drawing.Point(11, 18);
            this.gbbuscarfunc.Name = "gbbuscarfunc";
            this.gbbuscarfunc.Size = new System.Drawing.Size(485, 314);
            this.gbbuscarfunc.TabIndex = 22;
            this.gbbuscarfunc.TabStop = false;
            this.gbbuscarfunc.Text = "Buscar Funcionário";
            // 
            // listBoxfuncionario
            // 
            this.listBoxfuncionario.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.listBoxfuncionario.FormattingEnabled = true;
            this.listBoxfuncionario.Location = new System.Drawing.Point(26, 72);
            this.listBoxfuncionario.Name = "listBoxfuncionario";
            this.listBoxfuncionario.Size = new System.Drawing.Size(430, 160);
            this.listBoxfuncionario.TabIndex = 21;
            // 
            // btnfuncbuscar
            // 
            this.btnfuncbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfuncbuscar.Location = new System.Drawing.Point(248, 15);
            this.btnfuncbuscar.Name = "btnfuncbuscar";
            this.btnfuncbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnfuncbuscar.TabIndex = 20;
            this.btnfuncbuscar.Text = "Buscar";
            this.btnfuncbuscar.UseVisualStyleBackColor = true;
            this.btnfuncbuscar.Click += new System.EventHandler(this.btnfuncbuscar_Click);
            // 
            // btnfuncbuscalimpar
            // 
            this.btnfuncbuscalimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnfuncbuscalimpar.Location = new System.Drawing.Point(382, 257);
            this.btnfuncbuscalimpar.Name = "btnfuncbuscalimpar";
            this.btnfuncbuscalimpar.Size = new System.Drawing.Size(75, 31);
            this.btnfuncbuscalimpar.TabIndex = 18;
            this.btnfuncbuscalimpar.Text = "Limpar";
            this.btnfuncbuscalimpar.UseVisualStyleBackColor = true;
            this.btnfuncbuscalimpar.Click += new System.EventHandler(this.btnfuncbuscalimpar_Click);
            // 
            // txtfuncbuscarg
            // 
            this.txtfuncbuscarg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtfuncbuscarg.Location = new System.Drawing.Point(59, 19);
            this.txtfuncbuscarg.Name = "txtfuncbuscarg";
            this.txtfuncbuscarg.Size = new System.Drawing.Size(180, 25);
            this.txtfuncbuscarg.TabIndex = 9;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(22, 22);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(31, 17);
            this.label89.TabIndex = 1;
            this.label89.Text = "RG:";
            // 
            // gbleitores
            // 
            this.gbleitores.Controls.Add(this.gbbuscarleitor);
            this.gbleitores.Controls.Add(this.gbexcluirleitor);
            this.gbleitores.Controls.Add(this.gbalterleitor);
            this.gbleitores.Controls.Add(this.gbcadleitor);
            this.gbleitores.Location = new System.Drawing.Point(2, 27);
            this.gbleitores.Name = "gbleitores";
            this.gbleitores.Size = new System.Drawing.Size(508, 344);
            this.gbleitores.TabIndex = 21;
            this.gbleitores.TabStop = false;
            this.gbleitores.Text = "Leitores";
            // 
            // gbbuscarleitor
            // 
            this.gbbuscarleitor.Controls.Add(this.listBoxleitor);
            this.gbbuscarleitor.Controls.Add(this.btnleitorbuscar);
            this.gbbuscarleitor.Controls.Add(this.btnleitorbuscarlimpar);
            this.gbbuscarleitor.Controls.Add(this.txtleitorbuscarrg);
            this.gbbuscarleitor.Controls.Add(this.label106);
            this.gbbuscarleitor.Location = new System.Drawing.Point(11, 18);
            this.gbbuscarleitor.Name = "gbbuscarleitor";
            this.gbbuscarleitor.Size = new System.Drawing.Size(485, 314);
            this.gbbuscarleitor.TabIndex = 21;
            this.gbbuscarleitor.TabStop = false;
            this.gbbuscarleitor.Text = "Buscar Leitor";
            // 
            // listBoxleitor
            // 
            this.listBoxleitor.FormattingEnabled = true;
            this.listBoxleitor.Location = new System.Drawing.Point(25, 70);
            this.listBoxleitor.Name = "listBoxleitor";
            this.listBoxleitor.Size = new System.Drawing.Size(430, 173);
            this.listBoxleitor.TabIndex = 21;
            // 
            // btnleitorbuscar
            // 
            this.btnleitorbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitorbuscar.Location = new System.Drawing.Point(248, 20);
            this.btnleitorbuscar.Name = "btnleitorbuscar";
            this.btnleitorbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnleitorbuscar.TabIndex = 20;
            this.btnleitorbuscar.Text = "Buscar";
            this.btnleitorbuscar.UseVisualStyleBackColor = true;
            this.btnleitorbuscar.Click += new System.EventHandler(this.btnleitorbuscar_Click);
            // 
            // btnleitorbuscarlimpar
            // 
            this.btnleitorbuscarlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitorbuscarlimpar.Location = new System.Drawing.Point(382, 257);
            this.btnleitorbuscarlimpar.Name = "btnleitorbuscarlimpar";
            this.btnleitorbuscarlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnleitorbuscarlimpar.TabIndex = 18;
            this.btnleitorbuscarlimpar.Text = "Limpar";
            this.btnleitorbuscarlimpar.UseVisualStyleBackColor = true;
            this.btnleitorbuscarlimpar.Click += new System.EventHandler(this.btnleitorbuscarlimpar_Click);
            // 
            // txtleitorbuscarrg
            // 
            this.txtleitorbuscarrg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitorbuscarrg.Location = new System.Drawing.Point(59, 24);
            this.txtleitorbuscarrg.Name = "txtleitorbuscarrg";
            this.txtleitorbuscarrg.Size = new System.Drawing.Size(180, 25);
            this.txtleitorbuscarrg.TabIndex = 9;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(22, 27);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(31, 17);
            this.label106.TabIndex = 1;
            this.label106.Text = "RG:";
            // 
            // gbexcluirleitor
            // 
            this.gbexcluirleitor.Controls.Add(this.dateleitordeldatanasc);
            this.gbexcluirleitor.Controls.Add(this.btnleitordelbuscar);
            this.gbexcluirleitor.Controls.Add(this.btnleitorexcluir);
            this.gbexcluirleitor.Controls.Add(this.btnleitordellimpar);
            this.gbexcluirleitor.Controls.Add(this.cbleitordelstatus);
            this.gbexcluirleitor.Controls.Add(this.txtleitordelcpf);
            this.gbexcluirleitor.Controls.Add(this.label93);
            this.gbexcluirleitor.Controls.Add(this.txtleitordelcontato);
            this.gbexcluirleitor.Controls.Add(this.txtleitordelemail);
            this.gbexcluirleitor.Controls.Add(this.txtleitordelrg);
            this.gbexcluirleitor.Controls.Add(this.txtleitordelnome);
            this.gbexcluirleitor.Controls.Add(this.label94);
            this.gbexcluirleitor.Controls.Add(this.label95);
            this.gbexcluirleitor.Controls.Add(this.label96);
            this.gbexcluirleitor.Controls.Add(this.label97);
            this.gbexcluirleitor.Controls.Add(this.label99);
            this.gbexcluirleitor.Controls.Add(this.label100);
            this.gbexcluirleitor.Location = new System.Drawing.Point(11, 19);
            this.gbexcluirleitor.Name = "gbexcluirleitor";
            this.gbexcluirleitor.Size = new System.Drawing.Size(485, 314);
            this.gbexcluirleitor.TabIndex = 21;
            this.gbexcluirleitor.TabStop = false;
            this.gbexcluirleitor.Text = "Excluir Leitor";
            // 
            // dateleitordeldatanasc
            // 
            this.dateleitordeldatanasc.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateleitordeldatanasc.Location = new System.Drawing.Point(124, 228);
            this.dateleitordeldatanasc.Name = "dateleitordeldatanasc";
            this.dateleitordeldatanasc.Size = new System.Drawing.Size(236, 20);
            this.dateleitordeldatanasc.TabIndex = 33;
            // 
            // btnleitordelbuscar
            // 
            this.btnleitordelbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitordelbuscar.Location = new System.Drawing.Point(248, 20);
            this.btnleitordelbuscar.Name = "btnleitordelbuscar";
            this.btnleitordelbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnleitordelbuscar.TabIndex = 20;
            this.btnleitordelbuscar.Text = "Buscar";
            this.btnleitordelbuscar.UseVisualStyleBackColor = true;
            this.btnleitordelbuscar.Click += new System.EventHandler(this.btnleitordelbuscar_Click);
            // 
            // btnleitorexcluir
            // 
            this.btnleitorexcluir.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitorexcluir.Location = new System.Drawing.Point(289, 257);
            this.btnleitorexcluir.Name = "btnleitorexcluir";
            this.btnleitorexcluir.Size = new System.Drawing.Size(75, 31);
            this.btnleitorexcluir.TabIndex = 19;
            this.btnleitorexcluir.Text = "Excluir";
            this.btnleitorexcluir.UseVisualStyleBackColor = true;
            this.btnleitorexcluir.Click += new System.EventHandler(this.btnleitorexcluir_Click);
            // 
            // btnleitordellimpar
            // 
            this.btnleitordellimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitordellimpar.Location = new System.Drawing.Point(382, 257);
            this.btnleitordellimpar.Name = "btnleitordellimpar";
            this.btnleitordellimpar.Size = new System.Drawing.Size(75, 31);
            this.btnleitordellimpar.TabIndex = 18;
            this.btnleitordellimpar.Text = "Limpar";
            this.btnleitordellimpar.UseVisualStyleBackColor = true;
            this.btnleitordellimpar.Click += new System.EventHandler(this.btnleitordellimpar_Click);
            // 
            // cbleitordelstatus
            // 
            this.cbleitordelstatus.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbleitordelstatus.FormattingEnabled = true;
            this.cbleitordelstatus.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbleitordelstatus.Location = new System.Drawing.Point(312, 118);
            this.cbleitordelstatus.Name = "cbleitordelstatus";
            this.cbleitordelstatus.Size = new System.Drawing.Size(145, 25);
            this.cbleitordelstatus.TabIndex = 17;
            // 
            // txtleitordelcpf
            // 
            this.txtleitordelcpf.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitordelcpf.Location = new System.Drawing.Point(65, 116);
            this.txtleitordelcpf.Name = "txtleitordelcpf";
            this.txtleitordelcpf.Size = new System.Drawing.Size(182, 25);
            this.txtleitordelcpf.TabIndex = 15;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(21, 121);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(38, 17);
            this.label93.TabIndex = 14;
            this.label93.Text = "CPF:";
            // 
            // txtleitordelcontato
            // 
            this.txtleitordelcontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitordelcontato.Location = new System.Drawing.Point(80, 191);
            this.txtleitordelcontato.Name = "txtleitordelcontato";
            this.txtleitordelcontato.Size = new System.Drawing.Size(149, 25);
            this.txtleitordelcontato.TabIndex = 12;
            // 
            // txtleitordelemail
            // 
            this.txtleitordelemail.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitordelemail.Location = new System.Drawing.Point(65, 152);
            this.txtleitordelemail.Name = "txtleitordelemail";
            this.txtleitordelemail.Size = new System.Drawing.Size(392, 25);
            this.txtleitordelemail.TabIndex = 11;
            // 
            // txtleitordelrg
            // 
            this.txtleitordelrg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitordelrg.Location = new System.Drawing.Point(59, 24);
            this.txtleitordelrg.Name = "txtleitordelrg";
            this.txtleitordelrg.Size = new System.Drawing.Size(180, 25);
            this.txtleitordelrg.TabIndex = 9;
            // 
            // txtleitordelnome
            // 
            this.txtleitordelnome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitordelnome.Location = new System.Drawing.Point(68, 80);
            this.txtleitordelnome.Name = "txtleitordelnome";
            this.txtleitordelnome.Size = new System.Drawing.Size(389, 25);
            this.txtleitordelnome.TabIndex = 8;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(22, 196);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(57, 17);
            this.label94.TabIndex = 5;
            this.label94.Text = "Contato:";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(26, 229);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(97, 17);
            this.label95.TabIndex = 4;
            this.label95.Text = "Data de Nasc.:";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(22, 156);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(44, 17);
            this.label96.TabIndex = 3;
            this.label96.Text = "Email:";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(254, 121);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(47, 17);
            this.label97.TabIndex = 2;
            this.label97.Text = "Status:";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(22, 27);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(31, 17);
            this.label99.TabIndex = 1;
            this.label99.Text = "RG:";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(22, 83);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(47, 17);
            this.label100.TabIndex = 0;
            this.label100.Text = "Nome:";
            // 
            // gbalterleitor
            // 
            this.gbalterleitor.Controls.Add(this.dateleitoralterdatanasc);
            this.gbalterleitor.Controls.Add(this.btnleitoralterbuscar);
            this.gbalterleitor.Controls.Add(this.btnleitoralterar);
            this.gbalterleitor.Controls.Add(this.btnleitoralterlimpar);
            this.gbalterleitor.Controls.Add(this.cbleitoralterstatus);
            this.gbalterleitor.Controls.Add(this.txtleitoraltercpf);
            this.gbalterleitor.Controls.Add(this.label17);
            this.gbalterleitor.Controls.Add(this.txtleitoraltercontato);
            this.gbalterleitor.Controls.Add(this.txtleitoralteremail);
            this.gbalterleitor.Controls.Add(this.txtleitoralterrg);
            this.gbalterleitor.Controls.Add(this.txtleitoralternome);
            this.gbalterleitor.Controls.Add(this.label19);
            this.gbalterleitor.Controls.Add(this.label20);
            this.gbalterleitor.Controls.Add(this.label21);
            this.gbalterleitor.Controls.Add(this.label22);
            this.gbalterleitor.Controls.Add(this.label23);
            this.gbalterleitor.Controls.Add(this.label24);
            this.gbalterleitor.Location = new System.Drawing.Point(11, 19);
            this.gbalterleitor.Name = "gbalterleitor";
            this.gbalterleitor.Size = new System.Drawing.Size(485, 314);
            this.gbalterleitor.TabIndex = 20;
            this.gbalterleitor.TabStop = false;
            this.gbalterleitor.Text = "Alterar Leitor";
            // 
            // dateleitoralterdatanasc
            // 
            this.dateleitoralterdatanasc.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateleitoralterdatanasc.Location = new System.Drawing.Point(116, 228);
            this.dateleitoralterdatanasc.Name = "dateleitoralterdatanasc";
            this.dateleitoralterdatanasc.Size = new System.Drawing.Size(236, 20);
            this.dateleitoralterdatanasc.TabIndex = 33;
            // 
            // btnleitoralterbuscar
            // 
            this.btnleitoralterbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitoralterbuscar.Location = new System.Drawing.Point(248, 20);
            this.btnleitoralterbuscar.Name = "btnleitoralterbuscar";
            this.btnleitoralterbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnleitoralterbuscar.TabIndex = 20;
            this.btnleitoralterbuscar.Text = "Buscar";
            this.btnleitoralterbuscar.UseVisualStyleBackColor = true;
            this.btnleitoralterbuscar.Click += new System.EventHandler(this.btnleitoralterbuscar_Click);
            // 
            // btnleitoralterar
            // 
            this.btnleitoralterar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitoralterar.Location = new System.Drawing.Point(289, 257);
            this.btnleitoralterar.Name = "btnleitoralterar";
            this.btnleitoralterar.Size = new System.Drawing.Size(75, 31);
            this.btnleitoralterar.TabIndex = 19;
            this.btnleitoralterar.Text = "Alterar";
            this.btnleitoralterar.UseVisualStyleBackColor = true;
            this.btnleitoralterar.Click += new System.EventHandler(this.btnleitoralterar_Click);
            // 
            // btnleitoralterlimpar
            // 
            this.btnleitoralterlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitoralterlimpar.Location = new System.Drawing.Point(382, 257);
            this.btnleitoralterlimpar.Name = "btnleitoralterlimpar";
            this.btnleitoralterlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnleitoralterlimpar.TabIndex = 18;
            this.btnleitoralterlimpar.Text = "Limpar";
            this.btnleitoralterlimpar.UseVisualStyleBackColor = true;
            this.btnleitoralterlimpar.Click += new System.EventHandler(this.btnleitoralterlimpar_Click);
            // 
            // cbleitoralterstatus
            // 
            this.cbleitoralterstatus.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbleitoralterstatus.FormattingEnabled = true;
            this.cbleitoralterstatus.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbleitoralterstatus.Location = new System.Drawing.Point(309, 115);
            this.cbleitoralterstatus.Name = "cbleitoralterstatus";
            this.cbleitoralterstatus.Size = new System.Drawing.Size(148, 25);
            this.cbleitoralterstatus.TabIndex = 17;
            // 
            // txtleitoraltercpf
            // 
            this.txtleitoraltercpf.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitoraltercpf.Location = new System.Drawing.Point(65, 116);
            this.txtleitoraltercpf.Name = "txtleitoraltercpf";
            this.txtleitoraltercpf.Size = new System.Drawing.Size(182, 25);
            this.txtleitoraltercpf.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(21, 121);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 17);
            this.label17.TabIndex = 14;
            this.label17.Text = "CPF:";
            // 
            // txtleitoraltercontato
            // 
            this.txtleitoraltercontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitoraltercontato.Location = new System.Drawing.Point(80, 191);
            this.txtleitoraltercontato.Name = "txtleitoraltercontato";
            this.txtleitoraltercontato.Size = new System.Drawing.Size(149, 25);
            this.txtleitoraltercontato.TabIndex = 12;
            // 
            // txtleitoralteremail
            // 
            this.txtleitoralteremail.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitoralteremail.Location = new System.Drawing.Point(65, 152);
            this.txtleitoralteremail.Name = "txtleitoralteremail";
            this.txtleitoralteremail.Size = new System.Drawing.Size(392, 25);
            this.txtleitoralteremail.TabIndex = 11;
            // 
            // txtleitoralterrg
            // 
            this.txtleitoralterrg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitoralterrg.Location = new System.Drawing.Point(59, 24);
            this.txtleitoralterrg.Name = "txtleitoralterrg";
            this.txtleitoralterrg.Size = new System.Drawing.Size(180, 25);
            this.txtleitoralterrg.TabIndex = 9;
            // 
            // txtleitoralternome
            // 
            this.txtleitoralternome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitoralternome.Location = new System.Drawing.Point(68, 80);
            this.txtleitoralternome.Name = "txtleitoralternome";
            this.txtleitoralternome.Size = new System.Drawing.Size(389, 25);
            this.txtleitoralternome.TabIndex = 8;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(22, 196);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 17);
            this.label19.TabIndex = 5;
            this.label19.Text = "Contato:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(22, 229);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 17);
            this.label20.TabIndex = 4;
            this.label20.Text = "Data de Nasc.:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(22, 156);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 17);
            this.label21.TabIndex = 3;
            this.label21.Text = "Email:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(254, 121);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 17);
            this.label22.TabIndex = 2;
            this.label22.Text = "Status:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(22, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 17);
            this.label23.TabIndex = 1;
            this.label23.Text = "RG:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(22, 83);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 17);
            this.label24.TabIndex = 0;
            this.label24.Text = "Nome:";
            // 
            // gbcadleitor
            // 
            this.gbcadleitor.Controls.Add(this.dateleitorcaddatanasc);
            this.gbcadleitor.Controls.Add(this.btnleitorcadastrar);
            this.gbcadleitor.Controls.Add(this.btnleitorcadlimpar);
            this.gbcadleitor.Controls.Add(this.cbleitorcadstatus);
            this.gbcadleitor.Controls.Add(this.txtleitorcadcpf);
            this.gbcadleitor.Controls.Add(this.label25);
            this.gbcadleitor.Controls.Add(this.txtleitorcadcontato);
            this.gbcadleitor.Controls.Add(this.txtleitorcademail);
            this.gbcadleitor.Controls.Add(this.txtleitorcadrg);
            this.gbcadleitor.Controls.Add(this.txtleitorcadnome);
            this.gbcadleitor.Controls.Add(this.label27);
            this.gbcadleitor.Controls.Add(this.label28);
            this.gbcadleitor.Controls.Add(this.label29);
            this.gbcadleitor.Controls.Add(this.label30);
            this.gbcadleitor.Controls.Add(this.label31);
            this.gbcadleitor.Controls.Add(this.label32);
            this.gbcadleitor.Location = new System.Drawing.Point(11, 19);
            this.gbcadleitor.Name = "gbcadleitor";
            this.gbcadleitor.Size = new System.Drawing.Size(485, 314);
            this.gbcadleitor.TabIndex = 2;
            this.gbcadleitor.TabStop = false;
            this.gbcadleitor.Text = "Cadastrar Leitor";
            // 
            // dateleitorcaddatanasc
            // 
            this.dateleitorcaddatanasc.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateleitorcaddatanasc.Location = new System.Drawing.Point(120, 206);
            this.dateleitorcaddatanasc.Name = "dateleitorcaddatanasc";
            this.dateleitorcaddatanasc.Size = new System.Drawing.Size(236, 20);
            this.dateleitorcaddatanasc.TabIndex = 33;
            // 
            // btnleitorcadastrar
            // 
            this.btnleitorcadastrar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitorcadastrar.Location = new System.Drawing.Point(289, 257);
            this.btnleitorcadastrar.Name = "btnleitorcadastrar";
            this.btnleitorcadastrar.Size = new System.Drawing.Size(75, 31);
            this.btnleitorcadastrar.TabIndex = 19;
            this.btnleitorcadastrar.Text = "Cadastrar";
            this.btnleitorcadastrar.UseVisualStyleBackColor = true;
            this.btnleitorcadastrar.Click += new System.EventHandler(this.btnleitorcadastrar_Click);
            // 
            // btnleitorcadlimpar
            // 
            this.btnleitorcadlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnleitorcadlimpar.Location = new System.Drawing.Point(382, 257);
            this.btnleitorcadlimpar.Name = "btnleitorcadlimpar";
            this.btnleitorcadlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnleitorcadlimpar.TabIndex = 18;
            this.btnleitorcadlimpar.Text = "Limpar";
            this.btnleitorcadlimpar.UseVisualStyleBackColor = true;
            this.btnleitorcadlimpar.Click += new System.EventHandler(this.btnleitorcadlimpar_Click);
            // 
            // cbleitorcadstatus
            // 
            this.cbleitorcadstatus.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbleitorcadstatus.FormattingEnabled = true;
            this.cbleitorcadstatus.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbleitorcadstatus.Location = new System.Drawing.Point(289, 164);
            this.cbleitorcadstatus.Name = "cbleitorcadstatus";
            this.cbleitorcadstatus.Size = new System.Drawing.Size(167, 25);
            this.cbleitorcadstatus.TabIndex = 17;
            // 
            // txtleitorcadcpf
            // 
            this.txtleitorcadcpf.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitorcadcpf.Location = new System.Drawing.Point(275, 87);
            this.txtleitorcadcpf.Name = "txtleitorcadcpf";
            this.txtleitorcadcpf.Size = new System.Drawing.Size(182, 25);
            this.txtleitorcadcpf.TabIndex = 15;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(240, 93);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(38, 17);
            this.label25.TabIndex = 14;
            this.label25.Text = "CPF:";
            // 
            // txtleitorcadcontato
            // 
            this.txtleitorcadcontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitorcadcontato.Location = new System.Drawing.Point(80, 165);
            this.txtleitorcadcontato.Name = "txtleitorcadcontato";
            this.txtleitorcadcontato.Size = new System.Drawing.Size(149, 25);
            this.txtleitorcadcontato.TabIndex = 12;
            // 
            // txtleitorcademail
            // 
            this.txtleitorcademail.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitorcademail.Location = new System.Drawing.Point(65, 126);
            this.txtleitorcademail.Name = "txtleitorcademail";
            this.txtleitorcademail.Size = new System.Drawing.Size(392, 25);
            this.txtleitorcademail.TabIndex = 11;
            // 
            // txtleitorcadrg
            // 
            this.txtleitorcadrg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitorcadrg.Location = new System.Drawing.Point(68, 87);
            this.txtleitorcadrg.Name = "txtleitorcadrg";
            this.txtleitorcadrg.Size = new System.Drawing.Size(161, 25);
            this.txtleitorcadrg.TabIndex = 9;
            // 
            // txtleitorcadnome
            // 
            this.txtleitorcadnome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtleitorcadnome.Location = new System.Drawing.Point(68, 54);
            this.txtleitorcadnome.Name = "txtleitorcadnome";
            this.txtleitorcadnome.Size = new System.Drawing.Size(389, 25);
            this.txtleitorcadnome.TabIndex = 8;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(22, 170);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 17);
            this.label27.TabIndex = 5;
            this.label27.Text = "Contato:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(22, 210);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(97, 17);
            this.label28.TabIndex = 4;
            this.label28.Text = "Data de Nasc.:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(22, 130);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 17);
            this.label29.TabIndex = 3;
            this.label29.Text = "Email:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(236, 169);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(47, 17);
            this.label30.TabIndex = 2;
            this.label30.Text = "Status:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(22, 95);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 17);
            this.label31.TabIndex = 1;
            this.label31.Text = "RG:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(22, 57);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(47, 17);
            this.label32.TabIndex = 0;
            this.label32.Text = "Nome:";
            // 
            // gbautores
            // 
            this.gbautores.Controls.Add(this.gbcadautor);
            this.gbautores.Controls.Add(this.gbbuscarautor);
            this.gbautores.Controls.Add(this.gbexcluirautor);
            this.gbautores.Controls.Add(this.gbalterautor);
            this.gbautores.Location = new System.Drawing.Point(1, 27);
            this.gbautores.Name = "gbautores";
            this.gbautores.Size = new System.Drawing.Size(508, 344);
            this.gbautores.TabIndex = 22;
            this.gbautores.TabStop = false;
            this.gbautores.Text = "Autores";
            // 
            // gbcadautor
            // 
            this.gbcadautor.Controls.Add(this.dateautorcaddatanasc);
            this.gbcadautor.Controls.Add(this.dateautorcaddatafalesc);
            this.gbcadautor.Controls.Add(this.txtautorcadid);
            this.gbcadautor.Controls.Add(this.label37);
            this.gbcadautor.Controls.Add(this.btnautorcadastrar);
            this.gbcadautor.Controls.Add(this.btnautorcadlimpar);
            this.gbcadautor.Controls.Add(this.cbautorcadcategoria);
            this.gbcadautor.Controls.Add(this.txtautorcadcontato);
            this.gbcadautor.Controls.Add(this.txtautorcadnome);
            this.gbcadautor.Controls.Add(this.label39);
            this.gbcadautor.Controls.Add(this.label40);
            this.gbcadautor.Controls.Add(this.label41);
            this.gbcadautor.Controls.Add(this.label42);
            this.gbcadautor.Controls.Add(this.label44);
            this.gbcadautor.Location = new System.Drawing.Point(11, 19);
            this.gbcadautor.Name = "gbcadautor";
            this.gbcadautor.Size = new System.Drawing.Size(485, 314);
            this.gbcadautor.TabIndex = 2;
            this.gbcadautor.TabStop = false;
            this.gbcadautor.Text = "Cadastrar Autor";
            // 
            // dateautorcaddatanasc
            // 
            this.dateautorcaddatanasc.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateautorcaddatanasc.Location = new System.Drawing.Point(117, 113);
            this.dateautorcaddatanasc.Name = "dateautorcaddatanasc";
            this.dateautorcaddatanasc.Size = new System.Drawing.Size(236, 20);
            this.dateautorcaddatanasc.TabIndex = 34;
            // 
            // dateautorcaddatafalesc
            // 
            this.dateautorcaddatafalesc.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateautorcaddatafalesc.Location = new System.Drawing.Point(162, 145);
            this.dateautorcaddatafalesc.Name = "dateautorcaddatafalesc";
            this.dateautorcaddatafalesc.Size = new System.Drawing.Size(236, 20);
            this.dateautorcaddatafalesc.TabIndex = 33;
            // 
            // txtautorcadid
            // 
            this.txtautorcadid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautorcadid.Location = new System.Drawing.Point(69, 45);
            this.txtautorcadid.Name = "txtautorcadid";
            this.txtautorcadid.Size = new System.Drawing.Size(75, 25);
            this.txtautorcadid.TabIndex = 22;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(23, 48);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(27, 17);
            this.label37.TabIndex = 21;
            this.label37.Text = "ID:";
            // 
            // btnautorcadastrar
            // 
            this.btnautorcadastrar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautorcadastrar.Location = new System.Drawing.Point(289, 257);
            this.btnautorcadastrar.Name = "btnautorcadastrar";
            this.btnautorcadastrar.Size = new System.Drawing.Size(75, 31);
            this.btnautorcadastrar.TabIndex = 19;
            this.btnautorcadastrar.Text = "Cadastrar";
            this.btnautorcadastrar.UseVisualStyleBackColor = true;
            this.btnautorcadastrar.Click += new System.EventHandler(this.btnautorcadastrar_Click);
            // 
            // btnautorcadlimpar
            // 
            this.btnautorcadlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautorcadlimpar.Location = new System.Drawing.Point(381, 257);
            this.btnautorcadlimpar.Name = "btnautorcadlimpar";
            this.btnautorcadlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnautorcadlimpar.TabIndex = 18;
            this.btnautorcadlimpar.Text = "Limpar";
            this.btnautorcadlimpar.UseVisualStyleBackColor = true;
            this.btnautorcadlimpar.Click += new System.EventHandler(this.btnautorcadlimpar_Click);
            // 
            // cbautorcadcategoria
            // 
            this.cbautorcadcategoria.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbautorcadcategoria.FormattingEnabled = true;
            this.cbautorcadcategoria.Items.AddRange(new object[] {
            "Auto-Ajuda ",
            "Ciência ",
            "Culinária | Gastronomia ",
            "Fantasia | Ficção ",
            "Filosofia ",
            "Guerra ",
            "História",
            "Lingüistica ",
            "Lit. Estrangeira ",
            "Lit. Infanto-Juvenil",
            "Música ",
            "Política ",
            "Psicologia ",
            "Romance ",
            "Quadrinhos ",
            "Religião ",
            "Saúde ",
            "Terror ",
            "Vestibular ",
            "Cronica ",
            "Poesia | Poemas "});
            this.cbautorcadcategoria.Location = new System.Drawing.Point(150, 175);
            this.cbautorcadcategoria.Name = "cbautorcadcategoria";
            this.cbautorcadcategoria.Size = new System.Drawing.Size(193, 25);
            this.cbautorcadcategoria.TabIndex = 17;
            // 
            // txtautorcadcontato
            // 
            this.txtautorcadcontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautorcadcontato.Location = new System.Drawing.Point(85, 213);
            this.txtautorcadcontato.Name = "txtautorcadcontato";
            this.txtautorcadcontato.Size = new System.Drawing.Size(373, 25);
            this.txtautorcadcontato.TabIndex = 12;
            // 
            // txtautorcadnome
            // 
            this.txtautorcadnome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautorcadnome.Location = new System.Drawing.Point(68, 76);
            this.txtautorcadnome.Name = "txtautorcadnome";
            this.txtautorcadnome.Size = new System.Drawing.Size(389, 25);
            this.txtautorcadnome.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(23, 217);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(57, 17);
            this.label39.TabIndex = 5;
            this.label39.Text = "Contato:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(23, 113);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(97, 17);
            this.label40.TabIndex = 4;
            this.label40.Text = "Data de Nasc.:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(21, 178);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(123, 17);
            this.label41.TabIndex = 3;
            this.label41.Text = "Categoria Principal:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(22, 146);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(139, 17);
            this.label42.TabIndex = 2;
            this.label42.Text = "Data de Falescimento:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(22, 79);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(47, 17);
            this.label44.TabIndex = 0;
            this.label44.Text = "Nome:";
            // 
            // gbbuscarautor
            // 
            this.gbbuscarautor.Controls.Add(this.listBoxautor);
            this.gbbuscarautor.Controls.Add(this.btnautorbuscar);
            this.gbbuscarautor.Controls.Add(this.btnautorbuscarlimpar);
            this.gbbuscarautor.Controls.Add(this.txtautorbuscarid);
            this.gbbuscarautor.Controls.Add(this.label98);
            this.gbbuscarautor.Location = new System.Drawing.Point(11, 18);
            this.gbbuscarautor.Name = "gbbuscarautor";
            this.gbbuscarautor.Size = new System.Drawing.Size(485, 314);
            this.gbbuscarautor.TabIndex = 23;
            this.gbbuscarautor.TabStop = false;
            this.gbbuscarautor.Text = "Buscar Autor";
            // 
            // listBoxautor
            // 
            this.listBoxautor.FormattingEnabled = true;
            this.listBoxautor.Location = new System.Drawing.Point(25, 87);
            this.listBoxautor.Name = "listBoxautor";
            this.listBoxautor.Size = new System.Drawing.Size(430, 147);
            this.listBoxautor.TabIndex = 21;
            // 
            // btnautorbuscar
            // 
            this.btnautorbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautorbuscar.Location = new System.Drawing.Point(248, 22);
            this.btnautorbuscar.Name = "btnautorbuscar";
            this.btnautorbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnautorbuscar.TabIndex = 20;
            this.btnautorbuscar.Text = "Buscar";
            this.btnautorbuscar.UseVisualStyleBackColor = true;
            this.btnautorbuscar.Click += new System.EventHandler(this.btnautorbuscar_Click);
            // 
            // btnautorbuscarlimpar
            // 
            this.btnautorbuscarlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautorbuscarlimpar.Location = new System.Drawing.Point(381, 257);
            this.btnautorbuscarlimpar.Name = "btnautorbuscarlimpar";
            this.btnautorbuscarlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnautorbuscarlimpar.TabIndex = 18;
            this.btnautorbuscarlimpar.Text = "Limpar";
            this.btnautorbuscarlimpar.UseVisualStyleBackColor = true;
            this.btnautorbuscarlimpar.Click += new System.EventHandler(this.btnautorbuscarlimpar_Click);
            // 
            // txtautorbuscarid
            // 
            this.txtautorbuscarid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautorbuscarid.Location = new System.Drawing.Point(59, 26);
            this.txtautorbuscarid.Name = "txtautorbuscarid";
            this.txtautorbuscarid.Size = new System.Drawing.Size(180, 25);
            this.txtautorbuscarid.TabIndex = 9;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(22, 29);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(27, 17);
            this.label98.TabIndex = 1;
            this.label98.Text = "ID:";
            // 
            // gbexcluirautor
            // 
            this.gbexcluirautor.Controls.Add(this.dateautordeldatafalesc);
            this.gbexcluirautor.Controls.Add(this.dateautordeldatanasc);
            this.gbexcluirautor.Controls.Add(this.cbautordelcategoria);
            this.gbexcluirautor.Controls.Add(this.txtautordelcontato);
            this.gbexcluirautor.Controls.Add(this.txtautordelnome);
            this.gbexcluirautor.Controls.Add(this.label68);
            this.gbexcluirautor.Controls.Add(this.label69);
            this.gbexcluirautor.Controls.Add(this.label70);
            this.gbexcluirautor.Controls.Add(this.label71);
            this.gbexcluirautor.Controls.Add(this.label83);
            this.gbexcluirautor.Controls.Add(this.btnautordelbuscar);
            this.gbexcluirautor.Controls.Add(this.btnautorexcluir);
            this.gbexcluirautor.Controls.Add(this.btnautordellimpar);
            this.gbexcluirautor.Controls.Add(this.txtautordelid);
            this.gbexcluirautor.Controls.Add(this.label92);
            this.gbexcluirautor.Location = new System.Drawing.Point(11, 19);
            this.gbexcluirautor.Name = "gbexcluirautor";
            this.gbexcluirautor.Size = new System.Drawing.Size(485, 314);
            this.gbexcluirautor.TabIndex = 31;
            this.gbexcluirautor.TabStop = false;
            this.gbexcluirautor.Text = "Excluir Autor";
            // 
            // dateautordeldatafalesc
            // 
            this.dateautordeldatafalesc.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateautordeldatafalesc.Location = new System.Drawing.Point(167, 144);
            this.dateautordeldatafalesc.Name = "dateautordeldatafalesc";
            this.dateautordeldatafalesc.Size = new System.Drawing.Size(236, 20);
            this.dateautordeldatafalesc.TabIndex = 32;
            // 
            // dateautordeldatanasc
            // 
            this.dateautordeldatanasc.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateautordeldatanasc.Location = new System.Drawing.Point(125, 110);
            this.dateautordeldatanasc.Name = "dateautordeldatanasc";
            this.dateautordeldatanasc.Size = new System.Drawing.Size(236, 20);
            this.dateautordeldatanasc.TabIndex = 31;
            // 
            // cbautordelcategoria
            // 
            this.cbautordelcategoria.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbautordelcategoria.FormattingEnabled = true;
            this.cbautordelcategoria.Items.AddRange(new object[] {
            "Auto-Ajuda ",
            "Ciência ",
            "Culinária | Gastronomia ",
            "Fantasia | Ficção ",
            "Filosofia ",
            "Guerra ",
            "História",
            "Lingüistica ",
            "Lit. Estrangeira ",
            "Lit. Infanto-Juvenil",
            "Música ",
            "Política ",
            "Psicologia ",
            "Romance ",
            "Quadrinhos ",
            "Religião ",
            "Saúde ",
            "Terror ",
            "Vestibular ",
            "Cronica ",
            "Poesia | Poemas "});
            this.cbautordelcategoria.Location = new System.Drawing.Point(153, 175);
            this.cbautordelcategoria.Name = "cbautordelcategoria";
            this.cbautordelcategoria.Size = new System.Drawing.Size(193, 25);
            this.cbautordelcategoria.TabIndex = 29;
            // 
            // txtautordelcontato
            // 
            this.txtautordelcontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautordelcontato.Location = new System.Drawing.Point(88, 213);
            this.txtautordelcontato.Name = "txtautordelcontato";
            this.txtautordelcontato.Size = new System.Drawing.Size(373, 25);
            this.txtautordelcontato.TabIndex = 28;
            // 
            // txtautordelnome
            // 
            this.txtautordelnome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautordelnome.Location = new System.Drawing.Point(71, 76);
            this.txtautordelnome.Name = "txtautordelnome";
            this.txtautordelnome.Size = new System.Drawing.Size(389, 25);
            this.txtautordelnome.TabIndex = 26;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(26, 217);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(57, 17);
            this.label68.TabIndex = 25;
            this.label68.Text = "Contato:";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(26, 110);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(97, 17);
            this.label69.TabIndex = 24;
            this.label69.Text = "Data de Nasc.:";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(24, 178);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(123, 17);
            this.label70.TabIndex = 23;
            this.label70.Text = "Categoria Principal:";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(25, 146);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(139, 17);
            this.label71.TabIndex = 22;
            this.label71.Text = "Data de Falescimento:";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(25, 79);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(47, 17);
            this.label83.TabIndex = 21;
            this.label83.Text = "Nome:";
            // 
            // btnautordelbuscar
            // 
            this.btnautordelbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautordelbuscar.Location = new System.Drawing.Point(248, 22);
            this.btnautordelbuscar.Name = "btnautordelbuscar";
            this.btnautordelbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnautordelbuscar.TabIndex = 20;
            this.btnautordelbuscar.Text = "Buscar";
            this.btnautordelbuscar.UseVisualStyleBackColor = true;
            this.btnautordelbuscar.Click += new System.EventHandler(this.btnautordelbuscar_Click);
            // 
            // btnautorexcluir
            // 
            this.btnautorexcluir.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautorexcluir.Location = new System.Drawing.Point(289, 257);
            this.btnautorexcluir.Name = "btnautorexcluir";
            this.btnautorexcluir.Size = new System.Drawing.Size(75, 31);
            this.btnautorexcluir.TabIndex = 19;
            this.btnautorexcluir.Text = "Excluir";
            this.btnautorexcluir.UseVisualStyleBackColor = true;
            this.btnautorexcluir.Click += new System.EventHandler(this.btnautorexcluir_Click);
            // 
            // btnautordellimpar
            // 
            this.btnautordellimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautordellimpar.Location = new System.Drawing.Point(381, 257);
            this.btnautordellimpar.Name = "btnautordellimpar";
            this.btnautordellimpar.Size = new System.Drawing.Size(75, 31);
            this.btnautordellimpar.TabIndex = 18;
            this.btnautordellimpar.Text = "Limpar";
            this.btnautordellimpar.UseVisualStyleBackColor = true;
            this.btnautordellimpar.Click += new System.EventHandler(this.btnautordellimpar_Click);
            // 
            // txtautordelid
            // 
            this.txtautordelid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautordelid.Location = new System.Drawing.Point(59, 26);
            this.txtautordelid.Name = "txtautordelid";
            this.txtautordelid.Size = new System.Drawing.Size(180, 25);
            this.txtautordelid.TabIndex = 9;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(22, 29);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(27, 17);
            this.label92.TabIndex = 1;
            this.label92.Text = "ID:";
            // 
            // gbalterautor
            // 
            this.gbalterautor.Controls.Add(this.dateautoralterdatanasc);
            this.gbalterautor.Controls.Add(this.dateautoralterdatafales);
            this.gbalterautor.Controls.Add(this.cbautoraltercategoria);
            this.gbalterautor.Controls.Add(this.txtautoraltercontato);
            this.gbalterautor.Controls.Add(this.txtautoralternome);
            this.gbalterautor.Controls.Add(this.label18);
            this.gbalterautor.Controls.Add(this.label26);
            this.gbalterautor.Controls.Add(this.label33);
            this.gbalterautor.Controls.Add(this.label34);
            this.gbalterautor.Controls.Add(this.label35);
            this.gbalterautor.Controls.Add(this.btnautoralterbuscar);
            this.gbalterautor.Controls.Add(this.btnautoralterar);
            this.gbalterautor.Controls.Add(this.btnautoralterlimpar);
            this.gbalterautor.Controls.Add(this.txtautoralterid);
            this.gbalterautor.Controls.Add(this.label36);
            this.gbalterautor.Location = new System.Drawing.Point(11, 19);
            this.gbalterautor.Name = "gbalterautor";
            this.gbalterautor.Size = new System.Drawing.Size(485, 314);
            this.gbalterautor.TabIndex = 20;
            this.gbalterautor.TabStop = false;
            this.gbalterautor.Text = "Alterar Autor";
            // 
            // dateautoralterdatanasc
            // 
            this.dateautoralterdatanasc.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateautoralterdatanasc.Location = new System.Drawing.Point(125, 109);
            this.dateautoralterdatanasc.Name = "dateautoralterdatanasc";
            this.dateautoralterdatanasc.Size = new System.Drawing.Size(236, 20);
            this.dateautoralterdatanasc.TabIndex = 34;
            // 
            // dateautoralterdatafales
            // 
            this.dateautoralterdatafales.CalendarFont = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateautoralterdatafales.Location = new System.Drawing.Point(167, 145);
            this.dateautoralterdatafales.Name = "dateautoralterdatafales";
            this.dateautoralterdatafales.Size = new System.Drawing.Size(236, 20);
            this.dateautoralterdatafales.TabIndex = 33;
            // 
            // cbautoraltercategoria
            // 
            this.cbautoraltercategoria.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.cbautoraltercategoria.FormattingEnabled = true;
            this.cbautoraltercategoria.Items.AddRange(new object[] {
            "Auto-Ajuda ",
            "Ciência ",
            "Culinária | Gastronomia ",
            "Fantasia | Ficção ",
            "Filosofia ",
            "Guerra ",
            "História",
            "Lingüistica ",
            "Lit. Estrangeira ",
            "Lit. Infanto-Juvenil",
            "Música ",
            "Política ",
            "Psicologia ",
            "Romance ",
            "Quadrinhos ",
            "Religião ",
            "Saúde ",
            "Terror ",
            "Vestibular ",
            "Cronica ",
            "Poesia | Poemas "});
            this.cbautoraltercategoria.Location = new System.Drawing.Point(153, 175);
            this.cbautoraltercategoria.Name = "cbautoraltercategoria";
            this.cbautoraltercategoria.Size = new System.Drawing.Size(193, 25);
            this.cbautoraltercategoria.TabIndex = 29;
            // 
            // txtautoraltercontato
            // 
            this.txtautoraltercontato.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautoraltercontato.Location = new System.Drawing.Point(88, 213);
            this.txtautoraltercontato.Name = "txtautoraltercontato";
            this.txtautoraltercontato.Size = new System.Drawing.Size(373, 25);
            this.txtautoraltercontato.TabIndex = 28;
            // 
            // txtautoralternome
            // 
            this.txtautoralternome.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautoralternome.Location = new System.Drawing.Point(71, 76);
            this.txtautoralternome.Name = "txtautoralternome";
            this.txtautoralternome.Size = new System.Drawing.Size(389, 25);
            this.txtautoralternome.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(26, 217);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 17);
            this.label18.TabIndex = 25;
            this.label18.Text = "Contato:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(26, 110);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(97, 17);
            this.label26.TabIndex = 24;
            this.label26.Text = "Data de Nasc.:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(24, 178);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(123, 17);
            this.label33.TabIndex = 23;
            this.label33.Text = "Categoria Principal:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(25, 146);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(139, 17);
            this.label34.TabIndex = 22;
            this.label34.Text = "Data de Falescimento:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(25, 79);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(47, 17);
            this.label35.TabIndex = 21;
            this.label35.Text = "Nome:";
            // 
            // btnautoralterbuscar
            // 
            this.btnautoralterbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautoralterbuscar.Location = new System.Drawing.Point(248, 22);
            this.btnautoralterbuscar.Name = "btnautoralterbuscar";
            this.btnautoralterbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnautoralterbuscar.TabIndex = 20;
            this.btnautoralterbuscar.Text = "Buscar";
            this.btnautoralterbuscar.UseVisualStyleBackColor = true;
            this.btnautoralterbuscar.Click += new System.EventHandler(this.btnautoralterbuscar_Click);
            // 
            // btnautoralterar
            // 
            this.btnautoralterar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautoralterar.Location = new System.Drawing.Point(289, 257);
            this.btnautoralterar.Name = "btnautoralterar";
            this.btnautoralterar.Size = new System.Drawing.Size(75, 31);
            this.btnautoralterar.TabIndex = 19;
            this.btnautoralterar.Text = "Alterar";
            this.btnautoralterar.UseVisualStyleBackColor = true;
            this.btnautoralterar.Click += new System.EventHandler(this.btnautoralterar_Click);
            // 
            // btnautoralterlimpar
            // 
            this.btnautoralterlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnautoralterlimpar.Location = new System.Drawing.Point(381, 257);
            this.btnautoralterlimpar.Name = "btnautoralterlimpar";
            this.btnautoralterlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnautoralterlimpar.TabIndex = 18;
            this.btnautoralterlimpar.Text = "Limpar";
            this.btnautoralterlimpar.UseVisualStyleBackColor = true;
            this.btnautoralterlimpar.Click += new System.EventHandler(this.btnautoralterlimpar_Click);
            // 
            // txtautoralterid
            // 
            this.txtautoralterid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtautoralterid.Location = new System.Drawing.Point(59, 26);
            this.txtautoralterid.Name = "txtautoralterid";
            this.txtautoralterid.Size = new System.Drawing.Size(180, 25);
            this.txtautoralterid.TabIndex = 9;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(22, 29);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(27, 17);
            this.label36.TabIndex = 1;
            this.label36.Text = "ID:";
            // 
            // gblivros
            // 
            this.gblivros.Controls.Add(this.gbalterlivro);
            this.gblivros.Controls.Add(this.gbcadlivro);
            this.gblivros.Controls.Add(this.gbbuscarlivro);
            this.gblivros.Controls.Add(this.gbexcluirlivro);
            this.gblivros.Location = new System.Drawing.Point(1, 27);
            this.gblivros.Name = "gblivros";
            this.gblivros.Size = new System.Drawing.Size(508, 344);
            this.gblivros.TabIndex = 21;
            this.gblivros.TabStop = false;
            this.gblivros.Text = "Livros";
            // 
            // gbbuscarlivro
            // 
            this.gbbuscarlivro.Controls.Add(this.listBoxlivro);
            this.gbbuscarlivro.Controls.Add(this.btnlivrobuscar);
            this.gbbuscarlivro.Controls.Add(this.btnlivrobuscarlimpar);
            this.gbbuscarlivro.Controls.Add(this.txtlivrobuscarid);
            this.gbbuscarlivro.Controls.Add(this.label84);
            this.gbbuscarlivro.Location = new System.Drawing.Point(11, 19);
            this.gbbuscarlivro.Name = "gbbuscarlivro";
            this.gbbuscarlivro.Size = new System.Drawing.Size(485, 314);
            this.gbbuscarlivro.TabIndex = 32;
            this.gbbuscarlivro.TabStop = false;
            this.gbbuscarlivro.Text = "Buscar Livro";
            // 
            // listBoxlivro
            // 
            this.listBoxlivro.FormattingEnabled = true;
            this.listBoxlivro.Location = new System.Drawing.Point(24, 76);
            this.listBoxlivro.Name = "listBoxlivro";
            this.listBoxlivro.Size = new System.Drawing.Size(430, 160);
            this.listBoxlivro.TabIndex = 21;
            // 
            // btnlivrobuscar
            // 
            this.btnlivrobuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnlivrobuscar.Location = new System.Drawing.Point(248, 15);
            this.btnlivrobuscar.Name = "btnlivrobuscar";
            this.btnlivrobuscar.Size = new System.Drawing.Size(75, 31);
            this.btnlivrobuscar.TabIndex = 20;
            this.btnlivrobuscar.Text = "Buscar";
            this.btnlivrobuscar.UseVisualStyleBackColor = true;
            this.btnlivrobuscar.Click += new System.EventHandler(this.btnlivrobuscar_Click);
            // 
            // btnlivrobuscarlimpar
            // 
            this.btnlivrobuscarlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnlivrobuscarlimpar.Location = new System.Drawing.Point(382, 257);
            this.btnlivrobuscarlimpar.Name = "btnlivrobuscarlimpar";
            this.btnlivrobuscarlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnlivrobuscarlimpar.TabIndex = 18;
            this.btnlivrobuscarlimpar.Text = "Limpar";
            this.btnlivrobuscarlimpar.UseVisualStyleBackColor = true;
            this.btnlivrobuscarlimpar.Click += new System.EventHandler(this.btnlivrobuscarlimpar_Click);
            // 
            // txtlivrobuscarid
            // 
            this.txtlivrobuscarid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrobuscarid.Location = new System.Drawing.Point(59, 19);
            this.txtlivrobuscarid.Name = "txtlivrobuscarid";
            this.txtlivrobuscarid.Size = new System.Drawing.Size(180, 25);
            this.txtlivrobuscarid.TabIndex = 9;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(22, 22);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(27, 17);
            this.label84.TabIndex = 1;
            this.label84.Text = "ID:";
            // 
            // gbexcluirlivro
            // 
            this.gbexcluirlivro.Controls.Add(this.cblivrodelcaegoria);
            this.gbexcluirlivro.Controls.Add(this.txtlivrodelquant);
            this.gbexcluirlivro.Controls.Add(this.label108);
            this.gbexcluirlivro.Controls.Add(this.label103);
            this.gbexcluirlivro.Controls.Add(this.txtlivrodeleditora);
            this.gbexcluirlivro.Controls.Add(this.label104);
            this.gbexcluirlivro.Controls.Add(this.txtlivrodeldescricao);
            this.gbexcluirlivro.Controls.Add(this.txtlivrodelano);
            this.gbexcluirlivro.Controls.Add(this.txtlivrodeledicao);
            this.gbexcluirlivro.Controls.Add(this.txtlivrodelidautor);
            this.gbexcluirlivro.Controls.Add(this.txtlivrodeltitulo);
            this.gbexcluirlivro.Controls.Add(this.label85);
            this.gbexcluirlivro.Controls.Add(this.label86);
            this.gbexcluirlivro.Controls.Add(this.label87);
            this.gbexcluirlivro.Controls.Add(this.label88);
            this.gbexcluirlivro.Controls.Add(this.label90);
            this.gbexcluirlivro.Controls.Add(this.btndelbuscar);
            this.gbexcluirlivro.Controls.Add(this.btnlivroexcluir);
            this.gbexcluirlivro.Controls.Add(this.btndellivrolimpar);
            this.gbexcluirlivro.Controls.Add(this.txtlivrodelid);
            this.gbexcluirlivro.Controls.Add(this.label91);
            this.gbexcluirlivro.Location = new System.Drawing.Point(11, 19);
            this.gbexcluirlivro.Name = "gbexcluirlivro";
            this.gbexcluirlivro.Size = new System.Drawing.Size(485, 314);
            this.gbexcluirlivro.TabIndex = 32;
            this.gbexcluirlivro.TabStop = false;
            this.gbexcluirlivro.Text = "Excluir Livro";
            // 
            // cblivrodelcaegoria
            // 
            this.cblivrodelcaegoria.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cblivrodelcaegoria.FormattingEnabled = true;
            this.cblivrodelcaegoria.Items.AddRange(new object[] {
            "Auto-Ajuda ",
            "Ciência ",
            "Culinária | Gastronomia ",
            "Fantasia | Ficção ",
            "Filosofia ",
            "Guerra ",
            "História",
            "Lingüistica ",
            "Lit. Estrangeira ",
            "Lit. Infanto-Juvenil",
            "Música ",
            "Política ",
            "Psicologia ",
            "Romance ",
            "Quadrinhos ",
            "Religião ",
            "Saúde ",
            "Terror ",
            "Vestibular ",
            "Cronica ",
            "Poesia | Poemas "});
            this.cblivrodelcaegoria.Location = new System.Drawing.Point(271, 126);
            this.cblivrodelcaegoria.Name = "cblivrodelcaegoria";
            this.cblivrodelcaegoria.Size = new System.Drawing.Size(182, 25);
            this.cblivrodelcaegoria.TabIndex = 38;
            // 
            // txtlivrodelquant
            // 
            this.txtlivrodelquant.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrodelquant.Location = new System.Drawing.Point(329, 162);
            this.txtlivrodelquant.Name = "txtlivrodelquant";
            this.txtlivrodelquant.Size = new System.Drawing.Size(84, 25);
            this.txtlivrodelquant.TabIndex = 37;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(249, 166);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(78, 17);
            this.label108.TabIndex = 36;
            this.label108.Text = "Quantidade:";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(202, 130);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(68, 17);
            this.label103.TabIndex = 34;
            this.label103.Text = "Categoria:";
            // 
            // txtlivrodeleditora
            // 
            this.txtlivrodeleditora.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrodeleditora.Location = new System.Drawing.Point(70, 161);
            this.txtlivrodeleditora.Name = "txtlivrodeleditora";
            this.txtlivrodeleditora.Size = new System.Drawing.Size(167, 25);
            this.txtlivrodeleditora.TabIndex = 33;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(19, 167);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(53, 17);
            this.label104.TabIndex = 32;
            this.label104.Text = "Editora:";
            // 
            // txtlivrodeldescricao
            // 
            this.txtlivrodeldescricao.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.txtlivrodeldescricao.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrodeldescricao.Location = new System.Drawing.Point(98, 201);
            this.txtlivrodeldescricao.Multiline = true;
            this.txtlivrodeldescricao.Name = "txtlivrodeldescricao";
            this.txtlivrodeldescricao.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtlivrodeldescricao.Size = new System.Drawing.Size(359, 58);
            this.txtlivrodeldescricao.TabIndex = 31;
            // 
            // txtlivrodelano
            // 
            this.txtlivrodelano.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrodelano.Location = new System.Drawing.Point(64, 126);
            this.txtlivrodelano.Name = "txtlivrodelano";
            this.txtlivrodelano.Size = new System.Drawing.Size(84, 25);
            this.txtlivrodelano.TabIndex = 30;
            // 
            // txtlivrodeledicao
            // 
            this.txtlivrodeledicao.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrodeledicao.Location = new System.Drawing.Point(257, 93);
            this.txtlivrodeledicao.Name = "txtlivrodeledicao";
            this.txtlivrodeledicao.Size = new System.Drawing.Size(114, 25);
            this.txtlivrodeledicao.TabIndex = 29;
            // 
            // txtlivrodelidautor
            // 
            this.txtlivrodelidautor.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrodelidautor.Location = new System.Drawing.Point(106, 92);
            this.txtlivrodelidautor.Name = "txtlivrodelidautor";
            this.txtlivrodelidautor.Size = new System.Drawing.Size(91, 25);
            this.txtlivrodelidautor.TabIndex = 28;
            // 
            // txtlivrodeltitulo
            // 
            this.txtlivrodeltitulo.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrodeltitulo.Location = new System.Drawing.Point(67, 54);
            this.txtlivrodeltitulo.Name = "txtlivrodeltitulo";
            this.txtlivrodeltitulo.Size = new System.Drawing.Size(389, 25);
            this.txtlivrodeltitulo.TabIndex = 27;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(21, 206);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(71, 17);
            this.label85.TabIndex = 26;
            this.label85.Text = "Descrição:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(205, 96);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(51, 17);
            this.label86.TabIndex = 25;
            this.label86.Text = "Edição:";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(21, 130);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(36, 17);
            this.label87.TabIndex = 24;
            this.label87.Text = "Ano:";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(21, 95);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(82, 17);
            this.label88.TabIndex = 23;
            this.label88.Text = "ID do Autor:";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(21, 57);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(44, 17);
            this.label90.TabIndex = 22;
            this.label90.Text = "Título:";
            // 
            // btndelbuscar
            // 
            this.btndelbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btndelbuscar.Location = new System.Drawing.Point(248, 15);
            this.btndelbuscar.Name = "btndelbuscar";
            this.btndelbuscar.Size = new System.Drawing.Size(75, 31);
            this.btndelbuscar.TabIndex = 20;
            this.btndelbuscar.Text = "Buscar";
            this.btndelbuscar.UseVisualStyleBackColor = true;
            this.btndelbuscar.Click += new System.EventHandler(this.btndelbuscar_Click);
            // 
            // btnlivroexcluir
            // 
            this.btnlivroexcluir.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnlivroexcluir.Location = new System.Drawing.Point(289, 276);
            this.btnlivroexcluir.Name = "btnlivroexcluir";
            this.btnlivroexcluir.Size = new System.Drawing.Size(75, 31);
            this.btnlivroexcluir.TabIndex = 19;
            this.btnlivroexcluir.Text = "Excluir";
            this.btnlivroexcluir.UseVisualStyleBackColor = true;
            this.btnlivroexcluir.Click += new System.EventHandler(this.btnlivroexcluir_Click);
            // 
            // btndellivrolimpar
            // 
            this.btndellivrolimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btndellivrolimpar.Location = new System.Drawing.Point(382, 276);
            this.btndellivrolimpar.Name = "btndellivrolimpar";
            this.btndellivrolimpar.Size = new System.Drawing.Size(75, 31);
            this.btndellivrolimpar.TabIndex = 18;
            this.btndellivrolimpar.Text = "Limpar";
            this.btndellivrolimpar.UseVisualStyleBackColor = true;
            this.btndellivrolimpar.Click += new System.EventHandler(this.btndellivrolimpar_Click);
            // 
            // txtlivrodelid
            // 
            this.txtlivrodelid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrodelid.Location = new System.Drawing.Point(59, 19);
            this.txtlivrodelid.Name = "txtlivrodelid";
            this.txtlivrodelid.Size = new System.Drawing.Size(180, 25);
            this.txtlivrodelid.TabIndex = 9;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(22, 22);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(27, 17);
            this.label91.TabIndex = 1;
            this.label91.Text = "ID:";
            // 
            // gbalterlivro
            // 
            this.gbalterlivro.Controls.Add(this.cblivroaltercategoria);
            this.gbalterlivro.Controls.Add(this.txtlivroalterquant);
            this.gbalterlivro.Controls.Add(this.label109);
            this.gbalterlivro.Controls.Add(this.label105);
            this.gbalterlivro.Controls.Add(this.txtlivroaltereditora);
            this.gbalterlivro.Controls.Add(this.label107);
            this.gbalterlivro.Controls.Add(this.txtlivroalterdescricao);
            this.gbalterlivro.Controls.Add(this.txtlivroalterano);
            this.gbalterlivro.Controls.Add(this.txtlivroalteredicao);
            this.gbalterlivro.Controls.Add(this.txtlivroalteridautor);
            this.gbalterlivro.Controls.Add(this.txtlivroaltertitulo);
            this.gbalterlivro.Controls.Add(this.label52);
            this.gbalterlivro.Controls.Add(this.label53);
            this.gbalterlivro.Controls.Add(this.label54);
            this.gbalterlivro.Controls.Add(this.label55);
            this.gbalterlivro.Controls.Add(this.label56);
            this.gbalterlivro.Controls.Add(this.btnlivroalterbuscar);
            this.gbalterlivro.Controls.Add(this.btnlivroalterar);
            this.gbalterlivro.Controls.Add(this.btnlivroalterlimpar);
            this.gbalterlivro.Controls.Add(this.txtlivroalterid);
            this.gbalterlivro.Controls.Add(this.label57);
            this.gbalterlivro.Location = new System.Drawing.Point(11, 19);
            this.gbalterlivro.Name = "gbalterlivro";
            this.gbalterlivro.Size = new System.Drawing.Size(485, 314);
            this.gbalterlivro.TabIndex = 20;
            this.gbalterlivro.TabStop = false;
            this.gbalterlivro.Text = "Alterar Livro";
            // 
            // cblivroaltercategoria
            // 
            this.cblivroaltercategoria.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cblivroaltercategoria.FormattingEnabled = true;
            this.cblivroaltercategoria.Items.AddRange(new object[] {
            "Auto-Ajuda ",
            "Ciência ",
            "Culinária | Gastronomia ",
            "Fantasia | Ficção ",
            "Filosofia ",
            "Guerra ",
            "História",
            "Lingüistica ",
            "Lit. Estrangeira ",
            "Lit. Infanto-Juvenil",
            "Música ",
            "Política ",
            "Psicologia ",
            "Romance ",
            "Quadrinhos ",
            "Religião ",
            "Saúde ",
            "Terror ",
            "Vestibular ",
            "Cronica ",
            "Poesia | Poemas "});
            this.cblivroaltercategoria.Location = new System.Drawing.Point(275, 125);
            this.cblivroaltercategoria.Name = "cblivroaltercategoria";
            this.cblivroaltercategoria.Size = new System.Drawing.Size(182, 25);
            this.cblivroaltercategoria.TabIndex = 40;
            // 
            // txtlivroalterquant
            // 
            this.txtlivroalterquant.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivroalterquant.Location = new System.Drawing.Point(329, 161);
            this.txtlivroalterquant.Name = "txtlivroalterquant";
            this.txtlivroalterquant.Size = new System.Drawing.Size(84, 25);
            this.txtlivroalterquant.TabIndex = 39;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(249, 165);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(78, 17);
            this.label109.TabIndex = 38;
            this.label109.Text = "Quantidade:";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(203, 128);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(68, 17);
            this.label105.TabIndex = 34;
            this.label105.Text = "Categoria:";
            // 
            // txtlivroaltereditora
            // 
            this.txtlivroaltereditora.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivroaltereditora.Location = new System.Drawing.Point(71, 159);
            this.txtlivroaltereditora.Name = "txtlivroaltereditora";
            this.txtlivroaltereditora.Size = new System.Drawing.Size(167, 25);
            this.txtlivroaltereditora.TabIndex = 33;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(20, 165);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(53, 17);
            this.label107.TabIndex = 32;
            this.label107.Text = "Editora:";
            // 
            // txtlivroalterdescricao
            // 
            this.txtlivroalterdescricao.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.txtlivroalterdescricao.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivroalterdescricao.Location = new System.Drawing.Point(98, 202);
            this.txtlivroalterdescricao.Multiline = true;
            this.txtlivroalterdescricao.Name = "txtlivroalterdescricao";
            this.txtlivroalterdescricao.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtlivroalterdescricao.Size = new System.Drawing.Size(359, 58);
            this.txtlivroalterdescricao.TabIndex = 31;
            // 
            // txtlivroalterano
            // 
            this.txtlivroalterano.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivroalterano.Location = new System.Drawing.Point(64, 127);
            this.txtlivroalterano.Name = "txtlivroalterano";
            this.txtlivroalterano.Size = new System.Drawing.Size(83, 25);
            this.txtlivroalterano.TabIndex = 30;
            // 
            // txtlivroalteredicao
            // 
            this.txtlivroalteredicao.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivroalteredicao.Location = new System.Drawing.Point(257, 94);
            this.txtlivroalteredicao.Name = "txtlivroalteredicao";
            this.txtlivroalteredicao.Size = new System.Drawing.Size(114, 25);
            this.txtlivroalteredicao.TabIndex = 29;
            // 
            // txtlivroalteridautor
            // 
            this.txtlivroalteridautor.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivroalteridautor.Location = new System.Drawing.Point(106, 93);
            this.txtlivroalteridautor.Name = "txtlivroalteridautor";
            this.txtlivroalteridautor.Size = new System.Drawing.Size(91, 25);
            this.txtlivroalteridautor.TabIndex = 28;
            // 
            // txtlivroaltertitulo
            // 
            this.txtlivroaltertitulo.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivroaltertitulo.Location = new System.Drawing.Point(67, 55);
            this.txtlivroaltertitulo.Name = "txtlivroaltertitulo";
            this.txtlivroaltertitulo.Size = new System.Drawing.Size(389, 25);
            this.txtlivroaltertitulo.TabIndex = 27;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(21, 207);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(71, 17);
            this.label52.TabIndex = 26;
            this.label52.Text = "Descrição:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(205, 97);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(51, 17);
            this.label53.TabIndex = 25;
            this.label53.Text = "Edição:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(21, 131);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(36, 17);
            this.label54.TabIndex = 24;
            this.label54.Text = "Ano:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(21, 96);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(82, 17);
            this.label55.TabIndex = 23;
            this.label55.Text = "ID do Autor:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(21, 58);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(44, 17);
            this.label56.TabIndex = 22;
            this.label56.Text = "Título:";
            // 
            // btnlivroalterbuscar
            // 
            this.btnlivroalterbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnlivroalterbuscar.Location = new System.Drawing.Point(248, 15);
            this.btnlivroalterbuscar.Name = "btnlivroalterbuscar";
            this.btnlivroalterbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnlivroalterbuscar.TabIndex = 20;
            this.btnlivroalterbuscar.Text = "Buscar";
            this.btnlivroalterbuscar.UseVisualStyleBackColor = true;
            this.btnlivroalterbuscar.Click += new System.EventHandler(this.btnlivroalterbuscar_Click);
            // 
            // btnlivroalterar
            // 
            this.btnlivroalterar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnlivroalterar.Location = new System.Drawing.Point(289, 277);
            this.btnlivroalterar.Name = "btnlivroalterar";
            this.btnlivroalterar.Size = new System.Drawing.Size(75, 31);
            this.btnlivroalterar.TabIndex = 19;
            this.btnlivroalterar.Text = "Alterar";
            this.btnlivroalterar.UseVisualStyleBackColor = true;
            this.btnlivroalterar.Click += new System.EventHandler(this.btnlivroalterar_Click);
            // 
            // btnlivroalterlimpar
            // 
            this.btnlivroalterlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnlivroalterlimpar.Location = new System.Drawing.Point(382, 277);
            this.btnlivroalterlimpar.Name = "btnlivroalterlimpar";
            this.btnlivroalterlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnlivroalterlimpar.TabIndex = 18;
            this.btnlivroalterlimpar.Text = "Limpar";
            this.btnlivroalterlimpar.UseVisualStyleBackColor = true;
            this.btnlivroalterlimpar.Click += new System.EventHandler(this.btnlivroalterlimpar_Click);
            // 
            // txtlivroalterid
            // 
            this.txtlivroalterid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivroalterid.Location = new System.Drawing.Point(59, 19);
            this.txtlivroalterid.Name = "txtlivroalterid";
            this.txtlivroalterid.Size = new System.Drawing.Size(180, 25);
            this.txtlivroalterid.TabIndex = 9;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(22, 22);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(27, 17);
            this.label57.TabIndex = 1;
            this.label57.Text = "ID:";
            // 
            // gbcadlivro
            // 
            this.gbcadlivro.Controls.Add(this.cblivrocadcategoria);
            this.gbcadlivro.Controls.Add(this.txtlivrocadquant);
            this.gbcadlivro.Controls.Add(this.label110);
            this.gbcadlivro.Controls.Add(this.label102);
            this.gbcadlivro.Controls.Add(this.txtlivrocadeditora);
            this.gbcadlivro.Controls.Add(this.label101);
            this.gbcadlivro.Controls.Add(this.txtlivrocadid);
            this.gbcadlivro.Controls.Add(this.label75);
            this.gbcadlivro.Controls.Add(this.btnlivrocadastrar);
            this.gbcadlivro.Controls.Add(this.btnlivrocadlimpar);
            this.gbcadlivro.Controls.Add(this.txtlivrocaddescricao);
            this.gbcadlivro.Controls.Add(this.txtlivrocadano);
            this.gbcadlivro.Controls.Add(this.txtlivrocadedicao);
            this.gbcadlivro.Controls.Add(this.txtlivrocadidautor);
            this.gbcadlivro.Controls.Add(this.txtlivrocadtitulo);
            this.gbcadlivro.Controls.Add(this.label45);
            this.gbcadlivro.Controls.Add(this.label46);
            this.gbcadlivro.Controls.Add(this.label47);
            this.gbcadlivro.Controls.Add(this.label49);
            this.gbcadlivro.Controls.Add(this.label50);
            this.gbcadlivro.Location = new System.Drawing.Point(11, 19);
            this.gbcadlivro.Name = "gbcadlivro";
            this.gbcadlivro.Size = new System.Drawing.Size(485, 314);
            this.gbcadlivro.TabIndex = 2;
            this.gbcadlivro.TabStop = false;
            this.gbcadlivro.Text = "Cadastrar Livro";
            // 
            // cblivrocadcategoria
            // 
            this.cblivrocadcategoria.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cblivrocadcategoria.FormattingEnabled = true;
            this.cblivrocadcategoria.Items.AddRange(new object[] {
            "Auto-Ajuda ",
            "Ciência ",
            "Culinária | Gastronomia ",
            "Fantasia | Ficção ",
            "Filosofia ",
            "Guerra ",
            "História",
            "Lingüistica ",
            "Lit. Estrangeira ",
            "Lit. Infanto-Juvenil",
            "Música ",
            "Política ",
            "Psicologia ",
            "Romance ",
            "Quadrinhos ",
            "Religião ",
            "Saúde ",
            "Terror ",
            "Vestibular ",
            "Cronica ",
            "Poesia | Poemas "});
            this.cblivrocadcategoria.Location = new System.Drawing.Point(277, 119);
            this.cblivrocadcategoria.Name = "cblivrocadcategoria";
            this.cblivrocadcategoria.Size = new System.Drawing.Size(182, 25);
            this.cblivrocadcategoria.TabIndex = 40;
            // 
            // txtlivrocadquant
            // 
            this.txtlivrocadquant.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrocadquant.Location = new System.Drawing.Point(333, 159);
            this.txtlivrocadquant.Name = "txtlivrocadquant";
            this.txtlivrocadquant.Size = new System.Drawing.Size(84, 25);
            this.txtlivrocadquant.TabIndex = 39;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(253, 163);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(78, 17);
            this.label110.TabIndex = 38;
            this.label110.Text = "Quantidade:";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(208, 126);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(68, 17);
            this.label102.TabIndex = 24;
            this.label102.Text = "Categoria:";
            // 
            // txtlivrocadeditora
            // 
            this.txtlivrocadeditora.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrocadeditora.Location = new System.Drawing.Point(76, 157);
            this.txtlivrocadeditora.Name = "txtlivrocadeditora";
            this.txtlivrocadeditora.Size = new System.Drawing.Size(167, 25);
            this.txtlivrocadeditora.TabIndex = 23;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(25, 163);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(53, 17);
            this.label101.TabIndex = 22;
            this.label101.Text = "Editora:";
            // 
            // txtlivrocadid
            // 
            this.txtlivrocadid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrocadid.Location = new System.Drawing.Point(71, 21);
            this.txtlivrocadid.Name = "txtlivrocadid";
            this.txtlivrocadid.Size = new System.Drawing.Size(75, 25);
            this.txtlivrocadid.TabIndex = 21;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(25, 24);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(27, 17);
            this.label75.TabIndex = 20;
            this.label75.Text = "ID:";
            // 
            // btnlivrocadastrar
            // 
            this.btnlivrocadastrar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnlivrocadastrar.Location = new System.Drawing.Point(268, 276);
            this.btnlivrocadastrar.Name = "btnlivrocadastrar";
            this.btnlivrocadastrar.Size = new System.Drawing.Size(75, 31);
            this.btnlivrocadastrar.TabIndex = 19;
            this.btnlivrocadastrar.Text = "Cadastrar";
            this.btnlivrocadastrar.UseVisualStyleBackColor = true;
            this.btnlivrocadastrar.Click += new System.EventHandler(this.btnlivrocadastrar_Click);
            // 
            // btnlivrocadlimpar
            // 
            this.btnlivrocadlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnlivrocadlimpar.Location = new System.Drawing.Point(356, 277);
            this.btnlivrocadlimpar.Name = "btnlivrocadlimpar";
            this.btnlivrocadlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnlivrocadlimpar.TabIndex = 18;
            this.btnlivrocadlimpar.Text = "Limpar";
            this.btnlivrocadlimpar.UseVisualStyleBackColor = true;
            this.btnlivrocadlimpar.Click += new System.EventHandler(this.txtfunccadlimpar_Click);
            // 
            // txtlivrocaddescricao
            // 
            this.txtlivrocaddescricao.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.txtlivrocaddescricao.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrocaddescricao.Location = new System.Drawing.Point(106, 212);
            this.txtlivrocaddescricao.Multiline = true;
            this.txtlivrocaddescricao.Name = "txtlivrocaddescricao";
            this.txtlivrocaddescricao.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtlivrocaddescricao.Size = new System.Drawing.Size(359, 58);
            this.txtlivrocaddescricao.TabIndex = 12;
            // 
            // txtlivrocadano
            // 
            this.txtlivrocadano.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrocadano.Location = new System.Drawing.Point(60, 123);
            this.txtlivrocadano.Name = "txtlivrocadano";
            this.txtlivrocadano.Size = new System.Drawing.Size(84, 25);
            this.txtlivrocadano.TabIndex = 11;
            // 
            // txtlivrocadedicao
            // 
            this.txtlivrocadedicao.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrocadedicao.Location = new System.Drawing.Point(262, 87);
            this.txtlivrocadedicao.Name = "txtlivrocadedicao";
            this.txtlivrocadedicao.Size = new System.Drawing.Size(114, 25);
            this.txtlivrocadedicao.TabIndex = 10;
            // 
            // txtlivrocadidautor
            // 
            this.txtlivrocadidautor.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrocadidautor.Location = new System.Drawing.Point(109, 89);
            this.txtlivrocadidautor.Name = "txtlivrocadidautor";
            this.txtlivrocadidautor.Size = new System.Drawing.Size(91, 25);
            this.txtlivrocadidautor.TabIndex = 9;
            // 
            // txtlivrocadtitulo
            // 
            this.txtlivrocadtitulo.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtlivrocadtitulo.Location = new System.Drawing.Point(70, 51);
            this.txtlivrocadtitulo.Name = "txtlivrocadtitulo";
            this.txtlivrocadtitulo.Size = new System.Drawing.Size(389, 25);
            this.txtlivrocadtitulo.TabIndex = 8;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(20, 218);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(71, 17);
            this.label45.TabIndex = 5;
            this.label45.Text = "Descrição:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(208, 93);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(51, 17);
            this.label46.TabIndex = 4;
            this.label46.Text = "Edição:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(24, 127);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(36, 17);
            this.label47.TabIndex = 3;
            this.label47.Text = "Ano:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(24, 92);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(82, 17);
            this.label49.TabIndex = 1;
            this.label49.Text = "ID do Autor:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(24, 54);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(44, 17);
            this.label50.TabIndex = 0;
            this.label50.Text = "Título:";
            // 
            // gbemprestimo
            // 
            this.gbemprestimo.Controls.Add(this.gbalterempres);
            this.gbemprestimo.Controls.Add(this.gbcadempres);
            this.gbemprestimo.Controls.Add(this.gbbuscarempres);
            this.gbemprestimo.Controls.Add(this.gbexcluirempres);
            this.gbemprestimo.Location = new System.Drawing.Point(1, 27);
            this.gbemprestimo.Name = "gbemprestimo";
            this.gbemprestimo.Size = new System.Drawing.Size(508, 344);
            this.gbemprestimo.TabIndex = 23;
            this.gbemprestimo.TabStop = false;
            this.gbemprestimo.Text = "Empréstimos";
            // 
            // gbalterempres
            // 
            this.gbalterempres.Controls.Add(this.txtempresalterrgleitor);
            this.gbalterempres.Controls.Add(this.label73);
            this.gbalterempres.Controls.Add(this.btnempresalterbuscar);
            this.gbalterempres.Controls.Add(this.txtempresalteridempres);
            this.gbalterempres.Controls.Add(this.label113);
            this.gbalterempres.Controls.Add(this.dateempresalterdatdev);
            this.gbalterempres.Controls.Add(this.dateempresalterdataret);
            this.gbalterempres.Controls.Add(this.txtempresalteridlivro);
            this.gbalterempres.Controls.Add(this.label38);
            this.gbalterempres.Controls.Add(this.txtempresalterrgfunc);
            this.gbalterempres.Controls.Add(this.label43);
            this.gbalterempres.Controls.Add(this.label48);
            this.gbalterempres.Controls.Add(this.label61);
            this.gbalterempres.Controls.Add(this.btnempresalterar);
            this.gbalterempres.Controls.Add(this.btnempresalterlimpar);
            this.gbalterempres.Location = new System.Drawing.Point(12, 18);
            this.gbalterempres.Name = "gbalterempres";
            this.gbalterempres.Size = new System.Drawing.Size(485, 314);
            this.gbalterempres.TabIndex = 20;
            this.gbalterempres.TabStop = false;
            this.gbalterempres.Text = "Alterar Empréstimo";
            // 
            // txtempresalterrgleitor
            // 
            this.txtempresalterrgleitor.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresalterrgleitor.Location = new System.Drawing.Point(111, 56);
            this.txtempresalterrgleitor.Name = "txtempresalterrgleitor";
            this.txtempresalterrgleitor.Size = new System.Drawing.Size(161, 25);
            this.txtempresalterrgleitor.TabIndex = 38;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(25, 61);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(88, 17);
            this.label73.TabIndex = 37;
            this.label73.Text = "RG do Leitor:";
            // 
            // btnempresalterbuscar
            // 
            this.btnempresalterbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnempresalterbuscar.Location = new System.Drawing.Point(239, 14);
            this.btnempresalterbuscar.Name = "btnempresalterbuscar";
            this.btnempresalterbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnempresalterbuscar.TabIndex = 36;
            this.btnempresalterbuscar.Text = "Buscar";
            this.btnempresalterbuscar.UseVisualStyleBackColor = true;
            this.btnempresalterbuscar.Click += new System.EventHandler(this.btnempresalterbuscar_Click);
            // 
            // txtempresalteridempres
            // 
            this.txtempresalteridempres.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresalteridempres.Location = new System.Drawing.Point(141, 19);
            this.txtempresalteridempres.Name = "txtempresalteridempres";
            this.txtempresalteridempres.Size = new System.Drawing.Size(83, 25);
            this.txtempresalteridempres.TabIndex = 35;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(21, 22);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(119, 17);
            this.label113.TabIndex = 34;
            this.label113.Text = "ID do Empréstimo:";
            // 
            // dateempresalterdatdev
            // 
            this.dateempresalterdatdev.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateempresalterdatdev.Location = new System.Drawing.Point(152, 193);
            this.dateempresalterdatdev.Name = "dateempresalterdatdev";
            this.dateempresalterdatdev.Size = new System.Drawing.Size(275, 25);
            this.dateempresalterdatdev.TabIndex = 32;
            // 
            // dateempresalterdataret
            // 
            this.dateempresalterdataret.Enabled = false;
            this.dateempresalterdataret.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateempresalterdataret.Location = new System.Drawing.Point(152, 159);
            this.dateempresalterdataret.Name = "dateempresalterdataret";
            this.dateempresalterdataret.Size = new System.Drawing.Size(275, 25);
            this.dateempresalterdataret.TabIndex = 31;
            // 
            // txtempresalteridlivro
            // 
            this.txtempresalteridlivro.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresalteridlivro.Location = new System.Drawing.Point(112, 90);
            this.txtempresalteridlivro.Name = "txtempresalteridlivro";
            this.txtempresalteridlivro.Size = new System.Drawing.Size(94, 25);
            this.txtempresalteridlivro.TabIndex = 29;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(26, 95);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(80, 17);
            this.label38.TabIndex = 28;
            this.label38.Text = "ID do Livro:";
            // 
            // txtempresalterrgfunc
            // 
            this.txtempresalterrgfunc.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresalterrgfunc.Location = new System.Drawing.Point(155, 122);
            this.txtempresalterrgfunc.Name = "txtempresalterrgfunc";
            this.txtempresalterrgfunc.Size = new System.Drawing.Size(149, 25);
            this.txtempresalterrgfunc.TabIndex = 27;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(25, 198);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(125, 17);
            this.label43.TabIndex = 24;
            this.label43.Text = "Data de Devolução:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(25, 165);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(112, 17);
            this.label48.TabIndex = 23;
            this.label48.Text = "Data de Retirada:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(25, 128);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(121, 17);
            this.label61.TabIndex = 22;
            this.label61.Text = "RG do Funcionário:";
            // 
            // btnempresalterar
            // 
            this.btnempresalterar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnempresalterar.Location = new System.Drawing.Point(289, 257);
            this.btnempresalterar.Name = "btnempresalterar";
            this.btnempresalterar.Size = new System.Drawing.Size(75, 31);
            this.btnempresalterar.TabIndex = 19;
            this.btnempresalterar.Text = "Alterar";
            this.btnempresalterar.UseVisualStyleBackColor = true;
            this.btnempresalterar.Click += new System.EventHandler(this.btnempresalterar_Click);
            // 
            // btnempresalterlimpar
            // 
            this.btnempresalterlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnempresalterlimpar.Location = new System.Drawing.Point(382, 257);
            this.btnempresalterlimpar.Name = "btnempresalterlimpar";
            this.btnempresalterlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnempresalterlimpar.TabIndex = 18;
            this.btnempresalterlimpar.Text = "Limpar";
            this.btnempresalterlimpar.UseVisualStyleBackColor = true;
            this.btnempresalterlimpar.Click += new System.EventHandler(this.btnempresalterlimpar_Click);
            // 
            // gbcadempres
            // 
            this.gbcadempres.Controls.Add(this.txtemprescadid);
            this.gbcadempres.Controls.Add(this.label111);
            this.gbcadempres.Controls.Add(this.dateemprescaddatadev);
            this.gbcadempres.Controls.Add(this.dateemprescaddataret);
            this.gbcadempres.Controls.Add(this.btnemprescadastrar);
            this.gbcadempres.Controls.Add(this.btnemprescadlimpar);
            this.gbcadempres.Controls.Add(this.txtemprescadidlivro);
            this.gbcadempres.Controls.Add(this.label59);
            this.gbcadempres.Controls.Add(this.txtemprescadrgfunc);
            this.gbcadempres.Controls.Add(this.txtemprescadrgleitor);
            this.gbcadempres.Controls.Add(this.label60);
            this.gbcadempres.Controls.Add(this.label62);
            this.gbcadempres.Controls.Add(this.label63);
            this.gbcadempres.Controls.Add(this.label65);
            this.gbcadempres.Location = new System.Drawing.Point(11, 19);
            this.gbcadempres.Name = "gbcadempres";
            this.gbcadempres.Size = new System.Drawing.Size(485, 314);
            this.gbcadempres.TabIndex = 2;
            this.gbcadempres.TabStop = false;
            this.gbcadempres.Text = "Cadastrar Empréstimo";
            // 
            // txtemprescadid
            // 
            this.txtemprescadid.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtemprescadid.Location = new System.Drawing.Point(142, 29);
            this.txtemprescadid.Name = "txtemprescadid";
            this.txtemprescadid.Size = new System.Drawing.Size(58, 25);
            this.txtemprescadid.TabIndex = 23;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.label111.Location = new System.Drawing.Point(23, 35);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(119, 17);
            this.label111.TabIndex = 22;
            this.label111.Text = "ID do Empréstimo:";
            // 
            // dateemprescaddatadev
            // 
            this.dateemprescaddatadev.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateemprescaddatadev.Location = new System.Drawing.Point(150, 163);
            this.dateemprescaddatadev.Name = "dateemprescaddatadev";
            this.dateemprescaddatadev.Size = new System.Drawing.Size(273, 25);
            this.dateemprescaddatadev.TabIndex = 21;
            // 
            // dateemprescaddataret
            // 
            this.dateemprescaddataret.Enabled = false;
            this.dateemprescaddataret.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateemprescaddataret.Location = new System.Drawing.Point(150, 130);
            this.dateemprescaddataret.Name = "dateemprescaddataret";
            this.dateemprescaddataret.Size = new System.Drawing.Size(273, 25);
            this.dateemprescaddataret.TabIndex = 20;
            // 
            // btnemprescadastrar
            // 
            this.btnemprescadastrar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnemprescadastrar.Location = new System.Drawing.Point(289, 257);
            this.btnemprescadastrar.Name = "btnemprescadastrar";
            this.btnemprescadastrar.Size = new System.Drawing.Size(75, 31);
            this.btnemprescadastrar.TabIndex = 19;
            this.btnemprescadastrar.Text = "Cadastrar";
            this.btnemprescadastrar.UseVisualStyleBackColor = true;
            this.btnemprescadastrar.Click += new System.EventHandler(this.btnemprescadastrar_Click);
            // 
            // btnemprescadlimpar
            // 
            this.btnemprescadlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnemprescadlimpar.Location = new System.Drawing.Point(382, 257);
            this.btnemprescadlimpar.Name = "btnemprescadlimpar";
            this.btnemprescadlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnemprescadlimpar.TabIndex = 18;
            this.btnemprescadlimpar.Text = "Limpar";
            this.btnemprescadlimpar.UseVisualStyleBackColor = true;
            this.btnemprescadlimpar.Click += new System.EventHandler(this.btnemprescadlimpar_Click);
            // 
            // txtemprescadidlivro
            // 
            this.txtemprescadidlivro.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtemprescadidlivro.Location = new System.Drawing.Point(363, 59);
            this.txtemprescadidlivro.Name = "txtemprescadidlivro";
            this.txtemprescadidlivro.Size = new System.Drawing.Size(94, 25);
            this.txtemprescadidlivro.TabIndex = 15;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(286, 64);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(80, 17);
            this.label59.TabIndex = 14;
            this.label59.Text = "ID do Livro:";
            // 
            // txtemprescadrgfunc
            // 
            this.txtemprescadrgfunc.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtemprescadrgfunc.Location = new System.Drawing.Point(153, 92);
            this.txtemprescadrgfunc.Name = "txtemprescadrgfunc";
            this.txtemprescadrgfunc.Size = new System.Drawing.Size(149, 25);
            this.txtemprescadrgfunc.TabIndex = 11;
            // 
            // txtemprescadrgleitor
            // 
            this.txtemprescadrgleitor.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtemprescadrgleitor.Location = new System.Drawing.Point(109, 59);
            this.txtemprescadrgleitor.Name = "txtemprescadrgleitor";
            this.txtemprescadrgleitor.Size = new System.Drawing.Size(161, 25);
            this.txtemprescadrgleitor.TabIndex = 9;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(23, 165);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(125, 17);
            this.label60.TabIndex = 7;
            this.label60.Text = "Data de Devolução:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(23, 135);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(112, 17);
            this.label62.TabIndex = 4;
            this.label62.Text = "Data de Retirada:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(23, 98);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(121, 17);
            this.label63.TabIndex = 3;
            this.label63.Text = "RG do Funcionário:";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(23, 65);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(88, 17);
            this.label65.TabIndex = 1;
            this.label65.Text = "RG do Leitor:";
            // 
            // gbbuscarempres
            // 
            this.gbbuscarempres.Controls.Add(this.listBoxemprestimo);
            this.gbbuscarempres.Controls.Add(this.btnempresbuscar);
            this.gbbuscarempres.Controls.Add(this.btnempresbuscarlimpar);
            this.gbbuscarempres.Controls.Add(this.txtempresbuscarg);
            this.gbbuscarempres.Controls.Add(this.label72);
            this.gbbuscarempres.Location = new System.Drawing.Point(12, 20);
            this.gbbuscarempres.Name = "gbbuscarempres";
            this.gbbuscarempres.Size = new System.Drawing.Size(485, 314);
            this.gbbuscarempres.TabIndex = 32;
            this.gbbuscarempres.TabStop = false;
            this.gbbuscarempres.Text = "Buscar Empréstimo";
            // 
            // listBoxemprestimo
            // 
            this.listBoxemprestimo.FormattingEnabled = true;
            this.listBoxemprestimo.Location = new System.Drawing.Point(24, 66);
            this.listBoxemprestimo.Name = "listBoxemprestimo";
            this.listBoxemprestimo.Size = new System.Drawing.Size(430, 173);
            this.listBoxemprestimo.TabIndex = 21;
            // 
            // btnempresbuscar
            // 
            this.btnempresbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnempresbuscar.Location = new System.Drawing.Point(286, 15);
            this.btnempresbuscar.Name = "btnempresbuscar";
            this.btnempresbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnempresbuscar.TabIndex = 20;
            this.btnempresbuscar.Text = "Buscar";
            this.btnempresbuscar.UseVisualStyleBackColor = true;
            this.btnempresbuscar.Click += new System.EventHandler(this.btnempresbuscar_Click);
            // 
            // btnempresbuscarlimpar
            // 
            this.btnempresbuscarlimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnempresbuscarlimpar.Location = new System.Drawing.Point(382, 257);
            this.btnempresbuscarlimpar.Name = "btnempresbuscarlimpar";
            this.btnempresbuscarlimpar.Size = new System.Drawing.Size(75, 31);
            this.btnempresbuscarlimpar.TabIndex = 18;
            this.btnempresbuscarlimpar.Text = "Limpar";
            this.btnempresbuscarlimpar.UseVisualStyleBackColor = true;
            this.btnempresbuscarlimpar.Click += new System.EventHandler(this.btnempresbuscarlimpar_Click);
            // 
            // txtempresbuscarg
            // 
            this.txtempresbuscarg.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresbuscarg.Location = new System.Drawing.Point(111, 19);
            this.txtempresbuscarg.Name = "txtempresbuscarg";
            this.txtempresbuscarg.Size = new System.Drawing.Size(161, 25);
            this.txtempresbuscarg.TabIndex = 9;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(22, 22);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(88, 17);
            this.label72.TabIndex = 1;
            this.label72.Text = "RG do Leitor:";
            // 
            // gbexcluirempres
            // 
            this.gbexcluirempres.Controls.Add(this.txtempresdelrgleitor);
            this.gbexcluirempres.Controls.Add(this.label112);
            this.gbexcluirempres.Controls.Add(this.dateempresdeldatadev);
            this.gbexcluirempres.Controls.Add(this.dateempresdeldataret);
            this.gbexcluirempres.Controls.Add(this.txtempresdelidlivro);
            this.gbexcluirempres.Controls.Add(this.label51);
            this.gbexcluirempres.Controls.Add(this.txtempresdelrgfunc);
            this.gbexcluirempres.Controls.Add(this.label58);
            this.gbexcluirempres.Controls.Add(this.label64);
            this.gbexcluirempres.Controls.Add(this.label66);
            this.gbexcluirempres.Controls.Add(this.btnempresdelbuscar);
            this.gbexcluirempres.Controls.Add(this.btnempresexcluir);
            this.gbexcluirempres.Controls.Add(this.btnempresdellimpar);
            this.gbexcluirempres.Controls.Add(this.txtempresdelidempres);
            this.gbexcluirempres.Controls.Add(this.label67);
            this.gbexcluirempres.Location = new System.Drawing.Point(11, 19);
            this.gbexcluirempres.Name = "gbexcluirempres";
            this.gbexcluirempres.Size = new System.Drawing.Size(485, 314);
            this.gbexcluirempres.TabIndex = 31;
            this.gbexcluirempres.TabStop = false;
            this.gbexcluirempres.Text = " Excluir Empréstimo";
            // 
            // txtempresdelrgleitor
            // 
            this.txtempresdelrgleitor.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresdelrgleitor.Location = new System.Drawing.Point(112, 56);
            this.txtempresdelrgleitor.Name = "txtempresdelrgleitor";
            this.txtempresdelrgleitor.Size = new System.Drawing.Size(161, 25);
            this.txtempresdelrgleitor.TabIndex = 33;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(26, 61);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(88, 17);
            this.label112.TabIndex = 32;
            this.label112.Text = "RG do Leitor:";
            // 
            // dateempresdeldatadev
            // 
            this.dateempresdeldatadev.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateempresdeldatadev.Location = new System.Drawing.Point(156, 197);
            this.dateempresdeldatadev.Name = "dateempresdeldatadev";
            this.dateempresdeldatadev.Size = new System.Drawing.Size(275, 25);
            this.dateempresdeldatadev.TabIndex = 31;
            // 
            // dateempresdeldataret
            // 
            this.dateempresdeldataret.Enabled = false;
            this.dateempresdeldataret.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.dateempresdeldataret.Location = new System.Drawing.Point(156, 164);
            this.dateempresdeldataret.Name = "dateempresdeldataret";
            this.dateempresdeldataret.Size = new System.Drawing.Size(275, 25);
            this.dateempresdeldataret.TabIndex = 30;
            // 
            // txtempresdelidlivro
            // 
            this.txtempresdelidlivro.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresdelidlivro.Location = new System.Drawing.Point(112, 90);
            this.txtempresdelidlivro.Name = "txtempresdelidlivro";
            this.txtempresdelidlivro.Size = new System.Drawing.Size(94, 25);
            this.txtempresdelidlivro.TabIndex = 29;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(26, 95);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(80, 17);
            this.label51.TabIndex = 28;
            this.label51.Text = "ID do Livro:";
            // 
            // txtempresdelrgfunc
            // 
            this.txtempresdelrgfunc.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresdelrgfunc.Location = new System.Drawing.Point(155, 122);
            this.txtempresdelrgfunc.Name = "txtempresdelrgfunc";
            this.txtempresdelrgfunc.Size = new System.Drawing.Size(149, 25);
            this.txtempresdelrgfunc.TabIndex = 27;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(25, 198);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(125, 17);
            this.label58.TabIndex = 24;
            this.label58.Text = "Data de Devolução:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(25, 165);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(112, 17);
            this.label64.TabIndex = 23;
            this.label64.Text = "Data de Retirada:";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(25, 128);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(121, 17);
            this.label66.TabIndex = 22;
            this.label66.Text = "RG do Funcionário:";
            // 
            // btnempresdelbuscar
            // 
            this.btnempresdelbuscar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnempresdelbuscar.Location = new System.Drawing.Point(240, 14);
            this.btnempresdelbuscar.Name = "btnempresdelbuscar";
            this.btnempresdelbuscar.Size = new System.Drawing.Size(75, 31);
            this.btnempresdelbuscar.TabIndex = 20;
            this.btnempresdelbuscar.Text = "Buscar";
            this.btnempresdelbuscar.UseVisualStyleBackColor = true;
            this.btnempresdelbuscar.Click += new System.EventHandler(this.btnempresdelbuscar_Click);
            // 
            // btnempresexcluir
            // 
            this.btnempresexcluir.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnempresexcluir.Location = new System.Drawing.Point(289, 257);
            this.btnempresexcluir.Name = "btnempresexcluir";
            this.btnempresexcluir.Size = new System.Drawing.Size(75, 31);
            this.btnempresexcluir.TabIndex = 19;
            this.btnempresexcluir.Text = "Excluir";
            this.btnempresexcluir.UseVisualStyleBackColor = true;
            this.btnempresexcluir.Click += new System.EventHandler(this.btnempresexcluir_Click);
            // 
            // btnempresdellimpar
            // 
            this.btnempresdellimpar.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.btnempresdellimpar.Location = new System.Drawing.Point(382, 257);
            this.btnempresdellimpar.Name = "btnempresdellimpar";
            this.btnempresdellimpar.Size = new System.Drawing.Size(75, 31);
            this.btnempresdellimpar.TabIndex = 18;
            this.btnempresdellimpar.Text = "Limpar";
            this.btnempresdellimpar.UseVisualStyleBackColor = true;
            this.btnempresdellimpar.Click += new System.EventHandler(this.btnempresdellimpar_Click);
            // 
            // txtempresdelidempres
            // 
            this.txtempresdelidempres.Font = new System.Drawing.Font("Times New Roman", 11.25F);
            this.txtempresdelidempres.Location = new System.Drawing.Point(142, 19);
            this.txtempresdelidempres.Name = "txtempresdelidempres";
            this.txtempresdelidempres.Size = new System.Drawing.Size(83, 25);
            this.txtempresdelidempres.TabIndex = 9;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(22, 22);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(119, 17);
            this.label67.TabIndex = 1;
            this.label67.Text = "ID do Empréstimo:";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(510, 370);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.gblivros);
            this.Controls.Add(this.gbautores);
            this.Controls.Add(this.gbleitores);
            this.Controls.Add(this.gbemprestimo);
            this.Controls.Add(this.gbinicio);
            this.Controls.Add(this.gbfuncionario);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Biblioteca";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gbinicio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbcadfunc.ResumeLayout(false);
            this.gbcadfunc.PerformLayout();
            this.gbfuncionario.ResumeLayout(false);
            this.gbexcluirfunc.ResumeLayout(false);
            this.gbexcluirfunc.PerformLayout();
            this.gbfuncalter.ResumeLayout(false);
            this.gbfuncalter.PerformLayout();
            this.gbbuscarfunc.ResumeLayout(false);
            this.gbbuscarfunc.PerformLayout();
            this.gbleitores.ResumeLayout(false);
            this.gbbuscarleitor.ResumeLayout(false);
            this.gbbuscarleitor.PerformLayout();
            this.gbexcluirleitor.ResumeLayout(false);
            this.gbexcluirleitor.PerformLayout();
            this.gbalterleitor.ResumeLayout(false);
            this.gbalterleitor.PerformLayout();
            this.gbcadleitor.ResumeLayout(false);
            this.gbcadleitor.PerformLayout();
            this.gbautores.ResumeLayout(false);
            this.gbcadautor.ResumeLayout(false);
            this.gbcadautor.PerformLayout();
            this.gbbuscarautor.ResumeLayout(false);
            this.gbbuscarautor.PerformLayout();
            this.gbexcluirautor.ResumeLayout(false);
            this.gbexcluirautor.PerformLayout();
            this.gbalterautor.ResumeLayout(false);
            this.gbalterautor.PerformLayout();
            this.gblivros.ResumeLayout(false);
            this.gbbuscarlivro.ResumeLayout(false);
            this.gbbuscarlivro.PerformLayout();
            this.gbexcluirlivro.ResumeLayout(false);
            this.gbexcluirlivro.PerformLayout();
            this.gbalterlivro.ResumeLayout(false);
            this.gbalterlivro.PerformLayout();
            this.gbcadlivro.ResumeLayout(false);
            this.gbcadlivro.PerformLayout();
            this.gbemprestimo.ResumeLayout(false);
            this.gbalterempres.ResumeLayout(false);
            this.gbalterempres.PerformLayout();
            this.gbcadempres.ResumeLayout(false);
            this.gbcadempres.PerformLayout();
            this.gbbuscarempres.ResumeLayout(false);
            this.gbbuscarempres.PerformLayout();
            this.gbexcluirempres.ResumeLayout(false);
            this.gbexcluirempres.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem empréstimosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funcionariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excluirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leitoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem autoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem livrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem inícioToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbinicio;
        private System.Windows.Forms.GroupBox gbcadfunc;
        private System.Windows.Forms.Button btnfunccadastrar;
        private System.Windows.Forms.GroupBox gbfuncionario;
        private System.Windows.Forms.GroupBox gbfuncalter;
        private System.Windows.Forms.Button btnfuncalterbuscar;
        private System.Windows.Forms.Button btnfuncalterar;
        private System.Windows.Forms.Button btnfuncalterlimpar;
        private System.Windows.Forms.TextBox txtfuncalterrg;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox gbleitores;
        private System.Windows.Forms.GroupBox gbcadleitor;
        private System.Windows.Forms.Button btnleitorcadastrar;
        private System.Windows.Forms.Button btnleitorcadlimpar;
        private System.Windows.Forms.ComboBox cbleitorcadstatus;
        private System.Windows.Forms.TextBox txtleitorcadcpf;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtleitorcadcontato;
        private System.Windows.Forms.TextBox txtleitorcademail;
        private System.Windows.Forms.TextBox txtleitorcadrg;
        private System.Windows.Forms.TextBox txtleitorcadnome;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox gbalterleitor;
        private System.Windows.Forms.Button btnleitoralterbuscar;
        private System.Windows.Forms.Button btnleitoralterar;
        private System.Windows.Forms.Button btnleitoralterlimpar;
        private System.Windows.Forms.ComboBox cbleitoralterstatus;
        private System.Windows.Forms.TextBox txtleitoraltercpf;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtleitoraltercontato;
        private System.Windows.Forms.TextBox txtleitoralteremail;
        private System.Windows.Forms.TextBox txtleitoralterrg;
        private System.Windows.Forms.TextBox txtleitoralternome;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox gbautores;
        private System.Windows.Forms.GroupBox gbcadautor;
        private System.Windows.Forms.TextBox txtautorcadid;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnautorcadastrar;
        private System.Windows.Forms.Button btnautorcadlimpar;
        private System.Windows.Forms.ComboBox cbautorcadcategoria;
        private System.Windows.Forms.TextBox txtautorcadcontato;
        private System.Windows.Forms.TextBox txtautorcadnome;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox gbalterautor;
        private System.Windows.Forms.ComboBox cbautoraltercategoria;
        private System.Windows.Forms.TextBox txtautoraltercontato;
        private System.Windows.Forms.TextBox txtautoralternome;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button btnautoralterbuscar;
        private System.Windows.Forms.Button btnautoralterar;
        private System.Windows.Forms.Button btnautoralterlimpar;
        private System.Windows.Forms.TextBox txtautoralterid;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox gblivros;
        private System.Windows.Forms.GroupBox gbcadlivro;
        private System.Windows.Forms.Button btnlivrocadastrar;
        private System.Windows.Forms.Button btnlivrocadlimpar;
        private System.Windows.Forms.TextBox txtlivrocadano;
        private System.Windows.Forms.TextBox txtlivrocadedicao;
        private System.Windows.Forms.TextBox txtlivrocadidautor;
        private System.Windows.Forms.TextBox txtlivrocadtitulo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox gbalterlivro;
        private System.Windows.Forms.Button btnlivroalterbuscar;
        private System.Windows.Forms.Button btnlivroalterar;
        private System.Windows.Forms.Button btnlivroalterlimpar;
        private System.Windows.Forms.TextBox txtlivroalterid;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox gbemprestimo;
        private System.Windows.Forms.GroupBox gbcadempres;
        private System.Windows.Forms.Button btnemprescadastrar;
        private System.Windows.Forms.Button btnemprescadlimpar;
        private System.Windows.Forms.TextBox txtemprescadidlivro;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtemprescadrgfunc;
        private System.Windows.Forms.TextBox txtemprescadrgleitor;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.GroupBox gbalterempres;
        private System.Windows.Forms.Button btnempresalterar;
        private System.Windows.Forms.Button btnempresalterlimpar;
        private System.Windows.Forms.TextBox txtlivrocadid;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox txtlivroalterdescricao;
        private System.Windows.Forms.TextBox txtlivroalterano;
        private System.Windows.Forms.TextBox txtlivroalteredicao;
        private System.Windows.Forms.TextBox txtlivroalteridautor;
        private System.Windows.Forms.TextBox txtlivroaltertitulo;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtempresalteridlivro;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtempresalterrgfunc;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.GroupBox gbbuscarempres;
        private System.Windows.Forms.Button btnempresbuscar;
        private System.Windows.Forms.Button btnempresbuscarlimpar;
        private System.Windows.Forms.TextBox txtempresbuscarg;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.GroupBox gbexcluirempres;
        private System.Windows.Forms.TextBox txtempresdelidlivro;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txtempresdelrgfunc;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button btnempresdelbuscar;
        private System.Windows.Forms.Button btnempresexcluir;
        private System.Windows.Forms.Button btnempresdellimpar;
        private System.Windows.Forms.TextBox txtempresdelidempres;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.GroupBox gbbuscarfunc;
        private System.Windows.Forms.ListBox listBoxfuncionario;
        private System.Windows.Forms.Button btnfuncbuscar;
        private System.Windows.Forms.Button btnfuncbuscalimpar;
        private System.Windows.Forms.TextBox txtfuncbuscarg;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.GroupBox gbexcluirfunc;
        private System.Windows.Forms.Button btnfuncdelbuscar;
        private System.Windows.Forms.Button btnfuncexcluir;
        private System.Windows.Forms.Button btnfuncdellimpar;
        private System.Windows.Forms.ComboBox cbdelfuncstatus;
        private System.Windows.Forms.TextBox txtdelfuncsenha;
        private System.Windows.Forms.TextBox txtdelfunccpf;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox txtdelfunccontato;
        private System.Windows.Forms.TextBox txtdelfuncemail;
        private System.Windows.Forms.TextBox txtdelfuncrg;
        private System.Windows.Forms.TextBox txtdelfuncnome;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.ListBox listBoxemprestimo;
        private System.Windows.Forms.GroupBox gbbuscarlivro;
        private System.Windows.Forms.ListBox listBoxlivro;
        private System.Windows.Forms.Button btnlivrobuscar;
        private System.Windows.Forms.Button btnlivrobuscarlimpar;
        private System.Windows.Forms.TextBox txtlivrobuscarid;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.GroupBox gbexcluirlivro;
        private System.Windows.Forms.TextBox txtlivrodeldescricao;
        private System.Windows.Forms.TextBox txtlivrodelano;
        private System.Windows.Forms.TextBox txtlivrodeledicao;
        private System.Windows.Forms.TextBox txtlivrodelidautor;
        private System.Windows.Forms.TextBox txtlivrodeltitulo;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Button btndelbuscar;
        private System.Windows.Forms.Button btnlivroexcluir;
        private System.Windows.Forms.Button btndellivrolimpar;
        private System.Windows.Forms.TextBox txtlivrodelid;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.GroupBox gbexcluirautor;
        private System.Windows.Forms.ComboBox cbautordelcategoria;
        private System.Windows.Forms.TextBox txtautordelcontato;
        private System.Windows.Forms.TextBox txtautordelnome;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Button btnautordelbuscar;
        private System.Windows.Forms.Button btnautorexcluir;
        private System.Windows.Forms.Button btnautordellimpar;
        private System.Windows.Forms.TextBox txtautordelid;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.GroupBox gbbuscarautor;
        private System.Windows.Forms.ListBox listBoxautor;
        private System.Windows.Forms.Button btnautorbuscar;
        private System.Windows.Forms.Button btnautorbuscarlimpar;
        private System.Windows.Forms.TextBox txtautorbuscarid;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.GroupBox gbbuscarleitor;
        private System.Windows.Forms.ListBox listBoxleitor;
        private System.Windows.Forms.Button btnleitorbuscar;
        private System.Windows.Forms.Button btnleitorbuscarlimpar;
        private System.Windows.Forms.TextBox txtleitorbuscarrg;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.GroupBox gbexcluirleitor;
        private System.Windows.Forms.Button btnleitordelbuscar;
        private System.Windows.Forms.Button btnleitorexcluir;
        private System.Windows.Forms.Button btnleitordellimpar;
        private System.Windows.Forms.ComboBox cbleitordelstatus;
        private System.Windows.Forms.TextBox txtleitordelcpf;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox txtleitordelcontato;
        private System.Windows.Forms.TextBox txtleitordelemail;
        private System.Windows.Forms.TextBox txtleitordelrg;
        private System.Windows.Forms.TextBox txtleitordelnome;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DateTimePicker dateemprescaddatadev;
        private System.Windows.Forms.DateTimePicker dateemprescaddataret;
        private System.Windows.Forms.TextBox txtfunccadrg;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker datefunccaddatanasc;
        private System.Windows.Forms.ComboBox cbfunccadstatus;
        private System.Windows.Forms.TextBox txtfunccadsenha;
        private System.Windows.Forms.TextBox txtfunccadcontato;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtfunccadcpf;
        private System.Windows.Forms.TextBox txtfunccademail;
        private System.Windows.Forms.TextBox txtfunccadnome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker datefuncalterdatanasc;
        private System.Windows.Forms.ComboBox cbfuncalterstatus;
        private System.Windows.Forms.TextBox txtfuncaltersenha;
        private System.Windows.Forms.TextBox txtfuncaltercpf;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtfuncaltercontato;
        private System.Windows.Forms.TextBox txtfuncalteremail;
        private System.Windows.Forms.TextBox txtfuncalternome;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker datedelfuncdatanasc;
        private System.Windows.Forms.DateTimePicker dateleitorcaddatanasc;
        private System.Windows.Forms.DateTimePicker dateleitordeldatanasc;
        private System.Windows.Forms.DateTimePicker dateleitoralterdatanasc;
        private System.Windows.Forms.DateTimePicker dateautordeldatafalesc;
        private System.Windows.Forms.DateTimePicker dateautordeldatanasc;
        private System.Windows.Forms.DateTimePicker dateautoralterdatanasc;
        private System.Windows.Forms.DateTimePicker dateautoralterdatafales;
        private System.Windows.Forms.DateTimePicker dateautorcaddatanasc;
        private System.Windows.Forms.DateTimePicker dateautorcaddatafalesc;
        private System.Windows.Forms.DateTimePicker dateempresdeldatadev;
        private System.Windows.Forms.DateTimePicker dateempresdeldataret;
        private System.Windows.Forms.DateTimePicker dateempresalterdatdev;
        private System.Windows.Forms.DateTimePicker dateempresalterdataret;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox txtlivrodeleditora;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox txtlivroaltereditora;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TextBox txtlivrocadeditora;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox txtlivrocaddescricao;
        private System.Windows.Forms.TextBox txtlivrocadquant;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox txtlivrodelquant;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TextBox txtlivroalterquant;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox txtemprescadid;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox txtempresdelrgleitor;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox txtempresalterrgleitor;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Button btnempresalterbuscar;
        private System.Windows.Forms.TextBox txtempresalteridempres;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Button btnfunccadlimpar;
        private System.Windows.Forms.ComboBox cblivrodelcaegoria;
        private System.Windows.Forms.ComboBox cblivroaltercategoria;
        private System.Windows.Forms.ComboBox cblivrocadcategoria;
    }
}