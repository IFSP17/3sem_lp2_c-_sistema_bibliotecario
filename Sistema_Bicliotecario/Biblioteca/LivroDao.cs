﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows.Forms;

namespace Biblioteca
{
    class LivroDao
    {
        public static String tabela_livro = "livro";
        public Livro livro {set; get;}

        public void cadastrar_livro()
        {
            int verifica;
            if (livro == null) return;
            String SQL = "Insert into :tabela_livro (id_livro, titulo, id_autor, edicao, ano, editora, categoria, descricao,quantidade) values (:id, ':titulo', :autor, ':edicao', :ano, ':editora', ':categoria', ':descricao',:quantidade)";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            verifica=db.executaSQL(SQL);
            if(verifica==0)
            {
                MessageBox.Show("Cadastrado com sucesso");
            }
            else
            {
                MessageBox.Show("Falha no cadastro");
            }
        }

        public void alterar_livro()
        {
            String SQL = "UPDATE :tabela_livro SET titulo =':titulo', id_autor =:autor,ano=:ano, edicao=':edicao', editora =':editora', categoria=':categoria', descricao=':descricao', quantidade=:quantidade where  id_livro =:id";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);

        }

        private String fazerSubstituicao(String SQL)
        {
            SQL = SQL.Replace(":tabela_livro", tabela_livro);
            if (livro != null)
            {
                SQL = SQL.Replace(":id", livro.id.ToString ());
                SQL = SQL.Replace(":titulo", livro.titulo);
                SQL = SQL.Replace(":autor", livro.id_autor.ToString());
                SQL = SQL.Replace(":edicao", livro.edicao);
                SQL = SQL.Replace(":ano", livro.ano.ToString());
                SQL = SQL.Replace(":editora", livro.editora);
                SQL = SQL.Replace(":categoria", livro.categoria);
                SQL = SQL.Replace(":quantidade", livro.quantidade.ToString());
                SQL = SQL.Replace(":descricao", livro.descricao);
              
            }

            return SQL;

        }

        public Livro consultar_livro(int id)
        {
            int verifica = 0;
            String SQL = "SELECT id_livro, titulo, id_autor, edicao, ano, editora, categoria, descricao,quantidade FROM :tabela_livro where id_livro = :id";
            SQL = SQL.Replace(":id", id.ToString());
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            Livro l = new Livro();
            while (dr.Read())
            {
                l.id = int.Parse(dr[0].ToString());
                l.titulo = dr[1].ToString();
                l.id_autor = int.Parse(dr[2].ToString());
                l.edicao = dr[3].ToString();
                l.ano = int.Parse(dr[4].ToString());
                l.editora = dr[5].ToString();
                l.categoria = dr[6].ToString();
                l.descricao = dr[7].ToString();
                l.quantidade =int.Parse(dr[8].ToString());
                verifica++;
 
            }
            if (verifica== 0)
            {
                MessageBox.Show("Livro não existente");
            }
            db.fechar();
            return l;
        }

        public void excluir_livro(int id)
        {
            String SQL = "DELETE FROM :tabela_livro where id_livro = :id";
            SQL = SQL.Replace(":tabela_livro", tabela_livro);
            SQL = SQL.Replace(":id", id.ToString());
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public List<Livro> listar_livro(int id)
        {
            int verifica = 0;
            String SQL = "SELECT  id_livro, titulo, id_autor, edicao, ano, editora, categoria, descricao, quantidade from :tabela_livro where id_livro=:id";
            SQL = SQL.Replace(":tabela_livro", tabela_livro);
            SQL = SQL.Replace(":id", id.ToString());
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            List<Livro> lista_livro = new List<Livro>();
            while (dr.Read())
            {
                Livro l = new Livro ();
                l.id = int.Parse (dr[0].ToString());
                l.titulo = dr[1].ToString();
                l.id_autor = int.Parse (dr[2].ToString());
                l.edicao = dr[3].ToString();
                l.ano = int.Parse (dr[4].ToString());
                l.editora = dr[5].ToString();
                l.categoria = dr[6].ToString();
                l.descricao = dr[7].ToString();
                l.quantidade = int.Parse(dr[8].ToString());
                lista_livro.Add(l);

                verifica++;
            }
            if (verifica==0)
            {
                MessageBox.Show("Nenhum resultado encontrado");
            }

            db.fechar();
            return lista_livro;
        }
    }
}
