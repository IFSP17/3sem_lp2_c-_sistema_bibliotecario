﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca
{
    class Autor
    {
        public int id { set; get; }
        public string nome { set; get; }
        public DateTime data_nasc { set; get;}
        public DateTime data_falecimento { set; get; }
        public string categoria_principal { set; get; }
        public string contato { set; get; }

        public override string ToString()
        {
            return "Código:"+id.ToString()+", "+ nome +", Nasc.:" + data_nasc.ToString() + ", Falescimento:" + data_falecimento.ToString() + ", Fone:" + contato + ", Categoria:" + categoria_principal;
        }
    }
}
