﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca
{
    class Livro
    {
        public int id { set; get; }
        public string titulo { set; get; }
        public int id_autor {set;get;}
        public string edicao { set; get; }
        public int ano { set; get; }
        public string editora { set; get; }
        public string categoria { set; get; }
        public string descricao { set; get; }
        public int quantidade { set; get; }

        public override string ToString()
        {
            return "ID:" + id.ToString() + ", Titulo:" + titulo + ", Autor:" + id_autor.ToString() + ", Edição:" + edicao + ", Ano:" + ano.ToString() + ", Editora:" + editora + ", Categoria:" + categoria + ", Descrição:" + descricao + ", Quant.:" + quantidade.ToString();
        }
    }
}
