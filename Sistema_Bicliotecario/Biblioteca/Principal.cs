﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca
{
    public partial class Principal : Form
    {
        private List<Funcionario> lista_func;
        private List<Livro> lista_livro;
        private List<Autor> lista_autor;
        private List<Leitor> lista_leitor;
        private List<Emprestimo> lista_emprestimo;
        public Principal()
        {
            InitializeComponent();
            gbinicio.BringToFront();
        }

        //toolstrip
        #region toolstrip
        private void inícioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbinicio.BringToFront();
        }        

        private void cadastrasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbfuncionario.BringToFront();
            gbcadfunc.BringToFront();            
        }

        private void alterarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbfuncionario.BringToFront();
            gbfuncalter.BringToFront();
        }

        private void excluirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbfuncionario.BringToFront();
            gbexcluirfunc.BringToFront();
        }

        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbfuncionario.BringToFront();
            gbbuscarfunc.BringToFront();
        }      
        

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            gbleitores.BringToFront();
            gbcadleitor.BringToFront();           
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            gbleitores.BringToFront();
            gbalterleitor.BringToFront();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            gbleitores.BringToFront();
            gbexcluirleitor.BringToFront();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            gbleitores.BringToFront();
            gbbuscarleitor.BringToFront();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            gbautores.BringToFront();
            gbcadautor.BringToFront();
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            gbautores.BringToFront();
            gbalterautor.BringToFront();
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            gbautores.BringToFront();
            gbexcluirautor.BringToFront();
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            gbautores.BringToFront();
            gbbuscarautor.BringToFront();
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            gblivros.BringToFront();
            gbcadlivro.BringToFront();
        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            gblivros.BringToFront();
            gbalterlivro.BringToFront();
        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            gblivros.BringToFront();
            gbexcluirlivro.BringToFront();
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            gblivros.BringToFront();
            gbbuscarlivro.BringToFront();
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            gbemprestimo.BringToFront();
            gbcadempres.BringToFront();
        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            gbemprestimo.BringToFront();
            gbalterempres.BringToFront();
        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {
            gbemprestimo.BringToFront();
            gbexcluirempres.BringToFront();
        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            gbemprestimo.BringToFront();
            gbbuscarempres.BringToFront();
        }
        #endregion
        //Funcionario
        #region funcionario
        //cadastrar

        private Funcionario recolheDadosCadastrarFuncionario()
        {
            try
            {
                Funcionario funcionario = new Funcionario();

                funcionario.nome = txtfunccadnome.Text;
                funcionario.rg = txtfunccadrg.Text;
                funcionario.cpf = txtfunccadcpf.Text;
                funcionario.contato = txtfunccadcontato.Text;
                funcionario.status = cbfunccadstatus.SelectedItem.ToString();
                funcionario.data_nasc = datefunccaddatanasc.Value;
                funcionario.email = txtfunccademail.Text;
                funcionario.senha = txtfunccadsenha.Text;

                return funcionario;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private Funcionario recolheDadosAlterarFuncionario()
        {
            try
            {
                Funcionario funcionario = new Funcionario();

                funcionario.nome = txtfuncalternome.Text;
                funcionario.rg = txtfuncalterrg.Text;
                funcionario.cpf = txtfuncaltercpf.Text;
                funcionario.contato = txtfuncaltercontato.Text;
                funcionario.status = cbfuncalterstatus.SelectedItem.ToString();
                funcionario.data_nasc = datefuncalterdatanasc.Value;
                funcionario.email = txtfuncalteremail.Text;
                funcionario.senha = txtfuncaltersenha.Text;

                return funcionario;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private Funcionario recolheDadosExcluirFuncionario()
        {
            try
            {
                Funcionario funcionario = new Funcionario();

                funcionario.nome = txtdelfuncnome.Text;
                funcionario.rg = txtdelfuncrg.Text;
                funcionario.cpf = txtdelfunccpf.Text;
                funcionario.contato = txtdelfunccontato.Text;
                funcionario.status = cbdelfuncstatus.SelectedItem.ToString();
                funcionario.data_nasc = datedelfuncdatanasc.Value;
                funcionario.email = txtdelfuncemail.Text;
                funcionario.senha = txtdelfuncsenha.Text;

                return funcionario;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private void preencheDadosAlterarFuncionario(Funcionario f)
        {
            txtfuncalterrg.Text = f.rg;
            txtfuncalternome.Text = f.nome;
            txtfuncaltercpf.Text = f.cpf;
            cbfuncalterstatus.SelectedItem = f.status;
            datefuncalterdatanasc.Value = f.data_nasc;
            txtfuncalteremail.Text = f.email;
            txtfuncaltercontato.Text = f.contato;
            txtfuncaltersenha.Text = f.senha;
        }
        private void preencheDadosExcluirFuncionario(Funcionario f)
        {
            txtdelfuncnome.Text = f.nome;
            txtdelfunccpf.Text = f.cpf;
            cbdelfuncstatus.SelectedItem = f.status;
            datedelfuncdatanasc.Value = f.data_nasc;
            txtdelfuncemail.Text = f.email;
            txtdelfunccontato.Text = f.contato;
            txtdelfuncsenha.Text = f.senha;
        }


        private void btnfunccadastrar_Click(object sender, EventArgs e)
        {
            Funcionario f = recolheDadosCadastrarFuncionario();
            if (f != null)
            {
                FuncionarioDAO dao = new FuncionarioDAO();
                dao.funcionario = f;
                dao.cadastrar_func();

                limpar_cadfunc();
            }
        }

        //limpar cadastrar
        private void btnfunccadlimpar_Click(object sender, EventArgs e)
        {
            limpar_cadfunc();
        }

        //buscar
        private void btnfuncbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDAO dao = new FuncionarioDAO();
                lista_func = dao.listar_func(txtfuncbuscarg.Text);
                listBoxfuncionario.DataSource = null;
                listBoxfuncionario.DataSource = lista_func;
            }

            catch (Exception excessao)
            {
                MessageBox.Show("Funcionario não existe!");
            }
        }

        //limpar buscar
        private void btnfuncbuscalimpar_Click(object sender, EventArgs e)
        {
            txtfuncbuscarg.Clear();
            listBoxfuncionario = null;
        }

        //buscar excluir
        private void btnfuncdelbuscar_Click(object sender, EventArgs e)
        {
            FuncionarioDAO dao = new FuncionarioDAO();
            Funcionario f = new Funcionario();

            f=dao.consultar_func(txtdelfuncrg.Text);
            preencheDadosExcluirFuncionario(f);
        }

        //excluir
        private void btnfuncexcluir_Click(object sender, EventArgs e)
        {
            FuncionarioDAO dao = new FuncionarioDAO();

            recolheDadosExcluirFuncionario();
            dao.excluir_func(txtdelfuncrg.Text);
            limpar_delfunc();
        }

        //limpar excluir
        private void btnfuncdellimpar_Click(object sender, EventArgs e)
        {
            limpar_delfunc();
        }

        //buscar alterar
        private void btnfuncalterbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Funcionario f = new Funcionario();
                FuncionarioDAO dao = new FuncionarioDAO();

                f.rg = txtfuncalterrg.Text;
                f=dao.consultar_func(f.rg);
                preencheDadosAlterarFuncionario(f);
            }

            catch (Exception excessao)
            {
                MessageBox.Show("Funcionario não existe!");
            }
        }

        //alterar
        private void btnfuncalterar_Click(object sender, EventArgs e)
        {
            Funcionario f = new Funcionario();
            FuncionarioDAO dao = new FuncionarioDAO();
            Funcionario f_novo = recolheDadosAlterarFuncionario();

            if (f_novo != null)
            {

                f.rg = f_novo.rg;
                f.nome = f_novo.nome;
                f.cpf = f_novo.cpf;
                f.status = f_novo.status;
                f.data_nasc = f_novo.data_nasc;
                f.email = f_novo.email;
                f.contato = f_novo.contato;
                f.senha = f_novo.senha;
                
                dao.funcionario = f;
                dao.alterar_func();

                limpar_alterfunc();
            }
        }
        //limpar alterar
        private void btnfuncalterlimpar_Click(object sender, EventArgs e)
        {
            limpar_alterfunc();
        }
        #endregion
        //livro
        #region livro
        private Livro recolheDadosCadastrarLivro()
        {
            try
            {
                Livro livro = new Livro();

                livro.id = int.Parse(txtlivrocadid.Text);
                livro.titulo = txtlivrocadtitulo.Text;
                livro.id_autor = int.Parse(txtlivrocadidautor.Text);
                livro.edicao = txtlivrocadedicao.Text;
                livro.ano = int.Parse(txtlivrocadano.Text);
                livro.categoria = cblivrocadcategoria.SelectedItem.ToString();
                livro.editora = txtlivrocadano.Text;
                livro.quantidade = int.Parse(txtlivrocadquant.Text);
                livro.descricao = txtlivrocaddescricao.Text;

                return livro;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private Livro recolheDadosAlterarLivro()
        {
            try
            {
                Livro livro = new Livro();
                livro.id = int.Parse(txtlivroalterid.Text);
                livro.titulo = txtlivroaltertitulo.Text;
                livro.id_autor = int.Parse(txtlivroalteridautor.Text);
                livro.edicao = txtlivroalteredicao.Text;
                livro.ano = int.Parse(txtlivroalterano.Text);
                livro.categoria = cblivroaltercategoria.SelectedItem.ToString();
                livro.editora = txtlivroaltereditora.Text;
                livro.quantidade = int.Parse(txtlivroalterquant.Text);
                livro.descricao = txtlivroalterdescricao.Text;

                return livro;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private Livro recolheDadosExcluirLivro()
        {
            try
            {
                Livro livro = new Livro();

                livro.id = int.Parse(txtlivrodelid.Text);
                livro.titulo = txtlivrodeltitulo.Text;
                livro.id_autor = int.Parse(txtlivrodelidautor.Text);
                livro.edicao = txtlivrodeledicao.Text;
                livro.ano = int.Parse(txtlivrodelano.Text);
                livro.categoria = cblivrodelcaegoria.SelectedItem.ToString();
                livro.editora = txtlivrodelano.Text;
                livro.quantidade = int.Parse(txtlivrodelquant.Text);
                livro.descricao = txtlivrodeldescricao.Text;

                return livro;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private void preencheDadosAlterarLivro(Livro l)
        {
            txtlivroaltertitulo.Text = l.titulo;
            txtlivroalteridautor.Text = l.id_autor.ToString();
            txtlivroalteredicao.Text = l.edicao;
            txtlivroalterano.Text = l.ano.ToString();
            cblivroaltercategoria.SelectedItem = l.categoria;
            txtlivroaltereditora.Text = l.editora;
            txtlivroalterquant.Text = l.quantidade.ToString();
            txtlivroalterdescricao.Text = l.descricao;
        }
        private void preencheDadosExcluirLivro(Livro l)
        {
            txtlivrodeltitulo.Text = l.titulo;
            txtlivrodelidautor.Text = l.id_autor.ToString();
            txtlivrodeledicao.Text = l.edicao;
            txtlivrodelano.Text = l.ano.ToString();
            cblivrodelcaegoria.SelectedItem = l.categoria;
            txtlivrodeleditora.Text = l.editora;
            txtlivrodelquant.Text = l.quantidade.ToString();
            txtlivrodeldescricao.Text = l.descricao;
        }
        //excluir
        private void btnlivroexcluir_Click(object sender, EventArgs e)
        {
            LivroDao dao = new LivroDao();

            recolheDadosExcluirLivro();
            dao.excluir_livro(int.Parse(txtlivrodelid.Text));
            limpar_dellivro();
        }

        //limpar excluir
        private void btndellivrolimpar_Click(object sender, EventArgs e)
        {
            limpar_dellivro();
        }

        //buscar excluir
        private void btndelbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                LivroDao dao = new LivroDao();
                Livro l = new Livro();

                l = dao.consultar_livro(int.Parse(txtlivrodelid.Text));
                preencheDadosExcluirLivro(l);
            }
            catch
            {
                MessageBox.Show("Preencha corretamente");
            }
            
            
            
        }

        //alterar
        private void btnlivroalterar_Click(object sender, EventArgs e)
        {
            Livro l = new Livro();
            LivroDao dao = new LivroDao();
            Livro l_novo = recolheDadosAlterarLivro();

            if (l_novo != null)
            {
                l.id = l_novo.id;
                l.titulo = l_novo.titulo;
                l.id_autor = l_novo.id_autor;
                l.edicao = l_novo.edicao;
                l.ano = l_novo.ano;
                l.categoria = l_novo.categoria;
                l.editora = l_novo.editora;
                l.quantidade = l_novo.quantidade;
                l.descricao = l_novo.descricao;
                
                dao.livro = l;
                dao.alterar_livro();

                limpar_alterlivro();
            }
        }

        //limpar alterar
        private void btnlivroalterlimpar_Click(object sender, EventArgs e)
        {
            limpar_alterlivro();
        }

        //buscar alterar
        private void btnlivroalterbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Livro l = new Livro();
                LivroDao dao = new LivroDao();

                l.id = int.Parse(txtlivroalterid.Text);
                l=dao.consultar_livro(l.id);
                preencheDadosAlterarLivro(l);
            }

            catch (Exception excessao)
            {
                MessageBox.Show("Livro não existe!");
            }
        }
        
        //buscar
        private void btnlivrobuscar_Click(object sender, EventArgs e)
        {
            try
            {

                LivroDao dao = new LivroDao();
                lista_livro = dao.listar_livro(int.Parse(txtlivrobuscarid.Text));
                listBoxlivro.DataSource = null;
                listBoxlivro.DataSource = lista_livro;

            }

            catch (Exception excessao)
            {
                MessageBox.Show("Livro não existe!");
            }
        }

        //limpar buscar
        private void btnlivrobuscarlimpar_Click(object sender, EventArgs e)
        {
            txtlivrobuscarid.Clear();
            listBoxlivro = null;
        }

        //cadastrar
        private void btnlivrocadastrar_Click(object sender, EventArgs e)
        {
            Livro l = recolheDadosCadastrarLivro();
            if (l != null)
            {
                LivroDao dao = new LivroDao();
                dao.livro = l;
                dao.cadastrar_livro();

                limpar_cadlivro();
            }
        }

        //limpar cadastrar
        private void txtfunccadlimpar_Click(object sender, EventArgs e)
        {
            limpar_cadlivro();
        }
        #endregion
        //autor
        #region autor
        private Autor recolheDadosCadastrarAutor()
        {
            try
            {
                Autor autor = new Autor();

                autor.id=int.Parse(txtautorcadid.Text);
                autor.nome = txtautorcadnome.Text;
                autor.data_nasc = dateautorcaddatanasc.Value;
                autor.data_falecimento = dateautorcaddatafalesc.Value;
                autor.categoria_principal = cbautorcadcategoria.SelectedItem.ToString();
                autor.contato = txtautorcadcontato.Text;

                return autor;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private Autor recolheDadosAlterarAutor()
        {
            try
            {
                Autor autor = new Autor();

                autor.id = int.Parse(txtautoralterid.Text);
                autor.nome = txtautoralternome.Text;
                autor.data_nasc = dateautoralterdatanasc.Value;
                autor.data_falecimento = dateautoralterdatafales.Value;
                autor.categoria_principal = cbautoraltercategoria.SelectedItem.ToString();
                autor.contato = txtautoraltercontato.Text;

                return autor;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private Autor recolheDadosExcluirAutor()
        {
            try
            {
                Autor autor = new Autor();

                autor.id = int.Parse(txtautordelid.Text);
                autor.nome = txtautordelnome.Text;
                autor.data_nasc = dateautordeldatanasc.Value;
                autor.data_falecimento = dateautordeldatafalesc.Value;
                autor.categoria_principal = cbautordelcategoria.SelectedItem.ToString();
                autor.contato = txtautordelcontato.Text;

                return autor;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private void preencheDadosAlterarAutor(Autor a)
        {
            txtautoralternome.Text = a.nome;
            dateautoralterdatanasc.Value = a.data_nasc;
            dateautoralterdatafales.Value = a.data_falecimento;
            cbautoraltercategoria.SelectedItem = a.categoria_principal;
            txtautoraltercontato.Text = a.contato;
        }
        private void preencheDadosExcluirAutor(Autor a)
        {
            txtautordelnome.Text = a.nome;
            dateautordeldatanasc.Value = a.data_nasc;
            dateautordeldatafalesc.Value = a.data_falecimento;
            cbautordelcategoria.SelectedItem = a.categoria_principal;
            txtautordelcontato.Text = a.contato;
        }

        //alterar
        private void btnautoralterar_Click(object sender, EventArgs e)
        {
            Autor a = new Autor();
            AutorDao dao = new AutorDao();
            Autor a_novo = recolheDadosAlterarAutor();

            if (a_novo != null)
            {
                a.id = a_novo.id;
                a.nome = a_novo.nome;
                a.data_nasc = a_novo.data_nasc;
                a.data_falecimento = a_novo.data_falecimento;
                a.categoria_principal = a_novo.categoria_principal;
                a.contato = a_novo.contato;

                dao.autor = a;
                dao.alterar_autor();

                limpar_alterautor();
            }
        }

        //limpar alterar
        private void btnautoralterlimpar_Click(object sender, EventArgs e)
        {
            limpar_alterautor();
        }

        //buscar alterar
        private void btnautoralterbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Autor a = new Autor();
                AutorDao dao = new AutorDao();

                a.id = int.Parse(txtautoralterid.Text);
                a=dao.consultar_autor(a.id);
                preencheDadosAlterarAutor(a);
            }

            catch (Exception excessao)
            {
                MessageBox.Show("Autor não existe!");
            }
        }
             
        //cadastrar        
        private void btnautorcadastrar_Click(object sender, EventArgs e)
        {
            Autor a = recolheDadosCadastrarAutor();
            if (a != null)
            {
                AutorDao dao = new AutorDao();
                dao.autor = a;
                dao.cadastrar_autor();

                limpar_cadautor();
            }
        }

        //limpar cadastrar
        private void btnautorcadlimpar_Click(object sender, EventArgs e)
        {
            limpar_cadautor();
        }

        //buscar
        private void btnautorbuscar_Click(object sender, EventArgs e)
        {
            try
            {

                AutorDao dao = new AutorDao();
                lista_autor = dao.listar_autor(int.Parse(txtautorbuscarid.Text));
                listBoxautor.DataSource = null;
                listBoxautor.DataSource = lista_autor;
            }

            catch (Exception excessao)
            {
                MessageBox.Show("Autor não existe!");
            }
        }

        //limpar buscar
        private void btnautorbuscarlimpar_Click(object sender, EventArgs e)
        {
            txtautorbuscarid.Clear();
            listBoxautor = null;
        }

        //excluir
        private void btnautorexcluir_Click(object sender, EventArgs e)
        {
            AutorDao dao = new AutorDao();

            recolheDadosExcluirAutor();
            dao.excluir_autor(int.Parse(txtautordelid.Text));
            limpar_delautor();
        }

        //buscar excluir
        private void btnautordelbuscar_Click(object sender, EventArgs e)
        {
            AutorDao dao = new AutorDao();
            Autor a = new Autor();

            a=dao.consultar_autor(int.Parse(txtautordelid.Text));
            preencheDadosExcluirAutor(a);
        }

        //limpar excluir
        private void btnautordellimpar_Click(object sender, EventArgs e)
        {
            limpar_delautor();
        }
        #endregion
        //leitor
        #region leitor
        private Leitor recolheDadosCadastrarLeitor()
        {
            try
            {
                Leitor leitor = new Leitor();

                leitor.nome = txtleitorcadnome.Text;
                leitor.rg = txtleitorcadrg.Text;
                leitor.cpf = txtleitorcadcpf.Text;
                leitor.contato = txtleitorcadcontato.Text;
                leitor.email = txtleitorcademail.Text;
                leitor.status = cbleitorcadstatus.SelectedItem.ToString();
                leitor.data_nasc = dateleitorcaddatanasc.Value;

                return leitor;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private Leitor recolheDadosAlterarLeitor()
        {
            try
            {
                Leitor leitor = new Leitor();

                leitor.nome = txtleitoralternome.Text;
                leitor.rg = txtleitoralterrg.Text;
                leitor.cpf = txtleitoraltercpf.Text;
                leitor.contato = txtleitoraltercontato.Text;
                leitor.email = txtleitoralteremail.Text;
                leitor.status = cbleitoralterstatus.SelectedItem.ToString();
                leitor.data_nasc = dateleitoralterdatanasc.Value;

                return leitor;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private Leitor recolheDadosExcluirLeitor()
        {
            try
            {
                Leitor leitor = new Leitor();

                leitor.nome = txtleitordelnome.Text;
                leitor.rg = txtleitordelrg.Text;
                leitor.cpf = txtleitordelcpf.Text;
                leitor.email = txtleitordelemail.Text;
                leitor.contato = txtleitordelcontato.Text;
                leitor.status = cbleitordelstatus.SelectedItem.ToString();
                leitor.data_nasc = dateleitordeldatanasc.Value;

                return leitor;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se preencheu todos os campos corretamente!");
                return null;
            }
        }
        private void preencheDadosExcluirLeitor(Leitor l)
        {
            txtleitordelnome.Text = l.nome;
            txtleitordelcpf.Text = l.cpf;
            cbleitordelstatus.SelectedItem = l.status;
            txtleitordelemail.Text = l.email;
            txtleitordelcontato.Text = l.contato;
            dateleitordeldatanasc.Value = l.data_nasc;
        }
        private void preencheDadosAlterarLeitor(Leitor l)
        {
            txtleitoralternome.Text = l.nome;
            txtleitoraltercpf.Text = l.cpf;
            cbleitoralterstatus.SelectedItem = l.status;
            txtleitoralteremail.Text = l.email;
            txtleitoraltercontato.Text = l.contato;
            dateleitoralterdatanasc.Value = l.data_nasc;
        }


        //buscar
        private void btnleitorbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                LeitorDAO dao = new LeitorDAO();
                lista_leitor = dao.listar_leitor(txtleitorbuscarrg.Text);
                listBoxleitor.DataSource = null;
                listBoxleitor.DataSource = lista_leitor;
            }

            catch (Exception excessao)
            {
                MessageBox.Show("Leitor não existe!");
            }
        }
        //limpar buscar
        private void btnleitorbuscarlimpar_Click(object sender, EventArgs e)
        {
            txtleitorbuscarrg.Clear();
            listBoxleitor = null;
        }
        //excluir
        private void btnleitorexcluir_Click(object sender, EventArgs e)
        {
            LeitorDAO dao = new LeitorDAO();

            recolheDadosExcluirLeitor();
            dao.excluir_leitor(txtleitordelrg.Text);
            limpar_delleitor();
        }

        //buscar excluir
        private void btnleitordelbuscar_Click(object sender, EventArgs e)
        {
            LeitorDAO dao = new LeitorDAO();
            Leitor l = new Leitor();

            l=dao.consultar_leitor(txtleitordelrg.Text);
            preencheDadosExcluirLeitor(l);
               
        }

        //limpar excluir
        private void btnleitordellimpar_Click(object sender, EventArgs e)
        {
            limpar_delleitor();
        }

        //alterar
        private void btnleitoralterar_Click(object sender, EventArgs e)
        {
            Leitor l = new Leitor();
            LeitorDAO dao = new LeitorDAO();
            Leitor l_novo = recolheDadosAlterarLeitor();

            if (l_novo != null)
            {
                l.rg = l_novo.rg;
                l.nome = l_novo.nome;
                l.cpf = l_novo.cpf;
                l.status = l_novo.status;
                l.data_nasc = l_novo.data_nasc;
                l.email = l_novo.email;
                l.contato = l_novo.email;
                
                dao.leitor = l;
                dao.alterar_leitor();

                limpar_alterleitor();
            }
        }

        //limpar alterar
        private void btnleitoralterlimpar_Click(object sender, EventArgs e)
        {
            limpar_alterleitor();
        }

        //buscar alterar
        private void btnleitoralterbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Leitor l = new Leitor();
                LeitorDAO dao = new LeitorDAO();

                l.rg = txtleitoralterrg.Text;
                l=dao.consultar_leitor(l.rg);
                preencheDadosAlterarLeitor(l);
            }

            catch (Exception excessao)
            {
                MessageBox.Show("Leitor não existe!");
            }
        }

        //cadastrar
        private void btnleitorcadastrar_Click(object sender, EventArgs e)
        {
            Leitor l = recolheDadosCadastrarLeitor();
            if (l != null)
            {
                LeitorDAO dao = new LeitorDAO();
                dao.leitor = l;
                dao.cadastrar_leitor();

                limpar_cadleitor();
            }
        }

        //limpar cadastrar
        private void btnleitorcadlimpar_Click(object sender, EventArgs e)
        {
            limpar_cadleitor();
        }
        #endregion
        //emprestimo
        #region emprestimo
        private Emprestimo recolheDadosCadastrarEmprestimo()
        {
            try
            {
                Emprestimo emprestimo = new Emprestimo();

                emprestimo.id=int.Parse(txtemprescadid.Text);
                emprestimo.rg_leitor = txtemprescadrgleitor.Text;
                emprestimo.id_livro = int.Parse(txtemprescadidlivro.Text);
                emprestimo.rg_funcionario = txtemprescadrgfunc.Text;
                emprestimo.data_retirada = dateemprescaddataret.Value;
                emprestimo.data_devolucao = dateemprescaddatadev.Value;

                return emprestimo;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se todos os campos foram preenchidos adequadamente");
                return null;
            }
        }
        private Emprestimo recolheDadosAlterarEmprestimo()
        {
            try
            {
                Emprestimo emprestimo = new Emprestimo();

                emprestimo.id = int.Parse(txtempresalteridempres.Text);
                emprestimo.rg_leitor = txtempresalterrgleitor.Text;
                emprestimo.id_livro = int.Parse(txtempresalteridlivro.Text);
                emprestimo.rg_funcionario = txtempresalterrgfunc.Text;
                emprestimo.data_retirada = dateempresalterdataret.Value;
                emprestimo.data_devolucao = dateempresalterdatdev.Value;

                return emprestimo;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se todos os campos foram preenchidos adequadamente");
                return null;
            }
        }
        private Emprestimo recolheDadosExcluirEmprestimo()
        {
            try
            {
                Emprestimo emprestimo = new Emprestimo();

                emprestimo.id = int.Parse(txtempresdelidempres.Text);
                emprestimo.rg_leitor = txtempresdelrgleitor.Text;
                emprestimo.id_livro = int.Parse(txtempresdelidlivro.Text);
                emprestimo.rg_funcionario = txtempresdelrgleitor.Text;
                emprestimo.data_retirada = dateempresdeldataret.Value;
                emprestimo.data_devolucao = dateempresdeldatadev.Value;

                return emprestimo;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique se todos os campos foram preenchidos adequadamente");
                return null;
            }
        }
        private void preencheDadosAlterarEmprestimo(Emprestimo emp)
        {
            
            txtempresalterrgleitor.Text = emp.rg_leitor;
            txtempresalteridlivro.Text = emp.id_livro.ToString();
            txtempresalterrgfunc.Text = emp.rg_funcionario;
            dateempresalterdataret.Value = emp.data_retirada;
            dateempresalterdatdev.Value = emp.data_devolucao;
        }
        private void preencheDadosExcluirEmprestimo(Emprestimo emp)
        {
            txtempresdelrgleitor.Text = emp.rg_leitor;
            txtempresdelidlivro.Text = emp.id_livro.ToString();
            txtempresdelrgfunc.Text = emp.rg_funcionario;
            dateempresdeldataret.Value = emp.data_retirada;
            dateempresdeldatadev.Value = emp.data_devolucao;
        }

        //buscar
        private void btnempresbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                EmprestimoDao dao = new EmprestimoDao();
                lista_emprestimo = dao.listar_emprestimo(txtempresbuscarg.Text);
                listBoxemprestimo.DataSource = null;
                listBoxemprestimo.DataSource = lista_emprestimo;
            }

            catch (Exception excessao)
            {
                MessageBox.Show(" Emprestimo não existe!");
            }
        }

        //limpar emprestimo
        private void btnempresbuscarlimpar_Click(object sender, EventArgs e)
        {
            txtempresbuscarg.Clear();
            listBoxemprestimo = null;
        }

        //buscar excluir
        private void btnempresdelbuscar_Click(object sender, EventArgs e)
        {
            EmprestimoDao dao = new EmprestimoDao();
            Emprestimo emp = new Emprestimo();

            emp=dao.consultar_emprestimo(int.Parse(txtempresdelidempres.Text));
            preencheDadosExcluirEmprestimo(emp);

        }

        //excluir
        private void btnempresexcluir_Click(object sender, EventArgs e)
        {
            EmprestimoDao dao = new EmprestimoDao();

            recolheDadosExcluirEmprestimo();
            dao.excluir_emprestimo(int.Parse(txtempresdelidempres.Text));
            limpar_delempres();
        }

        //limpar excluir
        private void btnempresdellimpar_Click(object sender, EventArgs e)
        {
            limpar_delempres();
        }

        //buscar alterar
        private void btnempresalterbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Emprestimo emp = new Emprestimo();
                EmprestimoDao dao = new EmprestimoDao();

                emp.id = int.Parse(txtempresalteridempres.Text);
                emp=dao.consultar_emprestimo(emp.id);
                preencheDadosAlterarEmprestimo(emp);
            }

            catch (Exception excessao)
            {
                MessageBox.Show("Emprestimo não existe!");
            }
        }

        //alterar
        private void btnempresalterar_Click(object sender, EventArgs e)
        {
            Emprestimo emp = new Emprestimo();
            EmprestimoDao dao = new EmprestimoDao();
            Emprestimo e_novo = recolheDadosAlterarEmprestimo();

            if (e_novo != null)
            {
                emp.id = e_novo.id;
                emp.rg_leitor = e_novo.rg_leitor;
                emp.id_livro = e_novo.id_livro;
                emp.rg_funcionario = e_novo.rg_funcionario;
                emp.data_retirada = e_novo.data_retirada;
                emp.data_devolucao = e_novo.data_devolucao;
                
                dao.emprestimo = emp;
                dao.alterar_emprestimo();

                limpar_alterempres();
            }
        }
        //limpar alterar
        private void btnempresalterlimpar_Click(object sender, EventArgs e)
        {
            limpar_alterempres();
        }

        //cadastrar
        private void btnemprescadastrar_Click(object sender, EventArgs e)
        {
            Emprestimo emp = recolheDadosCadastrarEmprestimo();
            if (emp != null)
            {
                EmprestimoDao dao = new EmprestimoDao();
                dao.emprestimo = emp;
                dao.cadastrar_emprestimo();

                limpar_cadempres();
            }
        }

        //limpar cadastrar
        private void btnemprescadlimpar_Click(object sender, EventArgs e)
        {
            limpar_cadempres();
        }
        #endregion



        #region limpar
        void limpar_cadfunc()
        {
            txtfunccadnome.Clear();
            txtfunccadrg.Clear();
            txtfunccadcpf.Clear();
            txtfunccadcontato.Clear();
            cbfunccadstatus.SelectedItem=null;
            datefunccaddatanasc.Value = DateTime.Now.Date;
            txtfunccademail.Clear();
            txtfunccadsenha.Clear();
        }
        void limpar_alterfunc()
        {
            txtfuncalternome.Clear();
            txtfuncalterrg.Clear();
            txtfuncaltercpf.Clear();
            txtfuncalteremail.Clear();
            txtfuncaltersenha.Clear();
            txtfuncaltercontato.Clear();
            cbfuncalterstatus.SelectedItem = null;
            datefuncalterdatanasc.Value= DateTime.Now.Date;
        }

        void limpar_delfunc()
        {
            txtdelfuncnome.Clear();
            txtdelfuncrg.Clear();
            txtdelfunccpf.Clear();
            txtdelfunccontato.Clear();
            cbdelfuncstatus.SelectedItem = null;
            datedelfuncdatanasc.Value= DateTime.Now.Date;
            txtdelfuncemail.Clear();
            txtdelfuncsenha.Clear();
        }

        void limpar_cadlivro()
        {
            txtlivrocadid.Clear();
            txtlivrocadtitulo.Clear();
            txtlivrocadidautor.Clear();
            txtlivrocadedicao.Clear();
            txtlivrocadano.Clear();
            cblivrocadcategoria.SelectedItem = null;
            txtlivrocadeditora.Clear();
            txtlivrocadquant.Clear();
            txtlivrocaddescricao.Clear();
        }

        void limpar_alterlivro()
        {
            txtlivroalterid.Clear();
            txtlivroaltertitulo.Clear();
            txtlivroalteridautor.Clear();
            txtlivroalteredicao.Clear();
            txtlivroalterano.Clear();
            cblivroaltercategoria.SelectedItem = null;
            txtlivroaltereditora.Clear();
            txtlivroalterquant.Clear();
            txtlivroalterdescricao.Clear();
        }

        void limpar_dellivro()
        {
            txtlivrodelid.Clear();
            txtlivrodeltitulo.Clear();
            txtlivrodelidautor.Clear();
            txtlivrodeledicao.Clear();
            txtlivrodeleditora.Clear();
            txtlivrodelquant.Clear();
            txtlivrodeldescricao.Clear();
            cblivrodelcaegoria.SelectedItem = null;
            txtlivrodelano.Clear();
        }

        void limpar_cadautor()
        {
            txtautorcadid.Clear();
            txtautorcadnome.Clear();
            dateautorcaddatanasc.Value= DateTime.Now.Date;
            dateautorcaddatafalesc.Value= DateTime.Now.Date;
            cbautorcadcategoria.SelectedItem = null;
            txtautorcadcontato.Clear();
        }

        void limpar_alterautor()
        {
            txtautoralterid.Clear();
            txtautoralternome.Clear();
            dateautoralterdatanasc.Value= DateTime.Now.Date;
            dateautorcaddatafalesc.Value= DateTime.Now.Date;
            cbautoraltercategoria.SelectedItem = null;
            txtautoraltercontato.Clear();
        }

        void limpar_delautor()
        {
            txtautordelid.Clear();
            txtautordelnome.Clear();
            txtautordelcontato.Clear();
            cbautordelcategoria.SelectedItem = null;
            dateautordeldatafalesc.Value=DateTime.Now.Date;
            dateautordeldatanasc.Value= DateTime.Now.Date;
        }

        void limpar_cadleitor()
        {
            txtleitorcadrg.Clear();
            txtleitorcadnome.Clear();
            txtleitorcadcpf.Clear();
            cbleitorcadstatus.SelectedItem = null;
            txtleitorcademail.Clear();
            txtleitorcadcontato.Clear();
            dateleitorcaddatanasc.Value= DateTime.Now.Date;
        }

        void limpar_alterleitor()
        {
            txtleitoralterrg.Clear();
            txtleitoralternome.Clear();
            txtleitorcadcpf.Clear();
            cbleitoralterstatus.SelectedItem = null;
            txtleitoralteremail.Clear();
            txtleitoraltercontato.Clear();
            dateleitoralterdatanasc.Value= DateTime.Now.Date;
        }

        void limpar_delleitor()
        {
            txtleitordelrg.Clear();
            txtleitordelnome.Clear();
            txtleitordelcpf.Clear();
            txtleitordelemail.Clear();
            txtleitordelcontato.Clear();
            cbleitordelstatus.SelectedItem = null;
            dateleitordeldatanasc.Value= DateTime.Now.Date;
        }

        void limpar_cadempres()
        {
            txtemprescadid.Clear();
            txtemprescadrgleitor.Clear();
            txtemprescadidlivro.Clear();
            txtemprescadrgfunc.Clear();
            dateemprescaddataret.Value= DateTime.Now.Date;
            dateempresdeldatadev.Value= DateTime.Now.Date;
        }

        void limpar_alterempres()
        {
            txtempresalteridempres.Clear();
            txtempresalterrgleitor.Clear();
            txtempresalterrgfunc.Clear();
            txtempresalteridlivro.Clear();
            dateempresdeldataret.Value= DateTime.Now.Date;
            dateempresdeldatadev.Value= DateTime.Now.Date;
        }

        void limpar_delempres()
        {
            txtempresdelidempres.Clear();
            txtempresdelrgleitor.Clear();
            txtempresdelidlivro.Clear();
            txtempresdelrgfunc.Clear();
            dateempresdeldataret.Value= DateTime.Now.Date;
            dateempresdeldatadev.Value= DateTime.Now.Date;
        }


        #endregion

        private void Principal_Load(object sender, EventArgs e)
        {
            listBoxfuncionario.HorizontalScrollbar = true;
            listBoxautor.HorizontalScrollbar = true;
            listBoxemprestimo.HorizontalScrollbar = true;
            listBoxleitor.HorizontalScrollbar = true;
            listBoxlivro.HorizontalScrollbar = true;
        }
    }
}
