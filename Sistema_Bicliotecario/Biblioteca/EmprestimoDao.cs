﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace Biblioteca
{
    class EmprestimoDao
    {
        public static String tabela_emprestimo = "emprestimo";
        public Emprestimo emprestimo { set; get; }

        public void cadastrar_emprestimo()
        {
            if (emprestimo == null) return;
            String SQL = "insert into :tabela_emprestimo (id_emprestimo, rg_leitor,id_livro,rg_funcionario,data_retirada,data_devolucao) values (:id, ':rg_leitor', :livro, ':rg_funcionario', ':data_retirada', ':data_devolucao')";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public void alterar_emprestimo()
        {
            String SQL = "UPDATE :tabela_emprestimo SET id_emprestimo =:id, rg_leitor =':rg_leitor', id_livro=:livro, rg_funcionario=':rg_funcionario', data_retirada =':data_retirada', data_devolucao=':data_devolucao' where  id_emprestimo=:id";
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.executaSQL(SQL);

        }

        private String fazerSubstituicao(String SQL)
        {
            SQL = SQL.Replace(":tabela_emprestimo", tabela_emprestimo);
            if (emprestimo != null)
            {
                SQL = SQL.Replace(":id", emprestimo.id.ToString());
                SQL = SQL.Replace(":rg_leitor", emprestimo.rg_leitor);
                SQL = SQL.Replace(":livro", emprestimo.id_livro.ToString());
                SQL = SQL.Replace(":rg_funcionario", emprestimo.rg_funcionario);
                SQL = SQL.Replace(":data_retirada", emprestimo.data_retirada.ToString());
                SQL = SQL.Replace(":data_devolucao", emprestimo.data_devolucao.ToString ());       

            }

            return SQL;

        }

        public Emprestimo consultar_emprestimo(int id)
        {
            String SQL = "SELECT id_emprestimo, rg_leitor,id_livro,rg_funcionario,data_retirada,data_devolucao FROM :tabela_emprestimo where id_emprestimo = :id";
            SQL = SQL.Replace(":id", id.ToString());
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            Emprestimo e = new Emprestimo();
            while (dr.Read())
            {
                e.id = int.Parse(dr[0].ToString());
                e.rg_leitor = dr[1].ToString();
                e.id_livro = int.Parse(dr[2].ToString());
                e.rg_funcionario = dr[3].ToString();
                e.data_retirada = DateTime.Parse(dr[4].ToString());
                e.data_devolucao = DateTime.Parse(dr[5].ToString());
                
            }

            db.fechar();
            return e;
        }

        public void excluir_emprestimo(int id)
        {
            String SQL = "DELETE FROM :tabela_emprestimo where id_emprestimo = :id";
            SQL = SQL.Replace(":tabela_emprestimo", tabela_emprestimo);
            SQL = SQL.Replace(":id", id.ToString());
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public List<Emprestimo> listar_emprestimo(string leitor)
        {
            String SQL = "SELECT id_emprestimo, rg_leitor,id_livro,rg_funcionario,data_retirada,data_devolucao from :tabela_emprestimo where rg_leitor=':leitor'";
            SQL = SQL.Replace(":tabela_emprestimo", tabela_emprestimo);
            SQL = SQL.Replace(":leitor", leitor);
            SQL = fazerSubstituicao(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            List<Emprestimo> lista_emprestimo = new List<Emprestimo>();
            while (dr.Read())
            {
                Emprestimo e = new Emprestimo();
                e.id = int.Parse(dr[0].ToString());
                e.rg_leitor = dr[1].ToString();
                e.id_livro = int.Parse(dr[2].ToString());
                e.rg_funcionario = dr[3].ToString();
                e.data_retirada = DateTime.Parse(dr[4].ToString());
                e.data_devolucao = DateTime.Parse(dr[5].ToString());

                lista_emprestimo.Add(e);
            }

            db.fechar();
            return lista_emprestimo;
        }
    }
}
